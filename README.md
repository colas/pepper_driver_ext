# pepper_driver_ext

Additional ROS drivers for the Pepper robot.

Check the [Documentation](https://colas.gitlabpages.inria.fr/pepper_driver_ext/index.html#).

The drivers extend the robot functionalities accessible in ROS.
Namely it adds:

- handling of the autonomous state of the robot,
- (un)loading and (de)activating dialog topics,
- text-to-speech functionality,
- access ALMemory,
- publish detected landmarks,
- handle tablet.

They are designed for ubuntu 16.04 with ROS kinetic and Python 2.7 to work alongside the official ROS drivers.

There are two main ROS packages:

- `pepper_driver_ext`: additional drivers,
- `pepper_driver_ext_msgs`: message and service definition for the above extension.
