# Set color for a list of LEDs specified by a mask
# See the Pepper LED placement documentation for reference: http://doc.aldebaran.com/2-5/family/pepper_technical/leds_pep.html#led-pepper

# LED list
# Right eye
uint64 EYE_R0=1              # 0x0000000001 -> FaceLedRight0 = RightFaceLed8 = Face/Led/[Color]/Right/45Deg
uint64 EYE_R1=2              # 0x0000000002 -> FaceLedRight1 = RightFaceLed7 = Face/Led/[Color]/Right/0Deg
uint64 EYE_R2=4              # 0x0000000004 -> FaceLedRight2 = RightFaceLed6 = Face/Led/[Color]/Right/315Deg
uint64 EYE_R3=8              # 0x0000000008 -> FaceLedRight3 = RightFaceLed5 = Face/Led/[Color]/Right/270Deg
uint64 EYE_R4=16             # 0x0000000010 -> FaceLedRight4 = RightFaceLed4 = Face/Led/[Color]/Right/225Deg
uint64 EYE_R5=32             # 0x0000000020 -> FaceLedRight5 = RightFaceLed3 = Face/Led/[Color]/Right/180Deg
uint64 EYE_R6=64             # 0x0000000040 -> FaceLedRight6 = RightFaceLed2 = Face/Led/[Color]/Right/135Deg
uint64 EYE_R7=128            # 0x0000000080 -> FaceLedRight7 = RightFaceLed1 = Face/Led/[Color]/Right/90Deg
# Left eye
uint64 EYE_L0=256            # 0x0000000100 -> FaceLedLeft0 = LeftFaceLed1 = Face/Led/[Color]/Left/45Deg
uint64 EYE_L1=512            # 0x0000000200 -> FaceLedLeft1 = LeftFaceLed2 = Face/Led/[Color]/Left/0Deg
uint64 EYE_L2=1024           # 0x0000000400 -> FaceLedLeft2 = LeftFaceLed3 = Face/Led/[Color]/Left/315Deg
uint64 EYE_L3=2048           # 0x0000000800 -> FaceLedLeft3 = LeftFaceLed4 = Face/Led/[Color]/Left/270Deg
uint64 EYE_L4=4096           # 0x0000001000 -> FaceLedLeft4 = LeftFaceLed5 = Face/Led/[Color]/Left/225Deg
uint64 EYE_L5=8192           # 0x0000002000 -> FaceLedLeft5 = LeftFaceLed6 = Face/Led/[Color]/Left/180Deg
uint64 EYE_L6=16384          # 0x0000004000 -> FaceLedLeft6 = LeftFaceLed7 = Face/Led/[Color]/Left/135Deg
uint64 EYE_L7=32768          # 0x0000008000 -> FaceLedLeft7 = LeftFaceLed8 = Face/Led/[Color]/Left/90Deg
# Right ear
uint64 EAR_R0=65536          # 0x0000010000 -> RightEarLed1 = Ears/Led/Right/0Deg
uint64 EAR_R1=131072         # 0x0000020000 -> RightEarLed2 = Ears/Led/Right/36Deg
uint64 EAR_R2=262144         # 0x0000040000 -> RightEarLed3 = Ears/Led/Right/72Deg
uint64 EAR_R3=524288         # 0x0000080000 -> RightEarLed4 = Ears/Led/Right/108Deg
uint64 EAR_R4=1048576        # 0x0000100000 -> RightEarLed5 = Ears/Led/Right/144Deg
uint64 EAR_R5=2097152        # 0x0000200000 -> RightEarLed6 = Ears/Led/Right/180Deg
uint64 EAR_R6=4194304        # 0x0000400000 -> RightEarLed7 = Ears/Led/Right/216Deg
uint64 EAR_R7=8388608        # 0x0000800000 -> RightEarLed8 = Ears/Led/Right/252Deg
uint64 EAR_R8=16777216       # 0x0001000000 -> RightEarLed9 = Ears/Led/Right/288Deg
uint64 EAR_R9=33554432       # 0x0002000000 -> RightEarLed10 = Ears/Led/Right/324Deg
# Left ear
uint64 EAR_L0=67108864       # 0x0004000000 -> LeftEarLed1 = Ears/Led/Left/0Deg
uint64 EAR_L1=134217728      # 0x0008000000 -> LeftEarLed2 = Ears/Led/Left/36Deg
uint64 EAR_L2=268435456      # 0x0010000000 -> LeftEarLed3 = Ears/Led/Left/72Deg
uint64 EAR_L3=536870912      # 0x0020000000 -> LeftEarLed4 = Ears/Led/Left/108Deg
uint64 EAR_L4=1073741824     # 0x0040000000 -> LeftEarLed5 = Ears/Led/Left/144Deg
uint64 EAR_L5=2147483648     # 0x0080000000 -> LeftEarLed6 = Ears/Led/Left/180Deg
uint64 EAR_L6=4294967296     # 0x0100000000 -> LeftEarLed7 = Ears/Led/Left/216Deg
uint64 EAR_L7=8589934592     # 0x0200000000 -> LeftEarLed8 = Ears/Led/Left/252Deg
uint64 EAR_L8=17179869184    # 0x0400000000 -> LeftEarLed9 = Ears/Led/Left/288Deg
uint64 EAR_L9=34359738368    # 0x0800000000 -> LeftEarLed10 = Ears/Led/Left/324Deg
# shoulders
uint64 SHOULDERS=68719476736 # 0x1000000000 -> ChestBoard/Led/[Color]

# Useful masks
uint64 EYE_RIGHT=255         # 0x00000000ff
uint64 EYE_LEFT=65280        # 0x000000ff00
uint64 EYES=65535            # 0x000000ffff
uint64 EAR_RIGHT=67043328    # 0x0003ff0000
uint64 EAR_LEFT=68652367872  # 0x0ffc000000
uint64 EARS=68719411200      # 0x0fffff0000
uint64 ALL=137438953471      # 0x1fffffffff


uint64 leds_mask
uint32[] color_list
float32[] time_list
---
bool success
string error_message
