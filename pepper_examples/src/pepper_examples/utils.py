"""Utility functions."""

import rospy


def get_ros_param(name, default=None, log_prefix=None):
    """Verbose parameter getter.

    Checks for parameter in parameter server using default if needed.

    Args:
        name: the name of the parameter.
        default: an optional default value.
        log_prefix: optional prefix to know which node checks which
            parameter (default None corresponds to '[node_name] ').

    Raises:
        KeyError: if neither parameter nor default value are there.
    """
    # handle default prefix
    if log_prefix is None:
        log_prefix = '[%s] ' % rospy.get_name()
    if rospy.has_param(name):
        val = rospy.get_param(name)
        rospy.loginfo('%sFound parameter %r: %r', log_prefix, name, val)
        return val
    if default is not None:
        rospy.logwarn('%sParameter %r not found, using default value %r.',
                      log_prefix, name, default)
        return default
    raise KeyError(name)
