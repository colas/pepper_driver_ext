#!/usr/bin/env python
"""
This node launches a dialog with the Pepper using the
service of ``pepper_dialog_driver``.

parameter:
- topfilename: the file wich content the topic 
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import (LoadDialogTopic, ManageDialogTopic, ListDialogTopics, SetAutonomousLifeLevel,
                                        GetLanguage, SetLanguage,
                                        ManageDialogTopicRequest, ListDialogTopicsRequest,SetAutonomousLifeLevelRequest)

# Local imports
from pepper_examples.utils import get_ros_param


class DialogTest:
    """Main class of the node."""

    def __init__(self):
        """Create node and publisher."""
        # create node
        rospy.init_node('dialog_test')
        # service proxy
        rospy.wait_for_service('load_dialog_topic')
        self.load_dialog_topic = rospy.ServiceProxy('load_dialog_topic', LoadDialogTopic)
        rospy.wait_for_service('manage_dialog_topic')
        self.manage_dialog_topic = rospy.ServiceProxy('manage_dialog_topic', ManageDialogTopic)
        rospy.wait_for_service('list_dialog_topics')
        self.list_dialog_topics = rospy.ServiceProxy('list_dialog_topics', ListDialogTopics)
        rospy.wait_for_service('get_language')
        self.get_language = rospy.ServiceProxy('get_language', GetLanguage)
        rospy.wait_for_service('set_language')
        self.set_language = rospy.ServiceProxy('set_language', SetLanguage)

        # stop autonomous life (to avoid default dialog interaction)
        rospy.wait_for_service('set_autonomous_life_level')
        self.set_autonomous_life_level = rospy.ServiceProxy(
            'set_autonomous_life_level', SetAutonomousLifeLevel)
        self.set_autonomous_life_level(SetAutonomousLifeLevelRequest.NO_AUTONOMOUS_LIFE)

    def load_dialog_topic_file(self, topic_file_name, activate):
        """Load a topic with activating or not

        parameter:
            topic_file_name: text file with a topic
            activate (with loading): bool

        return: LoadTopic message
        """
        # open the file
        try:
            with open(topic_file_name, "r") as file:
                content = file.read()
        except OSError:
            rospy.logwarn("[DialogTest] could not open topic file: %s",
                          topic_file_name)

        resp = self.load_dialog_topic(content, activate)

        return resp

    def activate_dialog_topic(self, topic_name, activate):
        """Activate/Deactivate a topic

        return: bool success
        """
        if activate:
            command = ManageDialogTopicRequest.ACTIVATE_TOPIC
        else:
            command = ManageDialogTopicRequest.DEACTIVATE_TOPIC

        resp = self.manage_dialog_topic(command, topic_name)

        return resp.success

    def unload_dialog_topic(self, topic_name):
        """Unload a topic

        return: bool success
        """
        command = ManageDialogTopicRequest.UNLOAD_TOPIC
        resp = self.manage_dialog_topic(command, topic_name)

        return resp.success


def main():
    """Instantiate then launch a dialog process"""
    try:
        node = DialogTest()

        # check the current langage
        resp = node.get_language()
        rospy.loginfo("[DialogTest] Language: %s", resp.language )

        # set language (French or English)
        # self.set_language('French')

        # load a topic (without activating it)
        topfilename = get_ros_param('~topfilename', '') # call rospy.get_param('~topfilename')
        resp = node.load_dialog_topic_file(topfilename, False)

        # activate a topic
        node.activate_dialog_topic(resp.topic_name, True)

        # check loaded and activated topics (for tuto)
        loaded_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.LOADED_TOPICS)
        rospy.loginfo("[DialogTest] Loaded topics: %r", loaded_dialog_topics.topic_names)
        activated_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.ACTIVATED_TOPICS)
        rospy.loginfo("[DialogTest] Activated topics: %r", activated_dialog_topics.topic_names)

        try:
                # you can now use the dialog 
                input("\nLet's speak to Pepper. Press Enter to stop dialog")
        finally:
                # deactivate the topic
                node.activate_dialog_topic(resp.topic_name, False)

                # unload the topic
                node.unload_dialog_topic(resp.topic_name)

                # check loaded and activated topics (for tuto)
                loaded_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.LOADED_TOPICS)
                rospy.loginfo("[DialogTest] Loaded topics: %r", loaded_dialog_topics.topic_names)
                activated_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.ACTIVATED_TOPICS)
                rospy.loginfo("[DialogTest] Activated topics: %r", activated_dialog_topics.topic_names)

    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
