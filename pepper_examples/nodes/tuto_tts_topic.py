#!/usr/bin/env python
"""
This node makes the Pepper say a single sentence using the
`/text_to_speech` topic published by `pepper_tts_driver`.

It publishes to:
   - `/text_to_speech` (std_msgs/String): the sentence to say.
Parameters:
    - `sentence`: sentence to say (default is 'Ceci est la phrase à prononcer par défaut.').
"""

# ROS imports
import rospy
from std_msgs.msg import String
# Local imports
from pepper_examples.utils import get_ros_param


class TTSTopic:
    """Main class of the node."""

    def __init__(self):
        """Create node and publisher."""
        # create node
        rospy.init_node('tts_topic_simple')
        # publisher
        self.tts_pub = rospy.Publisher("/text_to_speech", String, queue_size=1)
        rospy.sleep(1) # ensure publisher is there
        

    def say_text(self, text):
        """Send message"""
        self.tts_pub.publish(text)

def main():
    """Instantiate then run class."""
    try:
        node = TTSTopic()
        text = get_ros_param('~sentence', 'Ceci est la phrase à prononcer par défaut.') 
        node.say_text(text)        
        
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
