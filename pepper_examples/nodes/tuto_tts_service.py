#!/usr/bin/env python
"""
This node makes the Pepper say a single sentence using the
`/text_to_speech` service called by `pepper_tts_driver`.

It calls :
    - `/text_to_speech`: the sentence to say.

Parameters:
    - `sentence`:  sentence to say (default is 'Ceci est la phrase à prononcer par défaut.').
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import TextToSpeech
# Local imports
from pepper_examples.utils import get_ros_param


class TTSService:
    """Main class of the node."""

    def __init__(self):
        """Create node and publisher."""
        # create node
        rospy.init_node('tts_service')
        # service proxy
        rospy.wait_for_service('text_to_speech')
        self.tts = rospy.ServiceProxy('text_to_speech', TextToSpeech)

    def say_sentence(self,text):
        """Send message with a bit of waiting before and after."""
        try:
            self.tts(text)
        except rospy.ServiceException as e:
            rospy.logwarn("[TTSService] could not call service: %s", str(e))


def main():
    """Instantiate then run class."""
    try:
        node = TTSService()
        text = get_ros_param('~sentence', 'Ceci est la phrase à prononcer par défaut.')
        node.say_sentence(text)
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
