#!/usr/bin/env python
"""
This node makes the Pepper say a single sentence using the
`/text_to_speech` topic published by `pepper_tts_driver`.

It publishes to:
    - `/text_to_speech` (std_msgs/String): the sentence to say.
"""

# ROS imports
import rospy
from std_msgs.msg import String


def main():
    """Create ROS node, publisher and publish."""
    rospy.init_node('tts_topic_simple')
    tts_pub = rospy.Publisher('/text_to_speech', String, queue_size=1)
    rospy.sleep(1)  # ensure publisher is there (normally unnecessary)
    tts_pub.publish("Bonjour super simple.")
    rospy.sleep(1)  # ensure message has time to leave before exiting


if __name__ == '__main__':
    main()
