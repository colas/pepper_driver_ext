#!/usr/bin/env python
"""
This node launches a dialog (with the Pepper using the
service of ``pepper_dialog_driver``) and retrieves variable filled inside the dialog.

parameter:
- topfilename: the file wich content the topic (choixjour.top for the tuto)
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import (LoadDialogTopic, ManageDialogTopic, ManageDataStream,SetAutonomousLifeLevel, ManageDataStream,
                                        ManageDialogTopicRequest, ManageDataStreamRequest,SetAutonomousLifeLevelRequest, ManageDataStreamRequest)
from pepper_driver_ext_msgs.srv import (GetMemoryString, SetMemoryString)                                        
from std_msgs.msg import String

# Local imports
from pepper_examples.utils import get_ros_param


class DialogMemory:
    """Main class of the node."""

    def __init__(self):
        """Create node and publisher."""
        # create node
        rospy.init_node('dialog_memory')
        # service proxy for dialog
        rospy.wait_for_service('load_dialog_topic')
        self.load_dialog_topic = rospy.ServiceProxy('load_dialog_topic', LoadDialogTopic)
        rospy.wait_for_service('manage_dialog_topic')
        self.manage_dialog_topic = rospy.ServiceProxy('manage_dialog_topic', ManageDialogTopic)
        # service proxy for memory
        rospy.wait_for_service('manage_data_stream')
        self.manage_data_stream = rospy.ServiceProxy('manage_data_stream', ManageDataStream)
        #rospy.wait_for_service('get_memory_string')
        #self.get_memory_string = rospy.ServiceProxy('get_memory_string', GetMemoryString)
        #rospy.wait_for_service('set_memory_string')
        #self.set_memory_string = rospy.ServiceProxy('set_memory_string', SetMemoryString)
        

        # prepare ros topic for the 'jour' variable
        self.manage_data_stream(ManageDataStreamRequest.START, 'jour', 'jour_ros_topic', ManageDataStreamRequest.STRING)
        # subscriber for this variable
        self.variable_sub = rospy.Subscriber('/jour_ros_topic', String, self.variable_cb)

        # stop autonomous life (to avoid default dialog interaction)
        rospy.wait_for_service('set_autonomous_life_level')
        self.set_autonomous_life_level = rospy.ServiceProxy('set_autonomous_life_level', SetAutonomousLifeLevel)
        self.set_autonomous_life_level(SetAutonomousLifeLevelRequest.NO_AUTONOMOUS_LIFE)


    def load_dialog_topic_file(self, topic_file_name, activate):
        """Load a topic with activating or not

        parameter:
            topic_file_name: text file with a topic
            activate (with loading): bool

        return: LoadTopic message
        """
        # open the file
        try:
            with open(topic_file_name, "r") as file:
                content = file.read()
        except OSError:
            rospy.logwarn("[DialogTest] could not open topic file: %s",
                          topic_file_name)

        resp = self.load_dialog_topic(content, activate)

        return resp

    def unload_dialog_topic(self, topic_name):
        """Unload a topic

        return: bool success
        """
        command = ManageDialogTopicRequest.UNLOAD_TOPIC
        resp = self.manage_dialog_topic(command, topic_name)

        return resp.success

    def variable_cb(self, msg):
        """Check the value of the memory when the variable was filled
        """
        rospy.loginfo("Variable value is: %s", msg.data)

        #Other test with get/set_memory_string        
        #variable_name = 'jour'
        #resp = self.get_memory_string(variable_name)
        #if not resp.success:
        #    rospy.logwarn("The variable %r doesn't exist", variable_name)
        #else:
        #    rospy.loginfo("The variable %r has been filled with %r", variable_name, resp.value)
        #if msg.data=="Lundi":
        #    resp = self.set_memory_string(variable_name, "Mardi")
        

def main():
    """Instantiate then launch a dialog process"""
    try:
        node = DialogMemory()

        # load a topic (with activating it)
        topfilename = get_ros_param('~topfilename', '') # call rospy.get_param('~topfilename')
        resp = node.load_dialog_topic_file(topfilename, True)   
        
        try:
            # you can now use the dialog 
            input("\nLet's speak to Pepper. Press Enter to stop dialog")
        finally:
            # unload the topic
            node.unload_dialog_topic(resp.topic_name)

                
    except rospy.ROSInterruptException:
      pass

if __name__ == '__main__':
    main()
