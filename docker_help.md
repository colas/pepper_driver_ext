# Docker help

Commands for different stages.
Using docker involves creating or getting an image, instantiating the image into a container and launching this container.


## Images

The `Dockerfile` specifications are located in the `Dockerfiles/` directory.
There are a few of them:
- `pepper_driver.Dockerfile` is the main one and defines several stages:
  - `pepper-driver-base`: ubuntu xenial (16.04) and ROS kinetic with official Pepper ROS drivers and naoqi. In addition, an empty catkin workspace and a `pepper` user,
  - `pepper-driver-prod`: on top, copies and compile `pepper_driver_ext`,
  - `pepper-driver-devel`: `pepper-driver-base` with sudoer right for `pepper` user,
  - `pepper-driver-doc`: `pepper-driver-base` with dependencies for building the documentation;
- `pepper_galactic_dev.Dockerfile`: ubuntu focal (20.04) with ROS 2 galactic for ROS 2 test purpose (especially on ubuntu >= 22.04);
- `rosbridge_galactic.Dockerfile`: ubuntu focal (20.04) with ROS 1 noetic, ROS 2 galactic, and the bridge between them for the Pepper drivers;
- `rosbridge_humble.Dockerfile`: ubuntu focal (20.04) with ROS 1 noetic, ROS 2 humble compiled from source, and the bridge between them for the Pepper drivers.

These images are available on the [project registry](https://gitlab.inria.fr/colas/pepper_driver_ext/container_registry).

### Building images

To build the images locally the environment passed must be the root of the repository and dockerfile and stage need to be specified:
```bash
docker build --target pepper-driver-base -t pepper-driver-base -f Dockerfiles/pepper_driver.Dockerfile .
docker build --target pepper-driver-prod -t pepper-driver-prod -f Dockerfiles/pepper_driver.Dockerfile .
docker build --target pepper-driver-devel -t pepper-driver-devel -f Dockerfiles/pepper_driver.Dockerfile .
docker build --target pepper-driver-doc -t pepper-driver-doc -f Dockerfiles/pepper_driver.Dockerfile .
docker build -t pepper-galactic-dev -f Dockerfiles/pepper_galactic_dev.Dockerfile .
docker build -t pepper-rosbridge-galactic -f Dockerfiles/rosbridge_galactic.Dockerfile .
docker build -t pepper-rosbridge-humble -f Dockerfiles/rosbridge_humble.Dockerfile .
```

### Pulling images

Instead of building the images locally, you can directly pull them from the registry:
```bash
docker pull registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-base
docker pull registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-prod
docker pull registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-devel
docker pull registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-doc
```


## Containers

Once the images are either built or pulled, you can create a container with the proper configuration

### `pepper-driver-prod` container

In order for the drivers to work, you need to create a container with the proper options.
In particular, it is necessary for the container to have access to the Pepper robot by sharing the network:
```bash
docker create -it --network host --name pepper-driver-prod pepper-driver-prod
```
or:
```bash
docker create -it --network host --name pepper-driver-prod registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-prod
```

### `pepper-driver-devel` container

You can instantiate a `pepper-driver-devel` container to continue developing the pepper drivers.
In that case, you probably want to share the current directory (from the root of the repository):
```bash
docker create -it --network host --volume=`pwd`:/home/pepper/pepper_driver_ext --name pepper-driver-devel pepper-driver-devel
```
or:
```bash
docker create -it --network host --volume=`pwd`:/home/pepper/pepper_driver_ext --name pepper-driver-devel registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-devel
```

You may need to be careful with the file permissions.


### `pepper-driver-doc` container

In order to compile the documentation, you need to instantiate a `pepper-driver-doc` container by sharing the current directory (from the root of the repository):
```bash
docker create -it --network host --volume=`pwd`:/home/pepper/pepper_driver_ext --name pepper-driver-doc pepper-driver-doc
```

You may need to be careful with the file permissions.


## Running containers

### Generic commands

Once containers are created, they can be started by their names:
```bash
docker start -i <container_name>
```

From inside the container, you can either stop it using `ctrl-d` (and then you'd need to restart it) or you can leave it running and detach using `ctrl-p, ctrl-q`.
To reattach a detached container, you can simply:
```bash
docker attach <container_name>
```

### Running drivers with `pepper-driver-prod`

You can start the drivers from the `pepper-driver-prod` container with:
```bash
roslaunch pepper_driver_ext pepper_driver_ext.launch
```

### Compiling documentation with `pepper-driver-doc`

Compiling the documentation is a simple matter of starting the container:
```bash
docker start -i pepper-driver-doc
```
