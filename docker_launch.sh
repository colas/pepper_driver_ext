#!/usr/bin/env bash

#########
# Usage #
#########

function usage {
    cat <<EOM
Usage: $(basename "$0") [OPTIONS] [PEPPER_INDEX]

  -h            display help

  -w            web share directory (default: current working directory)

  PEPPER_INDEX  index of the Pepper robot to connect to (0 or 1).
EOM
}


#####################
# Parsing arguments #
#####################

# Initial arguments
web_share=`realpath .`

# parse options
while getopts ":hw:" opt; do
    case $opt in
        h)
            usage
            exit 0;;
        w)
            web_share=`realpath $OPTARG`;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            usage
            exit 1;;
    esac
done

# get Pepper IP
case ${@:$OPTIND:1} in
    "")
        echo "Missing Pepper index" >&2
        usage
        exit 1;;
    0)
        pepper_ip="100.75.166.170";;
    1)
        pepper_ip="100.75.166.171";;
    *)
        echo "Unknown Pepper index: ${@:$OPTIND:1}" >&2
        exit 2;;
esac

# get host ip address
host_ip=`ip route get $pepper_ip | sed -n 's/.*src \([^ ]*\).*/\1/p'`

# consume arguments
shift $((OPTIND))


####################
# Actual launching #
####################
docker run --rm -it --network host --volume $web_share:/mnt/web registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-prod bash -ic "roslaunch pepper_driver_ext pepper_driver_all.launch pepper_ip:=$pepper_ip web_directory:=/mnt/web web_ip:=$host_ip $@"
