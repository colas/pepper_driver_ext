Launch files
============

There are two launch files defined in the ``pepper_driver_ext`` ROS package.

.. _pde_launch:

``pepper_driver_ext.launch``
----------------------------

Launch the additional Pepper drivers in the ``pepper_driver_ext`` package:

- :doc:`/api_nodes/pepper_dialog_driver`
- :doc:`/api_nodes/pepper_autonomous_life_driver`
- :doc:`/api_nodes/pepper_tts_driver`
- :doc:`/api_nodes/pepper_memory_driver`
- :doc:`/api_nodes/pepper_landmark_driver`
- :doc:`/api_nodes/pepper_tablet_driver` (only if ``web_ip`` is not ``"None"``)
- :doc:`/api_nodes/pepper_touch_driver`
- :doc:`/api_nodes/pepper_led_driver`
- :doc:`/api_nodes/pepper_cmd_vel_driver`


Arguments
^^^^^^^^^

.. list-table::
   :widths: 20 55 25
   :header-rows: 1

   * - Argument
     - Description
     - Default value
   * - ``pepper_ip``
     - IP address of the Pepper robot
     - 
   * - ``default_language``
     - default dialog language
     - ``French``
   * - ``marker_diameter``
     - naomark diameter (in m)
     - ``0.09``
   * - ``web_ip``
     - IP address of the web server reachable from the Pepper robot
     - ``"None"``
   * - ``web_port``
     - port of the web server
     - ``8080``
   * - ``web_directory``
     - directory of the files to serve
     - ``"."``

.. important::
   The ``pepper_ip`` argument is mandatory.

.. note::
   The :doc:`/api_nodes/pepper_tablet_driver` is not launched if ``web_ip`` is the default ``"None"``.
   More information on the ``web_*`` arguments in the :doc:`documentation of pepper_tablet_driver </api_nodes/pepper_tablet_driver>`.


.. _pda_launch:

``pepper_driver_all.launch``
----------------------------

Launch the official Pepper drivers and the additional Pepper drivers:

- `pepper_bringup/pepper_full_py.launch <http://wiki.ros.org/pepper_bringup>`__
- `pepper_driver_ext.launch`_

.. note::
   In order to work around a `bug in the original drivers <https://gitlab.inria.fr/colas/pepper_driver_ext/-/issues/29>__`, the ``/cmd_vel`` topic from the official drivers is remapped into ``/moveToward/cmd_vel``.


Arguments
^^^^^^^^^

.. note::
   They are the same as above.

.. list-table::
   :widths: 20 55 25
   :header-rows: 1

   * - Argument
     - Description
     - Default value
   * - ``pepper_ip``
     - IP address of the Pepper robot
     - 
   * - ``default_language``
     - default dialog language
     - ``French``
   * - ``marker_diameter``
     - naomark diameter (in m)
     - ``0.09``
   * - ``web_ip``
     - IP address of the web server reachable from the Pepper robot
     - ``"None"``
   * - ``web_port``
     - port of the web server
     - ``8080``
   * - ``web_directory``
     - directory of the files to serve
     - ``"."``

.. important::
   The ``pepper_ip`` argument is still mandatory.
