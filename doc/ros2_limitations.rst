:orphan:

ROS 2 Limitations
=================

The drivers can run with ROS 2 with the docker containers as explained in the :ref:`launching documentation <launching_ros2>`.
This solution is based on the `ros1_bridge <https://github.com/ros2/ros1_bridge>`_ package.
However, there are a few limitations compared to a pure ROS 1 setup.


Memory access
-------------

The :doc:`/api_nodes/pepper_memory_driver` node provides a way to create a ROS topic publishing updates to a specific memory key in the Pepper system.
However, due to reliability issues in the ``dynamic_bridge`` of `ros1_bridge`_, the provided docker is using ``parameter_bridge``, which bridges a declared list of topics and services.
This list must be known at the start of ``parameter_bridge`` and cannot thus be dynamically changed at the creation of a new topic.

Therefore, there is currently no way to have a push mechanism for Pepper memory updates.

.. hint::
   A workaround is to repeatedly query the value of the desired key but it is less efficient.

.. note::
   An `issue <https://gitlab.inria.fr/colas/pepper_driver_ext/-/issues/47>`__ is opened on the repository to find a better solution.


Logging levels
--------------

Logging levels of ROS 1 nodes can be changed using services.
In order not to clutter the ROS 2 environment, it has been chosen not to bridge those services.


ROS 1 and 2 changes
-------------------

The following limitations are due to changes between ROS 1 and 2.


Actions
^^^^^^^

The original pepper drivers make use of `actions <https://wiki.ros.org/actionlib/DetailedDescription#Action_Interface_.26_Transport_Layer>`__ as implemented in ROS 1, which involves topics exchanges.
However, ROS 2 actions are `implemented differently <https://design.ros2.org/articles/actions.html#differences-between-ros-1-and-ros-2-actions>`__ and `ros1_bridge`_ does not yet offer bridging between them.

.. note::
   There is a pending `merge request <https://github.com/ros2/ros1_bridge/pull/256>`__ on the `ros1_bridge`_ repository.


Dynamic reconfigure
^^^^^^^^^^^^^^^^^^^

Parameters in ROS 1 are nominally handled by ``rosmaster`` as a centralized static parameter repository.
The `dynamic_reconfigure <https://wiki.ros.org/dynamic_reconfigure>`__ package proposes, through specific topics and services, a way for a node to have parameters dynamically changing during its lifetime (while being notified).

`Parameters in ROS 2 <https://design.ros2.org/articles/ros_parameters.html>`__ propose to natively integrate that dynamic behavior in a different way, which means that the ``dynamic_reconfigure`` specific topics and services are not available in ROS 2 for `ros1_bridge`_.

On the Pepper, only the camera drivers were using this functionality.


Nodelet
^^^^^^^

In ROS 1, a process can only host a single node.
To reduce copy costs in message passing, the `nodelet <https://wiki.ros.org/nodelet>`__ mechanism has been introduced, allowing to compile what should be standalone nodes into plugins for a ``nodelet_manager`` node to load into threads, which can then communicate using shared memory.
This mechanism made use of services to manage the nodelets.

In ROS 2, processes can natively contain several nodes and this mechanism is not needed any more.
Therefore the corresponding services are not bridged by `ros1_bridge`_.

On the Pepper, only the camera drivers were using this functionality.
