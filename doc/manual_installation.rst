:orphan:

Driver manual installation
==========================

The drivers rely on the `Python Naoqi SDK <http://doc.aldebaran.com/2-5/dev/python/index.html>`__, which is only available for Python 2.7.
They are designed to work alongside the official `ROS drivers <http://wiki.ros.org/pepper_robot>`__, for which the last release has been made under Ubuntu 16.04 with ROS Kinetic.

.. Note::
   As this setup is deprecated, it is usually preferable to use a :ref:`Docker image <drivers>`.


The manual install consists in:

- the system installation of prerequisites,
- the installation of the Python naoqi SDK,
- and the installation of the drivers themselves.


System prerequisites
--------------------

Instructions:

- install `Ubuntu 16.04 <https://releases.ubuntu.com/16.04/>`__,
- install `ROS Kinetic <http://wiki.ros.org/kinetic>`__.

Optional:

- install the following packages using `apt <https://manpages.ubuntu.com/manpages/xenial/man8/apt.8.html>`_:

  - ``python-catkin-tools``
  - ``ros-kinetic-pepper-dcm-bringup``
  - ``ros-kinetic-pepper-robot``
  - ``ros-kinetic-pepper-meshes``


Naoqi bindings
--------------

Instructions:

- install the `Python SDK <http://doc.aldebaran.com/2-5/dev/python/install_guide.html#linux>`__,
- place the ``export PYTHONPATH=...`` command in your ``~/.bashrc``.

.. hint::
   A version of the Python naoqi bindings is available in the ``resources/`` directory of the `project repository <https://gitlab.inria.fr/colas/pepper_driver_ext>`.


Driver installation and compilation
-----------------------------------

Instructions:

- create a `catkin workspace <https://catkin-tools.readthedocs.io/en/latest/verbs/catkin_init.html>`_,
- link or copy the ``pepper_driver_ext`` and ``pepper_driver_ext_msgs`` folders in the ``src/`` directory of your `catkin` workspace,
- `build <https://catkin-tools.readthedocs.io/en/latest/verbs/catkin_build.html>`_ the workspace,
- `source <https://manpages.ubuntu.com/manpages/xenial/en/man1/bash.1.html#shell%20builtin%20commands>`_ the ``devel/setup.bash`` from the workspace in your ``~/.bashrc``.

.. note::
   We advise using `catkin_tools <https://catkin-tools.readthedocs.io/en/latest/index.html>`__ rather than the default `catkin <http://wiki.ros.org/catkin>`__ command line utilities since it is more convenient.
