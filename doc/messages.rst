Message and Service definitions
===============================

There are several message and service types defined in the ``pepper_driver_ext_msgs`` ROS package.

Messages
--------

.. toctree::
   :maxdepth: 1
   :glob:

   api_msgs/msg/*


Services
--------

.. toctree::
   :maxdepth: 1
   :glob:

   api_msgs/srv/*
