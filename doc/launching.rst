Launching drivers
=================

.. important::
   :doc:`installation` of the drivers must have been done before launching them.

There are two different launch file offered: :ref:`pde_launch` and :ref:`pda_launch`.
Depending on your situation, launching the drivers is slightly different:

- `Launching natively`_ if on Ubuntu 16.04 with a :doc:`native installation <manual_installation>`,
- `Launching in docker`_ for ROS 1,
- `Launching for ROS 2`_.


Launching natively
------------------

In the native way, you just need to launch directly, for instance:

.. code-block:: none
   :class: command-line

   roslaunch pepper_driver_ext pepper_driver_all.launch pepper_ip:=<pepper_ip>

.. important::
   Don't forget to provide the ``pepper_ip`` argument and others if relevant (see :doc:`/launch_files`).



.. _launching_docker:

Launching in docker
-------------------

Standard approach
^^^^^^^^^^^^^^^^^

Using a docker container allows us to launch the drivers from any base system and not just a Ubuntu 16.04 with ROS kinetic.
It requires creating a container, starting it and, inside, launching the drivers.

With the ``pepper-driver-prod`` docker image, you can create a container in which launching the drivers:

.. code-block:: none
   :class: command-line

   docker create -it --network host --volume <web_directory>:/mnt/web --name pepper-drivers registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-prod

.. note::
   This step must only be done once.


Once the container is created, it is easy to launch with:

.. code-block:: none
   :class: command-line

   docker start -i pepper-drivers

Then, the drivers are launched as above using ``roslaunch``:

.. code-block:: none
   :class: command-line

   roslaunch pepper_driver_ext pepper_driver_all.launch pepper_ip:=<pepper_ip>

.. important::
   Don't forget to provide the ``pepper_ip`` argument and others if relevant (see :doc:`/launch_files`).


.. _quick_launching:

Quick launching
^^^^^^^^^^^^^^^

However, if you always launch the drivers with the same arguments, you can create a dedicated container which will directly launch the drivers when started:

.. code-block:: none
   :class: command-line

   docker create -it --network host --volume <web_directory>:/mnt/web --name pepper-drivers-launch registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-prod bash -ic "roslaunch pepper_driver_ext pepper_driver_all.launch pepper_ip:=<pepper_ip> web_directory:=/mnt/web web_ip:=<host_ip>"

.. important::
   Don't forget to replace the ``<web_directory>``, ``<pepper_ip>``, and ``<host_ip>`` placeholders.

.. note::
   This step must only be done once.


Once such a container is created, the drivers are directly launched with:

.. code-block:: none
   :class: command-line

   docker start -i pepper-drivers-launch

.. hint::
   The ``docker_launch.sh`` script in the repository is a variant of this method with parameter handling to make it easier.


.. _launching_ros2:

Launching for ROS 2
-------------------

Launching the drivers in ROS 2 requires launching both a container with the drivers in ROS 1 and a bridge.

Launch the ROS 1 drivers in a `docker <Launching in docker>`__.

Launch the bridge container:

.. code-block:: none
   :class: command-line

   docker run --rm -it --env ROS_DOMAIN_ID=$ROS_DOMAIN_ID --network host registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-rosbridge-galactic

.. hint::
   If using ROS2 Humble (on 22.04, for instance), you should use instead ``pepper-rosbridge-humble``:

   .. code-block:: none
      :class: command-line

      docker run --rm -it --env ROS_DOMAIN_ID=$ROS_DOMAIN_ID --network host registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-rosbridge-humble
