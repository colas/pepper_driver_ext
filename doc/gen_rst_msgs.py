#!/usr/bin/env python

"""Generate .rst files for ROS messages and services.

More adaptable than calling `rosdoc_lite` and trying to
patch the output.
"""

import argparse
import glob
import os.path


def gen_rst(input_filename, output_directory, ros_package_name):
    """Generate rst for either a service or a message."""
    # message or service name
    def_name = os.path.splitext(os.path.basename(input_filename))[0]
    # path for inclusion
    rel_path = os.path.relpath(input_filename, output_directory)
    # write .rst
    with open(os.path.join(output_directory, '%s.rst' % def_name), 'w') as f:
        f.write('``%s``\n' % def_name)
        f.write('='*(4+len(def_name)) + '\n')
        f.write('''
                .. literalinclude:: %s
                   :language: python
                   :caption: %s
                ''' % (rel_path, os.path.join(ros_package_name, def_name)))


def gen_rsts(ros_package, output_directory):
    """Generate all rsts from messages and services."""
    ros_package_name = os.path.basename(ros_package)
    # messages
    for f in glob.glob(os.path.join(ros_package, 'msg', '*.msg')):
        gen_rst(f, os.path.join(output_directory, 'msg'), ros_package_name)
    # services
    for f in glob.glob(os.path.join(ros_package, 'srv', '*.srv')):
        gen_rst(f, os.path.join(output_directory, 'srv'), ros_package_name)


def main():
    """Parse args."""
    parser = argparse.ArgumentParser(
        description='Generate rst for ROS messages and services.'
    )
    parser.add_argument('-o', '--output_directory', default='.',
                        help='output directory')
    parser.add_argument('package', nargs='*', default='.',
                        help='path to ROS package')
    args = parser.parse_args()
    for p in args.package:
        gen_rsts(p, args.output_directory)


if __name__ == '__main__':
    main()
