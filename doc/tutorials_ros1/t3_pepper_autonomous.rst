.. sectnum::
   :prefix: T
   :depth: 3
   :start: 3

.. |duration| image:: /_static/images/Clock.png
   :height: 40px


How to manage the autonomous life of Pepper
===========================================

.. note::
   This tutorial is only for ROS 1; for ROS 2, check :doc:`/tutorials_ros2/t3_pepper_autonomous`.


|duration| 5 min


This tutorial aims to learn how to manage the *autonomous life* of Pepper, by using :doc:`/api_nodes/pepper_autonomous_life_driver`.
Pepper is an interactive robot. Several levels of interaction have been developt. 
By default, the robot starts with a full *autonomous* life level, it means that a dialog is activated and several services for interaction are launched (facial tracking, motions for tracking, arms and head motions for speech gestures, etc. )  
Usually, we need to stop these services to avoid conflict with our own interaction dialog.

Levels of autonomous life
-------------------------

As explained in :doc:`/api_nodes/pepper_autonomous_life_driver`, there are four levels:
    - **REST**: no autonomous life nor motion (joint stiffness is 0)
    - **WAKE_UP**: no autonomous life but stiff
    - **NO_AUTONOMOUS_LIFE**: some autonomous motion but don't try to
        interact
    - **FULL_AUTONOMOUS_LIFE**: full autonomous life as when started up

If we want to develop a project with interaction and to keep a realistic behavior, the level **no_autonomous_life** is recommanded.
For this level, the autonomous life dialog is deactived but realistic body motions (arms and head) are kept. 
But the **wake_up** level is frequently used too for avoiding arm motion.
We will see in the next section how to set a level. 


How to change the level of autonomous life
------------------------------------------

We will see how to change the level of automous life by two ways: with an inline command and with a node.
Both ways use the services ``get/set_autonomous_life_level`` with the message format :doc:`/api_msgs/srv/GetAutonomousLifeLevel` and :doc:`/api_msgs/srv/SetAutonomousLifeLevel` respectively.

#. So with an inline command the level is a integer value (from 0 for **REST** to 3 for **FULL_AUTONOMOUS_LIFE** level):

   .. code-block:: none
      :class: command-line
      
      rosservice call /set_autonomous_life_level "level:2"

   
   .. warning::

      Be careful when you put the **REST** level (=0), the robot takes is rest position. So the head is going to be lowered: don't forget to hold its head.


#. With a *ros node*, prepare your empty node: ``autonomous_life_test.py`` (see one of the previous tutorials to create/use a ROS package).

   As it's a simple test, we won't build a class.  So we will just use it in a ``main`` function.


   - At first, let's start by the imports of the service definition and format.

   .. code-block:: python

      # ROS imports
      import rospy
      from pepper_driver_ext_msgs.srv import (SetAutonomousLifeLevel, SetAutonomousLifeLevelRequest)

   
   - And, the initialization of services. We launch a service as usual: (name_of_service, message_format). 
     Unlike inline command, we can use ``SetAutonomousLifeLevelRequest.*`` with REST, WAKE_UP, NO_AUTONOMOUS_LIFE, FULL_AUTONOMOUS_LIFE values (see :doc:`/api_msgs/srv/SetAutonomousLifeLevel`).

   .. code-block:: python

      if __name__ == '__main__':
         # create node
         rospy.init_node('autonomous_life_test')
         # service proxy
         rospy.wait_for_service('set_autonomous_life_level')
         set_autonomous_life_level = rospy.ServiceProxy('set_autonomous_life_level', SetAutonomousLifeLevel)
         
   For instance, if we want to put the *no_autonomous_life* level: 

   .. code-block:: python

         ...
         
         self.set_autonomous_life_level(SetAutonomousLifeLevelRequest.NO_AUTONOMOUS_LIFE) 

   
   .. Note::

      If you would like to know the current level, you will classically use the getter service (``get_autonomous_life_level``)

      .. code-block:: python

            # service proxy
            rospy.wait_for_service('get_autonomous_life_level')
            get_autonomous_life_level = rospy.ServiceProxy('get_autonomous_life_level', GetAutonomousLifeLevel)   

            #get the current level
            resp = get_autonomous_life_level()
            rospy.loginfo("[autonomous_life_test] The level of autonomous life is: %r", resp) 

#. *That's all folks !*


.. admonition:: Take-home message

   You learnt with this tutorial that the Pepper robot has got **four levels** of *autonomous* life. And, you know how to change it with the service ``set_autonomous_life_level`` with a inline command or with a ros node (by using :doc:`/api_msgs/srv/SetAutonomousLifeLevel` format).
   
