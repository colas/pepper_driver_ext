.. sectnum::
   :prefix: T
   :depth: 3
   :start: 4

.. |duration| image:: /_static/images/Clock.png
   :height: 40px


How to use pepper TTS with ROS 1
================================

.. note::
   This tutorial is only for ROS 1; for ROS 2, check :doc:`/tutorials_ros2/t4_tts`.

|duration| 15 min

This tutorial aims to learn how to use the text-to-speech functionality (TTS) with ROS, by using :doc:`/api_nodes/pepper_tts_driver`.
Two ways are presented: with a ROS *topic* and with a ROS *service*.


TTS with a ROS topic
--------------------

#. Before creating a node, we will publish manually a text on the right topic. However, we need to be sure that :doc:`/api_nodes/pepper_tts_driver` has been launched before. 
   Run the docker with :ref:`pda_launch` for instance. Check with ``rosnode list`` and  ``rosnode info /pepper_tts_driver`` if the *text_to_speech* topic and service of node :doc:`/api_nodes/pepper_tts_driver` are here.

#. Now, you can publish manually yourself a text to be pronounced by the robot.

   .. hint::
      Remember: ``rostopic pub /your_topic message_format "formatted_message"``  (here, the *message_format* is `std_msgs/String`)
      Remember: What is your topic? ``rostopic list``

      Answer: The topic is ``/text_to_speech``

#. Is it ok? So, we will use a package to build our node. Make your package with one node (or use a previous one).

   .. note::
      Once you have completed the package with a empty node file (called ``tts_topic.py``), don't forget to build it.

#. Now, let's write the node: ``tts_topic.py``

   .. hint::
      We will make the file piece by piece, so identify, for each piece, the key point (don't just copy-paste!).

   - At first, add ROS import.

     .. code-block:: python

        #!/usr/bin/env python

        # ROS imports
        import rospy
        from std_msgs.msg import String

   - Add the ``__init__`` function to prepare the publisher

     .. code-block:: python

        class TTSTopic:
            """Main class of the node."""

            def __init__(self):
                """Create node and publisher."""
                # create node
                rospy.init_node('tts_topic')
                # publisher
                self.tts_pub = rospy.Publisher("/text_to_speech", String, queue_size=1)
                rospy.sleep(1)  # ensure publisher is there

   - Then we need to create a function to publish the text, pronounced by the robot.

     .. code-block:: python

        def say_text(self, text):
            """Send message"""
            self.tts_pub.publish(text)

   - And the ``main`` function

     .. code-block:: python

        def main():
            """Instantiate then run class."""
            try:
                node = TTSTopic()
                text = "Ceci est un essai de synthèse à partir d'un texte."
                node.say_text(text)
            except rospy.ROSInterruptException:
                pass


        if __name__ == '__main__':
            main()

#. That's it ;-) ... No! We forgot the comment of the node itself at the beginning!

   .. code-block:: python

      #!/usr/bin/env python
      """
      This node makes the Pepper say a single sentence using the
      `/text_to_speech` topic published by `pepper_tts_driver`.

      It publishes to:
         - `/text_to_speech` (std_msgs/String): the sentence to say.
      """


#. Now we can launch our node!

   .. code-block:: none
      :class: command-line

      rosrun your_package tts_topic.py

   .. warning::

      Did you have done the 3 steps for using your beautiful node?

      (Before a tip: go to package folder --> ``roscd your_package``)

      1. Put the file **in the right place** (in ``nodes`` folder)

      2. **Build** the package (``catkin build``)

      3. **Source** the code: ``source ../devel/setup.bash``

.. hint::

   You can also retrieve this example of TTS with a ROS topic in the package ``pepper_examples``.


TTS with a ROS service
----------------------

We will use another way: instead of `topic` method, we will use a `service`.


#. As in the previous section, run the docker with :ref:`pda_launch` for instance. As the name of the service is ``text_to_speech`` (with :doc:`/api_msgs/srv/TextToSpeech` format), check with ``rosnode -q info /text_to_speech`` if the service of the TTS node of :doc:`/api_nodes/pepper_tts_driver` is here.

   .. note::

      The ``-q`` option gives only information about services and messages of the node.

      You have also noticed that the service name is the same than topic name. But it does not matter because we does usually not use topic and service at the same time (even it's possible).

#. Create a new node: ``tts_service.py``

#. Let's start by comments and imports:

   .. code-block:: python

      #!/usr/bin/env python
      """
      This node makes the Pepper say a single sentence using the
      `/text_to_speech` service called by `pepper_tts_driver`.

      It calls :
         - `/text_to_speech`: the sentence to say.

      Parameters:
         - `sentence`:  sentence to say (default is 'Ceci est la phrase à prononcer par défaut.').
      """

      # ROS imports
      import rospy
      from pepper_driver_ext_msgs.srv import TextToSpeech

#. The initialization step to be connected to the service:

   .. code-block:: python

      class TTSService:
         """Main class of the node."""

         def __init__(self):
            """Create node and publisher."""
            # create node
            rospy.init_node('tts_service')
            # service proxy
            rospy.wait_for_service('text_to_speech')
            self.tts = rospy.ServiceProxy('text_to_speech', TextToSpeech)

#. Now, the function to call the TTS service:

   .. code-block:: python

         def say_sentence(self,text):
            """Send message with a bit of waiting before and after."""
            try:
               self.tts(text)
            except rospy.ServiceException as e:
               rospy.logwarn("[TTSService] could not call service: %s", str(e))

   .. hint::
         As you can see in the exception part, unlike the approach by *topic*, the *service* warns you if the synthesis can't be done. That's why the approach by *service* is usually preferred.

#. Finally, the ``main`` part:

   .. code-block:: python

      def main():
         """Instantiate then run class."""
         try:
            node = TTSService()
            text = "Ceci est un essai de synthèse à partir d'un texte."
            node.say_sentence(text)
         except rospy.ROSInterruptException:
            pass


      if __name__ == '__main__':
         main()

#. Now we can launch our node.

   .. code-block:: none
      :class: command-line

      rosrun your_package tts_service.py

.. hint::
   You can also retrieve this example of TTS with a ROS service in the package *pepper_examples*


.. admonition:: Take-home message

   What you have learnt in this tutorial:

   1. How to make Pepper say a text by using a ROS topic.
   2. How to make Pepper say a text by using a ROS service.

   And the approach 2. is preferred (by the fact that we can obtain an error log message).
