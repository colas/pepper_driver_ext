.. sectnum::
   :prefix: T
   :depth: 3
   :start: 5

.. |duration| image:: /_static/images/Clock.png
   :height: 40px


How to manage a dialog with ROS 1
=================================

.. note::
   This tutorial is only for ROS 1; for ROS 2, check :doc:`/tutorials_ros2/t5_dialog`.


|duration| 45 min


This tutorial aims to learn how to manage a dialog with ROS, by using :doc:`/api_nodes/pepper_dialog_driver`.
We will use service approach. This tutorial presents main services, syntax and a link with the memory. For further details, please go to the dialog API reference.


What is a *dialog*
------------------

Before to start we need to recall what is a *dialog* (with Pepper).
Aldebaran provides a framework to manage a dialog between a human and the robot.

A dialog is a list of `rules` which link user input with robot output (*user rules*) and some independent outputs of the robot (*proposal rules*).

These rules concerning one conversation are grouped by *topic* (1 file = 1 topic).

.. warning::
   You read right, we talked about *topic*!
   BE CAREFUL, don't confuse *ROS topics* and *dialog topics*!
   It's easy to avoid because to manage a dialog with ROS we will use a *ROS service*, no ROS topic in the dialog business!
   So, for dialog with pepper, when we use the **topic** wording, it's for **collection of rules** for a conversation.
   It could be seen as a synonym of **conversation**.
   Is it OK for you? Well, we can go on.

The writing of rules for a dialog obviously follows specific syntax, but we will see how to do that later. At first we need to know how to start a dialog.


How to start a dialog: simplest way
-----------------------------------

#. Prepare your empty node: ``dialog_test.py`` (see one of the previous tutorials to create/use a ROS package)

#. As said just above, we need another file which contains a topic. Create a file (wherever you want, but in a *resources* folder of package is a good idea) with file extension ``.top`` and fill it with this simple conversation (choose the French or English example).

   .. code-block:: none

      topic: ~bonjour()
      language: frf

      u: (Bonjour) Bonjour ! Je m'appelle Pepper.

   or

   .. code-block:: none

      topic: ~hello()
      language: enu

      u: (Hello) Hello! My name is Pepper.

   In few words, we have the *name* of the topic, the *language* and when the user says what is between parentheses, the robot should answer by the remaining of the line (if it manages to recognize what the user said :-) ).
   The name of the file doesn't matter (even if the best way is to use the topic name itself)

   
#. We can start to manage the dialog. Come back in your node file (``dialog_test.py``). At first, let's start by the imports of the service definitions and their format.

   .. code-block:: python

      # ROS imports
      import rospy
      from pepper_driver_ext_msgs.srv import (LoadDialogTopic, ManageDialogTopic, SetAutonomousLifeLevel, 
                                                ManageDialogTopicRequest, SetAutonomousLifeLevelRequest)

   You can notice that there are a 3 definitions. We are going to use them below.
#. Now, the first step is the initialization of services. We will connect to each of them.

   .. note::
      We wait a service as usual with ``rospy.ServiceProxy(name_of_service, service_type)``

   .. code-block:: python

      class DialogTest:
          """Main class of the node."""

          def __init__(self):
              """Create node and publisher."""
              # create node
              rospy.init_node('dialog_test')
              # service proxy
              rospy.wait_for_service('load_dialog_topic')
              self.load_dialog_topic = rospy.ServiceProxy('load_dialog_topic', LoadDialogTopic)
              rospy.wait_for_service('manage_dialog_topic')
              self.manage_dialog_topic = rospy.ServiceProxy('manage_dialog_topic', ManageDialogTopic)
              
   .. hint::
      As you know, Pepper has got an autonomous life which gives a lot of interaction possibilities.
      But your dialog topic risks to enter in conflict with the dialog topic of the autonomous life!

      So, let's take advantage of the initialization phase to stop the autonomous life. We will use the ``set_autonomous_life_level`` service (with :doc:`/api_msgs/srv/SetAutonomousLifeLevel` format) 

      .. code-block:: python

         # stop autonomous life (to avoid default dialog interaction)
         rospy.wait_for_service('set_autonomous_life_level')
         self.set_autonomous_life_level = rospy.ServiceProxy('set_autonomous_life_level', SetAutonomousLifeLevel)
         self.set_autonomous_life_level(SetAutonomousLifeLevelRequest.NO_AUTONOMOUS_LIFE)  

#. We will now build the function to load a topic, i.e. load the content of a topic file. The *activate* argument is explained in the box below the code.

   - The first step is the retrieving of the content of the topic file:

     .. code-block:: python

        def load_dialog_topic_file(self, topic_file_name, activate):
            """Load a topic with activating or not

            parameter:
                topic_file_name: text file with a topic
                activate (with loading): bool

            return: LoadDialogTopic message
            """
            # open the file
            try:
                with open(topic_file_name, "r") as file:
                    content = file.read()
            except OSError:
                rospy.logwarn("[DialogTest] could not open topic file: %s", topic_file_name)

   - The second step is the loading itself by the service ``load_dialog_topic``:

     .. code-block:: python

            ...

            resp = self.load_dialog_topic(content, activate)

            return resp

   .. important::
      As you can see the ``load_dialog_topic`` takes the *content* of the file and also a boolean argument **activate**. So we need to talk about the difference between *load* and *activate* a topic.
      To be used a topic need to be *loaded* in the (robot) memory and *activated*. Don't forget also the fact that when a topic is activated, so the rules are used by the robot.
      It also means that you can *activate* or *deactivate* a topic during all the work with the robot according to the topic that you want to focus.

      As usually we use only one topic, we can load and activate at once with ``load_dialog_topic`` (thaht we will do for this simplest version).
      During the process, if we need to *activate*, *deactivate*, we will use the ``manage_dialog_topic`` service (with :doc:`/api_msgs/srv/ManageDialogTopic`). It will be showed in the next part.


#. At last, if we *load* something, it's a good way of coding to foreseen a function to unload (also with ``manage_dialog_topic`` and :doc:`/api_msgs/srv/ManageDialogTopic`).

   .. code-block:: python

      def unload_dialog_topic(self, topic_name):
         """Unload a topic

         return: bool success
         """
         command = ManageDialogTopicRequest.UNLOAD_TOPIC
         resp = self.manage_dialog_topic(command, topic_name)

         return resp.success

   .. hint::
      Unload your topic is not only a good way of coding but also avoid side effect.
      Indeed, when we code, test, code, test,... a topic, we need to be sure that it's the last version of the topic which is in the memory of the robot (from many years of experience ;-)).


#. The final point is the `main function``: the sequence with these 2 main steps *load+activate* / (*use*) / *unload*.

   .. code-block:: python

      def main():
         """Instantiate then launch a dialog process"""
         try:
            node = DialogTest()

            # load a topic (with activating it)
            topfilename = rospy.get_param('~topfilename')
            resp = node.load_dialog_topic_file(topfilename, True)

            
            try:
                  # you can now use the dialog 
                  input("\nLet's speak to Pepper. Press Enter to stop dialog")
            finally:
                  
                  # unload the topic
                  node.unload_dialog_topic(resp.topic_name)

         except rospy.ROSInterruptException:
            pass


      if __name__ == '__main__':
         main()

   .. important::
      
      If you launch this node by a command-line dont forget to give the full path for the topicfile:

      .. code-block:: none
         :class: command-line

         rosrun your_package dialog_test.py ~topfilename:='/home/.....resources/bonjour.top'

      .. hint::

         In a launch file you can do better to find easily the right path (with the *find* shell command):

         .. code-block:: xml

            <launch>
               <!-- Arguments -->
               <arg name="topfilename" default="$(find your_package)/resources/bonjour.top" doc="text file with a topic"/>

               <!--  dialog test  -->
               <node pkg="your_package" type="dialog_test.py" name="dialog_test" output="screen">
                  <param name="topfilename" value="$(arg topfilename)"/>
               </node>

            </launch>


         and by:

      .. code-block:: none
         :class: command-line

         roslaunch your_package your_launch_file.launch




How to start a dialog: the full way (with activate/deactivate)
--------------------------------------------------------------

As explained in the previous section, there is a difference between *load* and *activate*. In this section, we will add, to the previous example, a function to activate/deactivate with the ``manage_dialog_topic``. 
And we will take the opportunity to present how to list loaded/activated topics and dsplay the current language.

#. Come back in your previous node file (``dialog_test.py``). At first, let's start by adding the imports of ``ListDialogTopics``, ``GetLanguage``, ``SetLanguage`` and ``ListDialogTopicsRequest``.

#. Now, add in ``__init__`` the initialization of (new) services. 

   .. code-block:: python
            
            ...

            rospy.wait_for_service('list_dialog_topics')
            self.list_dialog_topics = rospy.ServiceProxy('list_dialog_topics', ListDialogTopics)
            rospy.wait_for_service('get_language')
            self.get_language = rospy.ServiceProxy('get_language', GetLanguage)
            rospy.wait_for_service('set_language')
            self.set_language = rospy.ServiceProxy('set_language', SetLanguage)


#. Let's write the function fot the activation/deactivation with ``manage_dialog_topic``. For this, it works with a command message (:doc:`/api_msgs/srv/ManageDialogTopic`).

   .. code-block:: python

      def activate_dialog_topic(self, topic_name, activate):
         """Activate/Deactivate a topic

         return: bool success
         """
         if activate:
            command = ManageDialogTopicRequest.ACTIVATE_TOPIC
         else:
            command = ManageDialogTopicRequest.DEACTIVATE_TOPIC

         resp = self.manage_dialog_topic(command, topic_name)

         return resp.success

#. We will rewrite the ``main`` function: the sequence with now these 4 steps *load* / *activate* / (*use*) /  *deactivate* / *unload*.

   .. note::
      We take the opportunity to present how to list loaded/activated topics with ``list_dialog_topics`` ( with a command :doc:`/api_msgs/srv/ListDialogTopics`).
      This is how we can retrieve topics (we will add these lines in the ``main`` function below):

      .. code-block:: python

         loaded_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.LOADED_TOPICS)
         activated_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.ACTIVATED_TOPICS)
      
      *loaded_dialog_topics* and *activated_dialog_topics* are a *list* of topic names (String)


      and if we want to check the current language:

      .. code-block:: python

         resp = node.get_language()
         rospy.loginfo("[DialogTest] Language: %s", resp.language )

      You have perhaps already noticed that a call to a service displays a log (in the window of the docker), so you have the information of topics/language in the log. Here, it's for the tutorial.


   And the full ``main`` function (you can noticed that we load the topic file without activating the topic):

   .. code-block:: python

      def main():
         """Instantiate then launch a dialog process"""
         try:
            node = DialogTest()

            # check the current langage
            resp = node.get_language()
            rospy.loginfo("[DialogTest] Language: %s", resp.language )

            # set language (French or English)
            # self.set_language('French')

            # load a topic (without activating it)
            topfilename = get_ros_param('~topfilename', '')
            resp = node.load_dialog_topic_file(topfilename, False)

            # activate a topic
            node.activate_dialog_topic(resp.topic_name, True)

            # check loaded and activated topics (for tuto)
            loaded_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.LOADED_TOPICS)
            rospy.loginfo("[DialogTest] Loaded topics: %r", loaded_dialog_topics.topic_names)
            activated_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.ACTIVATED_TOPICS)
            rospy.loginfo("[DialogTest] Activated topics: %r", activated_dialog_topics.topic_names)

            try:
                  # you can now use the dialog 
                  input("\nLet's speak to Pepper. Press Enter to stop dialog")
            finally:
                  # deactivate the topic
                  node.activate_dialog_topic(resp.topic_name, False)

                  # unload the topic
                  node.unload_dialog_topic(resp.topic_name)

                  # check loaded and activated topics (for tuto)
                  loaded_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.LOADED_TOPICS)
                  rospy.loginfo("[DialogTest] Loaded topics: %r", loaded_dialog_topics.topic_names)
                  activated_dialog_topics = node.list_dialog_topics(ListDialogTopicsRequest.ACTIVATED_TOPICS)
                  rospy.loginfo("[DialogTest] Activated topics: %r", activated_dialog_topics.topic_names)

         except rospy.ROSInterruptException:
            pass


      if __name__ == '__main__':
         main()

   It would have been better to use 2 topic files, to play with activate/desactivate, but the main point is here (and you can easily build your own example to test it).

   .. hint::
      You can also retrieve this example of dialog with a ROS service in the package *pepper_examples*



Dialog Syntax
-------------

The section presents some basic points about syntax of a topic.

Come back to the topic file. To test the syntax, you need to have already done the previous part of this tutorial.

#. Let recall the basic structure:

   .. code-block:: none

      topic: ~bonjour()
      language: frf

      u: (Bonjour) Bonjour ! Je m'appelle Pepper.

   A *name* of topic (``bonjour``), a *language* (``frf`` --> French) and *rules*.

   A *user rule* is composed of ``u:`` followed by the input of the user ``(input)`` and the remaining is the output pronounced by the robot (triggered by the recognition of the input of the user).

#. About the input, several operators exist to propose choices in the input. You can test all of these:

   .. code-block:: none

      u: ([Bonjour Salut "Hé Pepper"]) Bonjour à vous !
      # with [ ] : choice of words to trigger the response.

      u: (Bonjour je suis {vraiment} content de vous rencontrer) Moi aussi !
      # with { } : the word can be missed.

      u: (Bonjour je suis {[vraiment très]} content de vous rencontrer) Moi aussi !
      # mixing [ ] and { }

      u: (Bonjour je suis * content de vous rencontrer) Moi aussi !
      # with * : replace any part of speech.

      u: (!Bonjour je suis content de vous rencontrer) Moi aussi !
      # with ! : the response will be triggered with "je suis content de vous rencontrer" 
      #          only it's not preceeded by "Bonjour".

   .. hint::
      A set of choice/words can be grouped in a *concept* to be easily used.

      .. code-block:: none

         concept: (salutation) [Bonjour Salut "Hé Pepper"]

         u: (~salutation) Bonjour à vous !

      The robot can also use *concept* : it takes words in the order. It can choose randomly (``^rand``).

      .. code-block:: none

         concept: (salutation) [Bonjour Salut "Bonjour à vous"]

         u: (~salutation) ^rand ~salutation

#. The rules can be composed by a tree structure. Look at this example:

   .. code-block:: none

      u: (Salut) Comment vas tu ?
         u1: (très bien) Parfait !
         u1: (pas très bien) Dommage ! Tu veux en parler ?
            u2: (oui) très bien, raconte moi.
            u2: (non) D'accord, c'est comme tu veux.

   - The *u1* are usable only if the (parent) *u* has been used.
   - The *u2* are usable only if the second *u1* has been used. The *u1: (très bien) Parfait !* doesn't open *u2* rules.
     When we are into a *u2* level, the *u1* are not active any more.
   - All *u* are always reachable.

#. It is also possible to structure the answers of Pepper with *proposal*. In our concern, we will mainly use it in this following way.

   .. code-block:: none

      u: (Salut) Comment vas tu ?
         u1: (très bien) Parfait ! ^goto(demande_nom)
         u1: (pas très bien) Dommage ! Tu veux en parler ?
            u2: (oui) très bien, raconte moi.
            u2: (non) D'accord, c'est comme tu veux.

      proposal: %demande_nom Quel jour de la semaine  sommes-nous ?
         u1: ([Lundi Mardi Mercredi Jeudi Vendredi]) Cool ! J'aime bien ce jour.
         u1: ([Samedi Dimanche]) Super ! J'aime bien le week-end.

   - ``^goto()``: the answer will be completed by the *proposal*
   - ``demande_nom``: is the tag of the *proposal* (``%demande_nom``)
   - ``proposal:``: start a new part of the conversation, not reachable by another way.

   .. note::
      ``proposal:`` can be used with other operators as ``^nextproposal``. See the original page for further explanation.


#. Now we are going to present how to store a value to use it in the dialog. It a key point of the dialog. In the next section we will see how to retrieve and use it in a node!

   An example of dialog with a variable:

      .. code-block:: none

         u: (Salut) Bonjour ! Quel jour de la semaine sommes-nous ?
            u1: (_[Lundi Mardi Mercredi Jeudi Vendredi]) Cool ! J'aime bien le $1 $jour=$1
            u1: (_[Samedi Dimanche]) Super ! J'aime bien le week-end $jour=$1

         u: (Quel jour t'ai-je dit) "Tu m'as indiqué que nous étions $jour"

         u: (Oublie le jour que je t'ai indiqué, s'il te plait) ^clear(jour) c'est oublié !

      - Operator ``_``: store the pronounced word in $1 and can be used in the robot response of the rule. We can store several words with several ``_[...]``. It will be ``$2``, ``$3``, etc.

        .. important::
           For the **English** language, ``_*`` is possible, to store anything! But unfortunately, with **French** language it is **not allowed**!

      - ``$jour``: is the name of the stored variable. We can reuse it in a other *rule* (as you can see in the second *u:*)
      - In the third *u:*, we can remove the variable with ``^clear`` (notice that there is non '$').
         .. important::
            If a variable does not exist (not filled or deleted), a robot answer, which uses this variable, will not be pronounced (neither only the beginning). 
            Here, "``Tu m'as indiqué que nous étions``" will not be pronounced.
       
   We can also conditioned the answer of the robot (even the input). Look at these examples:

      .. code-block:: none

         1. u: (Quel jour t'ai-je dit) ^first["Tu m'as indiqué que nous étions $jour" "Je ne me souviens pas que tu m'aies indiqué un jour"]

         2. u: (J'aime le lundi $jour=="Lundi") Moi aussi j'aime le Lundi.

         3. u: (Mon jour était le Lundi) ["Oui tout à fait $jour=="Lundi" " "Je ne pense pas"]

      1. The first answer is given if the variable exists. If ``$jour`` doesn't exist, therefore the second answer will be used. In other words, ``^first`` asks to evaluate all proposition by starting with the first until one is valid.
      2. The answer will be triggered by ``J'aime le lundi`` if the variable is equal to ``lundi``
      3. Like 2., but for the robot answer.

.. note::

   For more details or other functionalities (for integrating events, tag, focus, etc.) you can go to the original pages of Aldebaran:

   - An `overview <http://doc.aldebaran.com/2-5/naoqi/interaction/dialog/aldialog_syntax_overview.html>`__

   - A `cheat sheet <http://doc.aldebaran.com/2-5/naoqi/interaction/dialog/aldialog_syntax_cheat_sheet.html>`__

   - The `full syntax <http://doc.aldebaran.com/2-5/naoqi/interaction/dialog/dialog-syntax_full.html>`__


How to retrieve a user response to use it in a function
-------------------------------------------------------

   For this tutorial we will take the topic example of the choice of a day (used above). So, create your ``choixjour.top`` file with:

   .. literalinclude:: ../../pepper_examples/resources/choixjour.top
      :language: none
      :lines: 1-15

   #. For the node to test, you can use the simplest version of the dialog tutorial (i.e when the *loading* is done with activation.)
      We will use the :doc:`/api_nodes/pepper_memory_driver` with one of its services: ``manage_data_stream``. 
      The service creates a ros topic to publish value set into Pepper memmory. To read and write in the Pepper memory we can use ``get/set_memory_*`` service. 

   #. Add the imports related to these two services with format: :doc:`/api_msgs/srv/ManageDataStream` and :doc:`/api_msgs/srv/GetMemoryString`.
      Don't forget *ManageDataStreamRequest* (we will need of these commands).

   #. Let's recall the story:
      The service ``manage_data_stream`` allows us to create a **ros topic** which will be linked with our **variable in memory**. This variable can be used in the **dialog topic** file and in a node.
      In other terms, the *dialog topic* will "publish" in this *ros topic* when variable will be filled. The message for a subscriber on this topic will be the value of the variable. 
      So we just need to subscribe to this *ros topic* and when a event is triggered then we can retrieve the content of the variable (in the callback function). 
      In the node, we can also retrieve the variable value by ``get_memory_string``. You have noticed that we use :doc:`/api_msgs/srv/GetMemoryString` because the variable is a 'String' (a day of a week). There are other :doc:`types </api_msgs/srv/ManageDataStream>` (int, float and bool).
      
      It could give the following figure:


      .. figure:: /_static/images/dialog_memory.png
               :alt: simplified memory diagram
               :width: 100%
               :align: center

               *Figure 1 : Simplified memory diagram.*


      .. note::

         There is obviously a service to *set* a variable: ``set_memory_string`` with :doc:`/api_msgs/srv/GetMemoryString` (and other :doc:`types </api_msgs/srv/ManageDataStream>` too). We won't use it in this tutorial.

         Question: *Does ``set_memory_`` trigger a publishing on the ros topic of the variable?*
         
         . 
         
         . 
         
         . 
         
         . 
         
         Answer: *Yes, it does. The variable was modified in memory so a event was published.*   
            

   #. At first, in ``__init__``, we start with:

      - Preparing the *ros topic* for our variable *jour* by the ``manage_data_memory``. After advertising the service as usual, we will use it to create the associated *ros topic*.
        This service requires a *command* (``START``), name of variable ('jour'),  name of a *ros topic* ('jour_ros_topic')  and *type* of the variable (``ManageDataStreamRequest.STRING``).
        
        .. code-block:: python
            
            rospy.wait_for_service('manage_data_stream')
            self.manage_data_stream = rospy.ServiceProxy('manage_data_stream', ManageDataStream)
            self.manage_data_stream(ManageDataStreamRequest.START, 'jour', 'jour_ros_topic', ManageDataStreamRequest.STRING)
      
        .. important::

            Be careful, in the call of ``manage_data_stream``, it's a *ros topic* (the documentation talk about *topic_name*), not the *dialog topic*.
            Moreover, if we have got several variables, we need to create several *ros topics*.

      - Now, we create a subscriber on this *ros topic* associated with a callback function which we will called ``variable_cb``:

        .. code-block:: python

            self.variable_sub = rospy.Subscriber('/jour_ros_topic', String, self.variable_cb)
            
        .. important::

            The second parameter is 'String' because it's the *ros topic* format because it's the variable format (in memory). We need to define it in the import part:
            
            .. code-block:: python
            
               from std_msgs.msg import String  
               
            For the other types:

            -  integer: Int32
            -  float: Float32
            -  boolean: Bool
            -  string: String

            In case of doubt, you can look at the documentation for each ``GetMemory*`` messages  (for instance for float value: :doc:`/api_msgs/srv/GetMemoryFloat`) 

      - At last, as we will test ``get_memory_string``,  we launch the service ``get_memory_string`` (with :doc:`/api_msgs/srv/GetMemoryString`) to be able to retrieve value of a variable.

        .. code-block:: python

            rospy.wait_for_service('get_memory_string')
            self.get_memory_string = rospy.ServiceProxy('get_memory_string', GetMemoryString)
        
   #. It now remains to create the **callback function** attached to the subscriber. In this function, we have chosen to  display the variable value (contained in the message). 
      And we check by ``get_memory_string`` if the variable exists in the memory. this two steps are redondant (and useless... but it's for the tuto!).
      Remember that during the dialog the variable can be removed (``^clean()``).
      We call ``get_memory_string`` with name of variable as a *key* and we retrieve an object with value (see :doc:`/api_msgs/srv/GetMemoryString` for all members of the message).

      .. code-block:: python

         def variable_cb(self,msg):
            """Check the value of the memory when the variable was filled
            """
            rospy.loginfo("Variable value is: %s", msg.data)

            variable_name='jour'
            resp = self.get_memory_string(variable_name)
            if not resp.success:
                  rospy.logwarn("The variable %r doesn't exist", variable_name)
            else:
                  rospy.loginfo("The variable %r has been filled with %r", variable_name, resp.value)

   #. The ``main`` function is unchanged.

   .. hint::
      You can also retrieve this example of dialog with a ROS service in the package *pepper_examples*

.. admonition:: Take-home message

   What you have learnt in this tutorial:

   1. How to make a dialog by **loading, activating, deactivating** and **unloading**.
   2. How to build a *topic* for the dialog, i.e. **write rules with the qiChat syntax**: user input, pepper output, variable use. 
   3. How to **retrieve a variable** of a dialog in a function of a node.
