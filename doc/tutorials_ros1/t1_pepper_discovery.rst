.. sectnum::
   :prefix: T
   :depth: 3
   :start: 1

.. highlight:: none


Pepper discovery
================

.. note::
   This tutorial is only for ROS 1; for ROS 2, check :doc:`/tutorials_ros2/t1_pepper_discovery`.

The aim of this tutorial is to get to know the capabilities of the Pepper robot exposed by the ROS drivers.
It will also be an introduction to handling of ROS nodes, topics, and services.


.. important::
   You need to have done the previous tutorial: :doc:`/tutorials_ros1/t0_basic_setup` and have access to a Pepper robot with its IP address.


Launching drivers
-----------------

Launching the drivers is done with a :doc:`launch file </launch_files>`.
Doing so natively requires a specific environment and it is more convenient to do it in a :ref:`docker container <launching_docker>` since it can be done on any system.


Container creation
^^^^^^^^^^^^^^^^^^

.. note::
   This step must only be done once.

We first need to create a docker container from the ``pepper-driver-prod`` image.
We will name it ``pepper-drivers-noweb-<login>`` to be able to start it easily.

.. hint::
   Replace ``<login>`` by your actual login, for instance ``colas``.
   This prevents container name collision on shared computers.

#. Create a docker container with:

   .. code-block:: none
      :class: command-line

      docker create -it --network host --name pepper-drivers-noweb-<login> registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-prod


.. _actual_launching_ros1:

Actual launching
^^^^^^^^^^^^^^^^

.. note::
   These two steps must be done each time you want to start the Pepper drivers.

Once the container is created, it must be started and then, inside, we can launch the drivers.

#. Start your ``pepper-drivers-noweb-<login>`` container (make sure you replace ``<login>``, of course):

   .. code-block:: none
      :class: command-line

      docker start -i pepper-drivers-noweb-<login>

   .. hint::
      To distinguish a shell inside the docker from a shell on the host, you can refer to the user in the prompt: inside the container, the prompt starts with ``pepper@``.

#. Launch the drivers using ``roslaunch``:

   .. code-block:: none
      :class: command-line

      roslaunch pepper_driver_ext pepper_driver_all.launch pepper_ip:=<pepper_ip>

   .. important::
      Don't forget to provide the ``pepper_ip`` argument and others if relevant (see :doc:`/launch_files`).

   .. hint::
      You don't need to fully understand this command, it will be covered in more details in the next tutorial.

   .. note::
      You should see a lot of messages being displayed.
      If everything goes well, you shouldn't see any red error message and the drivers should not stop by themselves.

.. note::
   More information in the page on :doc:`/launching`.

   .. hint::
      When you get used to launch the drivers with always the same arguments, you can create a dedicated container as shown in :ref:`quick_launching`.


Nodes
-----

We will now explore the nodes run by the launch file.

#. In another terminal window, list the nodes using:

   .. code-block:: none
      :class: command-line

      rosnode list

   It list the nodes and nodelets launched; you should see:

   - ``/nao_logger``: logging of Pepper internal messages,
   - ``/naoqi_joint_states``: get pose of the Pepper robot,
   - ``/naoqi_moveto``: Pepper base motion,
   - :doc:`/api_nodes/pepper_autonomous_life_driver`: handling of autonomous life of Pepper robot,
   - :doc:`/api_nodes/pepper_dialog_driver`: handling of Pepper dialog,
   - :doc:`/api_nodes/pepper_landmark_driver`: landmark detection,
   - ``/pepper_laser``: laser driver,
   - :doc:`/api_nodes/pepper_memory_driver`: access to Pepper memory,
   - ``/pepper_robot/camera/*``: various camera drivers,
   - ``/pepper_robot/pose/*``: Pepper pose controller,
   - ``/pepper_robot/sonar/*``: sonar drivers,
   - :doc:`/api_nodes/pepper_tts_driver`: text-to-speech functionality,
   - ``/robot_state_publisher``: publish Pepper pose as ``/tf``
   - ``/rosout``: the log-aggregator part of `roscore` (always here).

#. Investigate the ``/naoqi_joint_states`` node using the ``rosnode info <node_name>`` command:

   #. Which topics does it subscribe to (if any)?
   #. Which topics does it publish on (if any)?
   #. Which services does it advertise (if any)?
   #. Which other nodes is it connected to (if any)?

   .. hint::
      You can use the ``-q`` flags if you only want topic and service information.
      However, notice how, without this flag, you know which other node is actually connected to this one.

   .. hint::
      The ``/rosout`` topic is particular and all nodes connect to them.
      It is the topic on which all nodes publish their logs (controled by the logger levels, which can be manipulated using the ``/node_name/set_logger_level`` service or, more conveniently, by ``rqt_console``).
      Normally, the only node listening to it is ``/rosout`` which will republish them in aggregated form on ``/rosout_agg`` for subsequent analysis if desired.

   .. hint::
      The ``*/get_loggers`` and ``*/set_logger_level`` services are used to control the log messages sent by the node.

#. Same questions with the ``/robot_state_publisher``, ``/pepper_tts_driver``, and ``/naoqi_moveto`` nodes.

#. A useful tool is the ``rqt_graph`` command.
   Run this command and, in the graphical window it opens, switch the drop-down control on the top left from ``Nodes only`` to ``Nodes/Topics (all)`` and uncheck the ``Hide: Dead sinks`` and ``Hide: Leaf topics`` boxes.

   You should get a graphical view of the topics (rectangles) and nodes (ellipses) as well as their connections.
   In the current state, most nodes are disconnected from each other since they mostly provide a ROS interface to the robot.

   .. hint::
      From here, it is sometimes easier to see what happens and, more importantly, when something goes wrong, which node is absent or not connected to the topic it should.


Topics
------

As with nodes, it is possible to list and get information on topics.

Topic information
^^^^^^^^^^^^^^^^^

#. List the topics using:

   .. code-block:: none
      :class: command-line

      rostopic list

   It lists the topics that the node declared either subscribing or publishing to; you should see:

   - ``/cmd_vel``: velocity command for the Pepper base,
   - ``/imu``: inertial measurement unit values,
   - ``/joint_states``: values for Pepper joint angles,
   - ``/landmark_detection`` and ``/landmarks``: detected visual markers (two different formats),
   - ``/laser/*/*``: laser data (two different formats),
   - ``/move_base_simple/goal``: goal for onboard navigation,
   - ``/odom``: odometry of the Pepper base,
   - ``/pepper_navigation/front``: some laser information?,
   - ``/pepper_robot/camera/*``: camera images and parameters,
   - ``/pepper_robot/pose/*``: actions for pose control,
   - ``/pepper_robot/sonar/*``: sonar sensor values,
   - ``/rosout`` and ``/rosout_agg``: ROS logs,
   - ``/text_to_speech``: topic to make the Pepper speak,
   - ``/tf`` and ``/tf_static``: transformations between frames.

#. As for nodes, you can use ``rostopic info <topic_name>`` to get more information.
   Investigate the ``/joint_states`` topic:

   #. Which nodes publish to it?
   #. Which nodes subscribe to it?
   #. What is its type?

   .. hint::
      You can use the ``rosmsg show <message_type>`` to know the content of a given message type.
      For instance:

      .. code-block:: none
         :class: command-line

         rosmsg show sensor_msgs/JointState

      .. code-block:: none

         std_msgs/Header header
           uint32 seq
           time stamp
           string frame_id
         string[] name
         float64[] position
         float64[] velocity
         float64[] effort

      It tells us that a ``sensor_msgs/JointState`` message is composed of a header (which is itself composed of a integer, a time stamp and a frame identifier), a list of joint names, and three lists of numbers: the position, velocity, and effort of each joint.

      For most messages, their definition can be also found on the ROS website, for instance: `sensor_msgs/JointState <https://docs.ros.org/en/api/sensor_msgs/html/msg/JointState.html>`__.
      Message types defined in ``pepper_driver_ext_msgs`` are documented in :doc:`/messages`.

#. Same questions for ``/cmd_vel``, ``/odom``, ``/laser/srd_front/scan``, and ``/text_to_speech``


Reading messages
^^^^^^^^^^^^^^^^

It is also possible to check individual messages being published on topics.

#. Look at the ``/odom`` topic using:

   .. code-block:: none
      :class: command-line

      rostopic echo /odom

   You should see an endless series of `nav_msgs/Odom <https://docs.ros.org/en/api/nav_msgs/html/msg/Odometry.html>`__ messages separated by ``---``.
   If you gently push the robot around, you should see the pose and velocities change.

#. Look at a message on the ``/joint_states`` topic and see what information it contains.

   .. hint::
      You can specify the number of messages to echo with the ``-n`` flag (for instance ``-n 1``).

#. Same question for ``/pepper_robot/sonar/front/sonar``.


Publishing messages
^^^^^^^^^^^^^^^^^^^

Finally, you can publish a message directly on the command line.


Speech
""""""

Using topics, one can make the robot speak.

#. In one terminal window, monitor the ``/text_to_speech`` topic (keep it visible). 
#. In another terminal window, make the robot say a sentence using:

   .. code-block:: none
      :class: command-line

      rostopic pub /text_to_speech std_msgs/String "data: 'Hello'"

   You should see the message appear in your ``rostopic echo`` terminal and you should hear the robot say “Hello”.

#. Check the options of ``rostopic pub`` using:

   .. code-block:: none
      :class: command-line

      rostopic pub --help

   .. hint::
      This is the most common command-line flag for command-line programs to display usage information.

   #. Use the ``--once`` flag to say “Bonjour” and exit.
   #. Make the robot say “Top” every 2 seconds.
   #. Make it stop!


Motion
""""""

Now we will make the robot move.

   .. note::
      The Pepper is designed to not move when plugged in.
      This is done by deactivating the wheel motors when the charging trapdoor is not closed.

   .. danger::
      Be especially careful when moving the robot: somebody should be around to prevent damages (either to people, the environment, or the robot itself).
      Commands should always be thoroughly considered before being sent to the robot and the robot should have space to maneuver.

#. Send a null velocity command once:

   .. code-block:: none
      :class: command-line

      rostopic pub --once /cmd_vel geometry_msgs/Twist "linear:
        x: 0.0
        y: 0.0
        z: 0.0
      angular:
        x: 0.0
        y: 0.0
        z: 0.0"

   .. hint::
      It is a good practice to start sending null commands.
      In particular, it allows faster stopping of the robot by recalling the previous command line.

#. Make the robot turn counterclockwise at 0.5 Rad/s using:

   .. code-block:: none
      :class: command-line

      rostopic pub --once /cmd_vel geometry_msgs/Twist "linear:
        x: 0.0
        y: 0.0
        z: 0.0
      angular:
        x: 0.0
        y: 0.0
        z: 0.5"

#. Stop it (by sending a null command) then make it turn clockwise then stop it.
#. Make the robot move forward at 0.10 m/s then stop the robot.


Services
--------

Service information
^^^^^^^^^^^^^^^^^^^

Finally, we can investigate services as we did with nodes and topics.

#. List the services using:

   .. code-block:: none
      :class: command-line

      rosservice list

   It lists the services that are advertised by the nodes; you should see:

   - ``/{del|get|set}_memory_*``: handle Pepper memory,
   - ``/{get|set}_autonomous_life_level``: see or change the level of Pepper's autonomous life,
   - ``/{get|set}_language``: see or change the language of dialogs on Pepper,
   - ``/list_dialog_topics``: list topics of Pepper's dialog engine,
   - ``/{load|manage}_dialog_topic``: load and manage topics of Pepper's dialog engine,
   - ``/manage_data_stream``: connect Pepper memory to ROS topics,
   - ``/*/get_loggers``, ``/*/set_logger_level``: manage log messages sent,
   - ``/text_to_speech``: make Pepper speak.

#. As for nodes and topics, you can use ``rosservice info <service_name>`` to get more information.
   Investigate the ``/text_to_speech`` service:

   #. Which node provides it?
   #. What is its type?
   #. What are its arguments?

   .. hint::
      As with messages, you can use ``rossrv show <service_type>`` to know the arguments and output of a given service type.
      For instance:

      .. code-block:: none
         :class: command-line

         rossrv show pepper_driver_ext_msgs/TextToSpeech

      .. code-block:: none

         string text
         ---
         bool success
         string error_message

      It tells us that the arguments of a ``pepper_driver_ext_msgs/TextToSpeech`` request consist of a single string (the text to speak aloud), and that the response to a request consists in a success Boolean and, if needed an error message.

      For most services, their definition can be also found on the ROS website, for instance: `std_srvs/SetBool <http://docs.ros.org/en/api/std_srvs/html/srv/SetBool.html>`__.
      Service types defined in ``pepper_driver_ext_msgs`` are documented in :doc:`/messages`.

#. Same questions for ``/get_language``, ``/set_language``, ``/load_dialog_topic``, and ``/list_dialog_topics``.


Service call
^^^^^^^^^^^^

Contrary to topics, one cannot listen to service requests or responses made by other nodes.
However, one can call services from the command line.

#. Check the current language of the dialog engine using:

   .. code-block:: none
      :class: command-line

      rosservice call /get_language "{}"

   .. code-block:: none

      success: True
      error_message: ""
      language: "French"

#. Change the language to ``English`` using the ``/set_language`` service.
#. Use the ``/text_to_speech`` *service* (and not topic) to make the robot say “Hello” then “Bonjour” and listen to its accent.
#. Switch back the language to ``French`` and listen to the robot say “Hello” and “Bonjour” again.
   Did you notice a difference?


Stopping drivers
----------------

Care should be taken to always stop the robot drivers at the end of the session.

#. Switch to the docker terminal where the drivers are launched.
#. Press ``ctrl+c`` to terminate the launch files with the drivers.
#. When finished press ``ctrl+d`` to exit the docker container.


.. admonition:: Take-home message

   At the start of this tutorial, you learned to :ref:`launch the drivers <actual_launching_ros1>` of the Pepper robot.

   Then, you have learned to investigate nodes using ``rosnode list`` and ``rosnode info`` and to check their connectivity using ``rqt_graph``.
   You have also learned to investigate topics using ``rostopic list`` and ``rostopic info`` and to listen or publish to them using respectively ``rostopic echo`` or ``rostopic pub``.
   Similarly, you have used ``rosservice list``, ``rosservice info``, and ``rosservice call`` to, respectively, list, get information, and use ROS services.
