.. sectnum::
   :prefix: T
   :depth: 3
   :start: 2

.. highlight:: none


Pepper teleoperation
====================

.. note::
   This tutorial is only for ROS 1; for ROS 2, check :doc:`/tutorials_ros2/t2_pepper_teleop`.

The aim of this tutorial is to create a ROS package with a Python node and a launch file to start it.
The node will allow joypad teleoperation of the Pepper robot base.

This tutorial is split into four parts:

- `general design`_ of the functionality,
- creation of a `ROS package`_,
- implementation of the `teleoperation node`_,
- and creation of a `launch file`_.

.. important::
   You need to have done the previous two tutorials: :doc:`/tutorials_ros1/t0_basic_setup` and :doc:`/tutorials_ros1/t1_pepper_discovery`.
   
   You will also need a joypad or a joystick and access to a Pepper robot (with its IP address).
   The joystick drivers are available in the following system package: ``ros-noetic-joy`` (installable with ``apt``).


General design
--------------

We want to be able to move the Pepper robot around using a joypad.

In the :doc:`/tutorials_ros1/t1_pepper_discovery` tutorial, we have seen that we can move the robot by sending a `geometry_msgs/Twist <https://docs.ros.org/en/api/geometry_msgs/html/msg/Twist.html>`__ message on the ``/cmd_vel`` topic.
How can we send such messages with a joypad?


Joystick
^^^^^^^^

First, we need to be able to receive events from the joypad.
Luckily, there is generic ROS driver for joysticks: ``joy_node`` in the `joy <https://wiki.ros.org/joy>`__ package.

#. Start `roscore` in a terminal window:

   .. code-block:: none
      :class: command-line

      roscore

   .. hint::
      `roscore` is automatically launched by `roslaunch` if needed, which is why we didn't have to do it in the previous tutorial.

#. In another terminal window start the joystick node:

   .. code-block:: none
      :class: command-line

      rosrun joy joy_node

   .. hint::
      You should see a warning::

          [ WARN] [1670330837.280009000]: Couldn't set gain on joystick force feedback: Bad file descriptor

      Ignore it for the moment, we will deal with it later.

#. Inspect the node:

   #. Which topic(s) does it publish on?
   #. With which message type?

#. Look at the messages while you move the axes and press the buttons.

   #. Which axis do you want to use to make the robot move forward/backward? Note down its index.
   #. Same question for left/right rotation.
   #. Same question for left/right translation.

#. The maximum translation velocity of the Pepper is 0.35 m/s and its maximum angular velocity is 1 Rad/s.
   Deduce the formulas to compute the following ``Twist`` attributes from the ``Joy.axes`` values:

   - ``linear.x``
   - ``linear.y``
   - ``linear.z``
   - ``angular.x``
   - ``angular.y``
   - ``angular.z``

   .. hint::
      Always be mindful of units.
      ``Joy`` values are unitless between -1 and 1, while ``Twist`` values are in m/s or rad/s.
      Therefore, you need multiplicative constants to ensure unit homogeneity of your equations.


Design
^^^^^^

We want to listen to the ``/joy`` messages and publish commands on the ``/cmd_vel`` topic.
For this, we will create a node which subscribes to ``/joy`` (with type `sensor_msgs/Joy <https://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Joy.html>`__) computes a relevant command (based on parameters) and publishes it on ``/cmd_vel`` (with type `geometry_msgs/Twist <https://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/Twist.html>`__.

The simplified design is illustrated below:

.. math::
   :nowrap:

   \begin{tikzpicture}[node distance=2cm]
       \node (teleop) [rectangle split, draw, rectangle split parts=3, text width=4.5cm, align=center]
           {
             \texttt{\textbf{teleop\_joy}}
             \nodepart[align=left]{two}
             \texttt{joy\_cb}
             \nodepart[align=right]{three}
             \texttt{cmd\_vel\_pub}
           };

       \path (teleop.two west) node (joy) at +(-2cm,0) [anchor=east] {\texttt{/joy}};
       \path (teleop.three east) node (cmd_vel) at +(2cm,0) [anchor=west] {\texttt{/cmd\_vel}};

       \draw [->, thick] (joy.east) .. controls +(right:0.5cm) and +(left:0.5cm) .. (teleop.two west);
       \draw [->, thick] (teleop.three east) .. controls +(right:0.5cm) and +(left:0.5cm) .. (cmd_vel.west);
   \end{tikzpicture}

Concretely, after initialization, the node will wait for messages and, when one arrives, a `callback <https://en.wikipedia.org/wiki/Callback_(computer_programming)>`__ function will be called to handle that `event <https://en.wikipedia.org/wiki/Event-driven_programming>`__.
In our case, we want that function to compute a new command and to publish it immediately.
Publication is made using a `rospy.Publisher <https://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers#Publishing_to_a_topic>`__ object that should be created first and used for every message (instead of recreating it for every message).
It should therefore be retained as a member of a class.

The class design is therefore:

.. math::
   :nowrap:

   \begin{tikzpicture}[node distance=2cm]
       \node (teleop_class) [rectangle split, draw, rectangle split parts=3, text width=6cm, align=center]
           {
             \texttt{\textbf{PepperTeleop}}
             \nodepart[align=left]{two}
             \texttt{cmd\_vel\_pub: Publisher}
             \nodepart[align=left]{three}
             \color{mygrey}
             \texttt{\_\_init\_\_()}\\
             \color{black}
             \texttt{joy\_cb(msg)}
           };
   \end{tikzpicture}


.. _ros1_package:

ROS Package
-----------

We have a general design and the algorithm (the formulas).
However, before we start the implementation, we need to layout the infrastructure to properly integrate our node in the ROS ecosystem, starting with the catkin workspace.
Concretely, it means creating a ROS package, which entails specifying several files.


File hierarchy
^^^^^^^^^^^^^^

A ROS package is a directory with at least 2 required files:

- ``package.xml``: metadata on the ROS package (name, description, authors, dependencies...),
- ``CMakeLists.txt``: compilation directives for the package (message generation, compilation, installation...).

In addition, if there is a Python package, it is necessary to have a ``setup.py`` to properly handle importing.

.. note::
   Python has a concept of `package <https://docs.python.org/3/tutorial/modules.html#packages>`__, which is independent from ROS packages.
   A ROS package can include a Python package but they are conceptually distinct.
   A ROS package can have nodes, whereas a Python package will have modules which can be imported.
   In this documentation, we sometimes use `package` as a shortcut for `ROS package`.

Besides these necessary files, a ROS package can host several kinds of files: nodes, launch files, configuration, message or service definition...
While those can be placed in arbitrary locations, there are conventions to guide us.
A full package can therefore look like::

    <catkin_ws>/src/
    ├-- other_package/
    |   └-- ...
    └-- package_name/
        ├-- config/
        |   └-- config.yaml
        ├-- launch/
        |   └-- launch_file.launch
        ├-- msg/
        |   └-- some_message.msg
        ├-- nodes/
        |   └-- some_node.py*
        ├-- src/
        |   └-- package_name/
        |       ├-- __init__.py
        |       └-- some_module.py
        ├-- srv/
        |   └-- some_service.srv
        ├-- CMakeLists.txt
        ├-- package.xml
        └-- setup.py


Step-by-step
^^^^^^^^^^^^

These files can be filled by hand but there are tools to provide templates.

#. In a terminal window, go to your catkin workspace:

   .. code-block:: none
      :class: command-line

      cd ~/catkin_ws/src

#. Initialize the package using `catkin create pkg <https://catkin-tools.readthedocs.io/en/latest/verbs/catkin_create.html>`__:

   .. code-block:: none
      :class: command-line

      catkin create pkg pepper_teleop -c rospy geometry_msgs sensor_msgs

   .. hint::
      This command creates the required files and directory for a ROS package named ``pepper_teleop`` which depends on the ROS packages ``rospy``, ``geometry_msgs``, and ``sensor_msgs``.

      These dependencies are specified in the ``package.xml`` metadata file and, for the compilation, also in the ``CMakeLists.txt`` file.
      Dependencies can always be added or removed later by editing those files.

#. Edit the ``pepper_teleop/package.xml`` file to fill in the:

   - description,
   - maintainer (with your own name and email),
   - license (in doubt, you can use BSD or MIT),
   - author (don't forget to uncomment the element).

   .. hint::
      Notice how our dependencies are listed inside ``<build_depend>`` or ``<exec_depend>`` tags.
      They could be stated inside ``<depend>`` tags if this granularity is not necessary.

#. Our node will be in the ``nodes/pepper_teleop.py`` file:

   - create the ``nodes`` directory inside the ``pepper_teleop`` package,
   - create a ``nodes/pepper_teleop.py`` file with ``#!/usr/bin/env python`` as first (and only for now) line,
   - change the `permission <https://en.wikipedia.org/wiki/File-system_permissions#Traditional_Unix_permissions>`__ of this file to make it executable.

#. Edit the ``CMakeLists.txt`` file to uncomment the ``catkin_install_python`` call (around line 162) with the correct argument:

   .. code-block:: cmake

      catkin_install_python(PROGRAMS
        nodes/pepper_teleop.py
        DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
      )

#. Now, if our Python nodes need to import local modules, we need to put them in a Python package inside the ROS package and to tell catkin to recognize it.

   #. Create a ``src/pepper_teleop`` directory *inside* the ``pepper_teleop`` ROS package.
   #. Create an empty ``__init__.py`` file inside this new directory.
   #. Create a ``setup.py`` file at the root of the ``pepper_teleop`` ROS package (alongside ``CMakeLists.txt`` and ``package.xml``) with the following content:

      .. code-block:: python

         ## ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD

         from distutils.core import setup
         from catkin_pkg.python_setup import generate_distutils_setup

         # fetch values from package.xml
         setup_args = generate_distutils_setup(
             packages=['pepper_teleop'],
             package_dir={'': 'src'},
         )

         setup(**setup_args)

      .. hint::
         You can copy-paste this file for your next ROS packages but don't forget to change the package name.

   #. Uncomment the ``catkin_python_setup()`` line in ``CMakeLists.txt`` (around line 23).

#. Now your first ROS package is ready but not yet known from the ROS tools:

   .. code-block:: none
      :class: command-line

      rospack find pepper_teleop

   .. code-block:: none

      [rospack] Error: package 'pepper_teleop' not found

   We need to:

   - rebuild the catkin workspace:

     .. code-block:: none
        :class: command-line

        catkin build

   - source the updated ``setup.bash``:

     .. code-block:: none
        :class: command-line

        source ~/catkin_ws/devel/setup.bash

#. Check ``pepper_teleop`` is now known:

   .. code-block:: none
      :class: command-line

      rospack find pepper_teleop

#. Run your (empty) node:

   .. code-block:: none
      :class: command-line

      rosrun pepper_teleop pepper_teleop.py

   .. hint::
      Obviously it should not do anything but it should not fail either (you should still have ``roscore`` running).

.. hint::
   You can change the dependencies or add new Python nodes or messages, etc. at any time: you just need to update the ``package.xml`` and ``CMakeLists.txt`` as required.
   When that happens, you need to rebuild the workspace and source again the ``setup.bash``.


.. _ros1_package_checklist:

ROS Package checklist
^^^^^^^^^^^^^^^^^^^^^
For reference for future packages, here are the things to complete and check after the creation of a package with ``catkin create pkg package_name -c dep1 dep2 ... depN``:

- dependencies in ``package.xml`` and ``CMakeLists.txt``
- module files in ``src/package_name/``
- ``src/package_name/__init__.py`` (usually empty)
- ``setup.py`` with proper content
- ``catkin_python_setup()`` call in ``CMakeLists.txt``
- proper shebang in nodes: ``#!/usr/bin/env python``
- nodes made executable: ``chmod a+x nodes/some_node.py`` (in the command line)
- ``catkin_install_python(...)`` call in ``CMakeLists.txt``
- rebuild the workspace: ``catkin build``
- source the ``setup.bash``: ``source ~/catkin_ws/devel/setup.bash``



Teleoperation node
------------------

Now that we have a properly recognized package we can finally implement our node.
It will be composed of a single class instantiated by a ``main`` function.
That function will also keep the node alive by sleeping until the node is asked to terminate, so that messages can be processed by the class.

In the first part, we will write the class before writing the rest of the node in the second part.


Class
^^^^^

Recall the design of the class:

.. math::
   :nowrap:

   \begin{tikzpicture}[node distance=2cm]
       \node (teleop_class) [rectangle split, draw, rectangle split parts=3, text width=6cm, align=center]
           {
             \texttt{\textbf{PepperTeleop}}
             \nodepart[align=left]{two}
             \texttt{cmd\_vel\_pub: Publisher}
             \nodepart[align=left]{three}
             \color{mygrey}
             \texttt{\_\_init\_\_()}\\
             \color{black}
             \texttt{joy\_cb(msg)}
           };
   \end{tikzpicture}

There is a single attribute and two methods.
The ``__init__`` method is a `special method <https://docs.python.org/3/reference/datamodel.html#object.__init__>`__ in Python called at the object creation in order typically to create its attributes.
In our case, we will use it to create our `publisher <http://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers#Publishing_to_a_topic>`__ and to `subscribe <https://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers#Subscribing_to_a_topic>`__ to the ``/joy`` topic.
The second method ``joy_cb`` will be called by the ROS event loop when a message arrives and will compute the command and send it as a message using the publisher.

#. Edit the ``nodes/pepper_teleop.py`` file to create the class with the following ``__init__`` code:

   .. code-block:: python

      class PepperTeleop:
          """Joystick teleoperation for Pepper robot."""

          def __init__(self):
              """Create publisher and subscriber."""
              # publisher
              self.cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
              # subscriber
              self.joy_sub = rospy.Subscriber('/joy', Joy, self.joy_cb)

   .. important::
      Be mindful of indentation, we *strongly* advise to stick to 4-space indentation.

#. Complete the class with the ``joy_cb`` method:

   .. code-block:: python

      class PepperTeleop:
          """Joystick teleoperation for Pepper robot."""

          ...

          def joy_cb(self, msg):
              """Compute and publish twist command."""
              # initialize message
              cmd_vel_msg = Twist()
              # fill message
              cmd_vel_msg.linear.x = ...
              cmd_vel_msg.linear.y = ...
              cmd_vel_msg.linear.z = ...
              cmd_vel_msg.angular.x = ...
              cmd_vel_msg.angular.y = ...
              cmd_vel_msg.angular.z = ...
              # publish message
              self.cmd_vel_pub.publish(cmd_vel_msg)

   .. hint::
      To get the value of, for instance, the third axis in the ``msg`` message of type `sensor_msgs/Joy <http://docs.ros.org/en/api/sensor_msgs/html/msg/Joy.html>`__, you can simply write ``msg.axes[2]``.
      Similarly, the first button is ``msg.buttons[0]``.

#. Complete the message-filling part according to your equations from `Joystick`_.


Node
^^^^

In Python, a class or function definition is a statement that is executed when the module is executed.
A class definition makes the class known to Python but does not create an instance of the class in the same way that a function definition does not execute the function.

The rest of the ``nodes/pepper_teleop.py`` file is therefore responsible for instantiating the class and importing the relevant modules and classes.
In particular, we will create a ``main`` function to initialize the node, instantiate an object of the class ``PepperTeleop``, and wait until the node is closed.
We will call this function in an idiomatic way.

#. At the start of the file, you should already have:

   .. code-block:: python

      #!/usr/bin/env python

   .. hint::
      This is the `shebang <https://en.wikipedia.org/wiki/Shebang_(Unix)>`__ line which indicates which interpreter should be used to run this file.
      Magic from catkin compilation will make sure the correct Python version is used.

#. Then write the documentation of your node:

   .. code-block:: python

      """Joystick teleoperation node for the Pepper robot.

      This node computes velocity commands from joystick messages.

      It subscribes to:
          - `/joy` (sensor_msgs/Joy): joystick message.

      It publishes to:
          - `/cmd_vel` (geometry_msgs/Twist): command velocity.
      """

   .. hint::
      This is called a `docstring <https://peps.python.org/pep-0257/#what-is-a-docstring>`__.
      It is important when coding to document our code and update the documentation when changes to the code are made.

#. Then import the ``rospy`` module and the ``Joy`` and ``Twist`` classes:

   .. code-block:: python

      import rospy
      from geometry_msgs.msg import Twist
      from sensor_msgs.msg import Joy

   .. hint::
      Notice how message definitions are in the ``.msg`` module in the relevant package.
      Those are automatically generated by the catkin compilation from ``*.msg`` message definitions.

#. At the end of the file, define the main function:

   .. code-block:: python

      def main():
          """Instantiate node and class."""
          # declare node
          rospy.init_node('pepper_teleop')
          # instantiate class
          pepper_teleop = PepperTeleop()
          # run
          rospy.spin()

#. Finally, conclude your file with the call to your ``main`` function:

   .. code-block:: python

      if __name__ == '__main__':
          main()

   .. hint::
      This condition basically states to call the function if the file is executed as a standalone program but not when it is loaded as a module.
      See `this answer <https://stackoverflow.com/a/419185>`__ for more details.

#. Save your file.

   .. hint::
      As it is a Python node, you do not need to rebuild the catkin workspace.
      That would be needed for new dependencies and message definitions.


Tests
^^^^^

.. role:: command(code)
   :class: command-line

We will first test our node in isolation before using it to make the robot move.

#. Normally, you should have a terminal window with ``roscore`` running and another one with ``joy_node``.
   If it is not the case any more, make it so.
#. Make sure the Pepper is not connected by listing the nodes: you should only have ``/joy_node`` and ``/rosout``.
#. Ready three additional terminal windows.
   In the first two run respectively:

   - :command:`rostopic echo /joy`
   - :command:`rostopic echo /cmd_vel`

#. Press buttons and move joypad axes.
   Check that the ``/joy`` messages are published as expected.
#. In the last terminal window, start your node:

   .. code-block:: none
      :class: command-line

      rosrun pepper_teleop pepper_teleop.py

#. Move the joypad axes and check values are correct.

   .. important::
      In particular, make sure the velocity commands published are not too large.
      For the Pepper it should not exceed 0.35 m/s and 1.0 rad/s.

#. If everything is correct, stop both ``rostopic echo`` commands and, instead, launch the robot (see above).
#. Use the joypad to make the robot move.

   .. danger::
      Be very careful when moving the robot.

#. Terminate all nodes and programs.
   Don't forget to close ``roscore`` as well.

   .. hint::
      Congratulations for your first ROS node!


Launch file
-----------

Now, it is a bit tedious to have to run separately all these commands.
We run the robot drivers using one of the provided :doc:`/launch_files`, which prevents starting dozens of commands in as many terminals.
The objective of this last part, is to write a small launch file to launch the joystick teleoperation.
Doing so, we will learn about ROS parameters and launch file arguments.

A launch file is an `XML <https://en.wikipedia.org/wiki/XML>`__ file with a specific `set of tags and attributes <https://wiki.ros.org/roslaunch/XML>`__ which is interpreted by the `roslaunch <https://wiki.ros.org/roslaunch>`__ tool.
Launch files are typically named with a ``.launch`` extension.

#. Go to the directory of your ``pepper_teleop`` ROS package:

   .. code-block:: none
      :class: command-line

      roscd pepper_teleop

#. Create a ``launch`` directory to put your launch file.
#. The root element of launch files always has the `<launch> <https://wiki.ros.org/roslaunch/XML/launch>`__ tag.
   In your ``launch/`` directory, create a ``pepper_teleop.launch`` file with the following content:

   .. literalinclude:: ../../pepper_teleop/launch/pepper_teleop.launch
      :language: xml
      :lines: 1,19

   .. hint::
      Notice how, in XML, an element is closed with a ``/``.
      It is similar to `HTML <https://en.wikipedia.org/wiki/HTML>`__ as both derive from `SGML <https://en.wikipedia.org/wiki/Standard_Generalized_Markup_Language>`__.

      In these languages, there are elements denoted with tags in a tree-like hierarchy.
      Each element can have attributes which have values (in quotes).
      An example can be:

      .. code-block:: xml

         <course id="M2_AVR">
             <name>Intégration Méthodologiques</name>
             <!-- TODO: check duration -->
             <duration>36 heures</duration>
             <lecturer name="Colas" firstname="Francis"/>
             <lecturer name="Colotte" firstname="Vincent"/>
             <sessions>
                 <session id="1">Introduction + Interaction multimodale</session>
                 <session id="2">ROS + Navigation</session>
             </sessions>
         </course>

      In this example, the root element has the tag ``course`` with the attribute ``id`` which has the value ``M2_AVR`` (quoted).
      This element has five children.
      The ``<!-- ... -->`` construct is an XML comment.

      Note that the elements with children are written ``<tag>...</tag>`` whereas empty elements (no children but can have attributes) can be written ``<tag ... />``.

#. The first node we want to run is ours.
   This can be done using the `<node> <https://wiki.ros.org/roslaunch/XML/node>`__ with the package, the type, and the name as attributes.

   #. Insert the following as child of the ``<launch>`` tag:

      .. literalinclude:: ../../pepper_teleop/launch/pepper_teleop.launch
         :language: xml
         :lines: 14,15,18

      .. hint::
         XML is like code, you want to put comments to make things clearer.

   #. Save your file and launch it (make sure no other ROS node is running):

      .. code-block:: none
         :class: command-line

         roslaunch pepper_teleop pepper_teleop.launch

      .. hint::
         This command launches the ``pepper_teleop.launch`` file found in the ``pepper_teleop`` ROS package.

      .. hint::
         Remember that you do not need to start `roscore` before since the ``roslaunch`` command will start it if it is not there yet.
         However, if `roscore` is already running, `roslaunch` will not start a new one.

   #. List the nodes in another terminal window.
   #. Stop the launch file by pressing ``ctrl-c`` in the terminal window you launched it.

#. Our node is useless without the joystick driver.

   #. Add the ``<node>`` element to launch the joystick driver.
   #. Test that it is properly launched and that the nodes are connected.

      .. hint::
         You can opt to set the ``output`` attribute to the value ``screen`` in this node.
         This will display all log output of the node in the terminal.
         This can help debug.

#. There can be several joystick devices connected to a computer and, fortunately, there is a way to choose and tell ``joy_node`` using `ROS parameters <https://wiki.ros.org/Parameter%20Server>`__.

   #. First, list all joysticks devices:

      .. code-block:: none
         :class: command-line

         ls /dev/input/js*

      .. hint::
         Even if you have only one joystick device (``/dev/input/js0``), continue this part since ROS parameters are used quite a lot. 

   #. Use the `jstest <https://manpages.ubuntu.com/manpages/focal/man1/jstest.1.html>`__ tool to find the device file corresponding to your joypad.

   #. Stop all nodes and launch files and start ``roscore`` in new terminal window.
      In another terminal window list the parameters with:

      .. code-block:: none
         :class: command-line

         rosparam list

      .. hint::
         You should see four parameters including the URI of roscore (``/roslaunch/uris/host_...``) as well as distribution and version information.

      .. hint::
         Remember: `roscore` is three things:

         - the `rosmaster` to keep track of nodes, topics, subscriptions...
         - the log aggregator (shown as the ``/rosout`` node in the list of nodes)
         - the parameter server.

         It means that parameters are kept while `roscore` is running and are forgotten when it stops.

         Remember also that, if `roscore` is not running, `roslaunch` will start it.

   #. Set the parameter for the joystick device using:

      .. code-block:: none
         :class: command-line

         rosparam set /joy_node/dev /dev/input/js2

      .. hint::
         Change the command above with your joystick device file.

   #. Check the parameter is correctly set with:

      .. code-block:: none
         :class: command-line

         rosparam get /joy_node/dev

   #. Without quitting `roscore`, launch your launch file and check that it is using the correct device (by looking at the ``/joy`` messages for instance).

#. It would be tedious to have to specify every parameter for every node manually before launching a launch file.
   Fortunately, there are two ways to set parameters in a launch file: with the `<param> <https://wiki.ros.org/roslaunch/XML/param>`__ and `<rosparam> <https://wiki.ros.org/roslaunch/XML/rosparam>`__ tags.
   The first one specifies a single parameter while the second loads parameters from a file.

   We will specify some parameters for `joy_node <https://wiki.ros.org/joy#joy_node>`_.
   In particular, we want to provide the right device, but also tell it to not be too sensitive to small values and to be quiet about the force-feedback warning we've seen above.

   #. Insert a ``param`` element in your `joy_node` ``<node>`` element to set the ``deadzone`` to a value of ``0.1``:

      .. literalinclude:: ../../pepper_teleop/launch/pepper_teleop.launch
         :language: xml
         :lines: 9

      .. hint::
         This tells the joystick driver to not consider axes values of less than 0.1.
         This is useful for not constantly sending very small commands.

      .. hint::
         A child in XML must be put before the closing of the element with ``</tag>``.
         Remember that ``<tag />`` is a shortcut for ``<tag></tag>`` so make sure to check your elements are not closed too early.

      .. hint::
         Make use of indentation to have a visual indication of the nesting levels of your elements.

         .. hint::
            Code editors such as Visual Studio Code often propose a way to automate indentation.
            With Visual Studio Code, the ``ctrl+shift+I`` key combination will fix indentation of the current file.
            If no formatter is available for your current file type, it will ask you to install one.

   #. Insert another ``param`` element to set ``dev_ff`` to an empty value (``value=""``).
   #. In the same way, insert a last ``param`` element for the ``dev`` ROS parameter.

      .. hint::
         The full name of this ROS parameter is ``/joy_node/dev`` since the node is called ``/joy_node`` because ``dev`` is a so-called `private` parameter (often written ``~dev``), that is, it is looked for in the namespace of the node.

         This leaves the possibility to run two joystick-driver nodes for different devices.
         Nodes must always have different names (otherwise the first one is killed by the `rosmaster`) and thus two joystick drivers have different private parameters.

   #. Quit `roscore` and launch your new launch file.
   #. Check that the joystick node is running with the correct device and parameters (it tells you).

#. This is nice but, if we want to change device, it is not very practical to have to change the contents of a file.
   We would like instead to be able to provide some arguments when launching (like we did for the simulation).

   This can be done by declaring an argument with the `<arg> <https://wiki.ros.org/roslaunch/XML/arg>`__ tag and using the values of this argument to set the parameter.

   #. Declare a ``joy_dev`` argument as child of the root ``<launch>`` tag:

      .. literalinclude:: ../../pepper_teleop/launch/pepper_teleop.launch
         :language: xml
         :lines: 2,3

      .. hint::
         It is a good idea to group all argument declarations near the top of the launch file.

      .. hint::
         Arguments do not need to have a default value.
         In that case, an error will be raised if the value is not provided at launch time.

   #. Change the value of the ``dev`` param in your ``joy_node`` so that it uses the value of the argument:

      .. literalinclude:: ../../pepper_teleop/launch/pepper_teleop.launch
         :language: xml
         :lines: 11

      .. hint::
         This ``$(...)`` syntax (called `substitution args <https://wiki.ros.org/roslaunch/XML#substitution_args>`__) is specific to `roslaunch` (it is not generic XML) and allows us to use some convenient functions (``arg foo`` to get the value of the ``foo`` launch-file argument in the present case).

   #. Launch your saved launch file to `pass <https://wiki.ros.org/roslaunch/Commandline%20Tools#Passing_in_args>`__ the ``joy_dev`` argument:

      .. code-block:: none
         :class: command-line

         roslaunch pepper_teleop pepper_teleop.launch joy_dev:=/dev/input/js2

   #. Check that the correct device is used.

#. With this launch file, we pass parameters to the ``joy_node`` node.
   Let's do the same to pass the maximum velocities to our node.

   #. Define two arguments ``max_lin_vel`` and ``max_ang_vel`` for your launch file (with proper default and description).
   #. Pass those arguments as parameters to the ``pepper_teleop`` node in the launch file.
   #. We can use `rospy.get_param <https://wiki.ros.org/rospy/Overview/Parameter%20Server#Getting_parameters>`__ to get the value of a parameter from the node.
      In the ``__init__`` method add the following:

      .. code-block:: python

         # parameters
         self.max_lin_vel = rospy.get_param('~max_lin_vel', 0.35)
         self.max_ang_vel = rospy.get_param('~max_ang_vel', 1.0)

   #. Modify your ``joy_cb`` callback method to use those parameters.

      .. hint::
         It is actually good practice to have as few literal constants in the code but use parameters whenever relevent, even for constants that will not be modified in practice.

   #. Modify the module docstring to include::

          Parameters:
              - `max_lin_vel`: maximum linear velocity in m/s
                  (default 0.35).
              - `max_ang_vel`: maximum angular velocity in rad/s
                  (default 1.0).

      .. hint::
         It is very important to keep the documentation synchronized with the code.

   #. Launch your launch file.


   .. hint::
      Your final launch file should look like:

      .. literalinclude:: ../../pepper_teleop/launch/pepper_teleop.launch
         :language: xml

      Your node should look like:

      .. literalinclude:: ../../pepper_teleop/nodes/pepper_teleop.py
         :language: python

.. admonition:: Take-home message

   In this tutorial, you've learned to create a ROS package, to write a simple node, and to write a launch file.

