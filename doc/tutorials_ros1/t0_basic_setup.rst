.. sectnum::
   :prefix: T
   :depth: 3
   :start: 0

.. highlight:: none


Basic setup and catkin workspace
================================

.. note::
   This tutorial is only for ROS 1; for ROS 2, check :doc:`/tutorials_ros2/t0_basic_setup`.

The aim of this tutorial is to explain the basic setup to use ``pepper_driver_ext``.
It is a longer and step-by-step version of the :doc:`/installation` instructions aimed at ROS beginners.
In particular, it details the environment and catkin workspace for new packages.

The target setup is `Ubuntu 20.04 <https://releases.ubuntu.com/20.04/>`__ with `ROS noetic <http://wiki.ros.org/noetic>`__ and `docker <https://www.docker.com>`__ (`prerequisites`_).
The drivers are running in a docker container while user nodes are run on the host.
The latter part requires installing the ``pepper_driver_ext_msgs`` message definitions on the host in a `catkin workspace`_.

.. hint::
   You don't need a robot for this tutorial: it stops right before launching the robot (see :doc:`/tutorials_ros1/t1_pepper_discovery`).

Prerequisites
-------------

.. important::
   This section requires sudoer rights on the computer.

   In the HP_I240 room, ROS and docker are already installed with additional packages.
   You can therefore skip this whole section to jump to `ROS environment`_.

You need to:

#. Install `Ubuntu 20.04 <https://releases.ubuntu.com/20.04/>`__: for instance, by following `this tutorial <https://ubuntu.com/tutorials/install-ubuntu-desktop>`__.
#. Install `ROS noetic <http://wiki.ros.org/noetic/Installation/Ubuntu>`__ using, at step 1.4, ``ros-noetic-desktop-full``.
   
   .. note::
      You may skip the `environment setup <http://wiki.ros.org/noetic/Installation/Ubuntu#noetic.2FInstallation.2FDebEnvironment.Environment_setup>`__ step as it will be detailed and explained below.

#. Install `Docker Engine <https://docs.docker.com/engine/install/ubuntu/>`__ and setup your user to run ``docker`` without ``sudo`` by following the first `post-installation step for Linux <https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user>`__.

   .. note::
      Other post-installation steps are not as important for us here.

#. Install additional packages:

   .. code-block:: none
      :class: command-line

      sudo apt install python3-catkin-tools


ROS environment
---------------


A key question is the location of files and executables and, hence, how tools can find them.
This is mostly handled with `environment variables <https://en.wikipedia.org/wiki/Environment_variable>`__.

By default, ROS commands are not located where the system looks for executables (defined in the ``PATH`` environment variable).
Therefore commands like ``roscore``, ``rosrun``, ``rostopic``, etc. are not available at the start event if they are installed.
Moreover compilation using `catkin` requires some environment variables to be set.
This was done so that one could have several ROS distributions on the same system.

.. hint::
   You can check commands are not accessible by running, for instance:

   .. code-block:: none
      :class: command-line

      rosrun
    
   You should see::

       bash: rosrun: command not found

   However if you use their full path, you'll see they are installed:

   .. code-block:: none
      :class: command-line

      /opt/ros/noetic/bin/rosrun

   This should display the usage of this command::

       Usage: rosrun [--prefix cmd] [--debug] PACKAGE EXECUTABLE [ARGS]
         rosrun will locate PACKAGE and try to find
         an executable named EXECUTABLE in the PACKAGE tree.
         If it finds it, it will run it with ARGS.


Therefore, before starting to work in ROS, it is necessary to properly setup the environment.
This is done by the ``/opt/ros/noetic/setup.bash`` script which needs to be `sourced <https://linuxcommand.org/lc3_man_pages/sourceh.html>`__ using:

.. code-block:: none
   :class: command-line

   source /opt/ros/noetic/setup.bash

It needs to be done in each shell, which is easy to forget.
Fortunately, there is a way to have the shell execute a command when it starts: putting that command in the ``.bashrc`` file located in the `home directory <https://en.wikipedia.org/wiki/Home_directory>`__ of the user (its shortcut in the shell is the `~ <https://en.wikipedia.org/wiki/Tilde#Directories_and_URLs>`__ character, which you will find prefixing several paths on this page).
This can be done by editing this file using a text editor (like `vim <https://en.wikipedia.org/wiki/Vim_(text_editor)>`__ or `nano <https://en.wikipedia.org/wiki/GNU_nano>`__) or, simply, using the following command:

.. code-block:: none
   :class: command-line

   echo -e "\n# ROS setup\nif [ -f /opt/ros/noetic/setup.bash ]; then\n    source /opt/ros/noetic/setup.bash\nfi" >> ~/.bashrc

.. hint::

   You can check the last few line of a file with the `tail <https://manpages.ubuntu.com/manpages/focal/en/man1/tail.1.html>`__ command:

   .. code-block:: none
      :class: command-line
      
      tail -4 ~/.bashrc

   .. code-block:: none

      # ROS setup
      if [ -f /opt/ros/noetic/setup.bash ]; then
          source /opt/ros/noetic/setup.bash
      fi

   .. note::
      The ``if [ -f ... ]`` construct checks that the file exists before attempting to source it.
      It prevents errors when sharing the same ``.bashrc`` across different computers.

Once that is done, every subsequent shell should be properly configured.
This can be checked by looking at various environment variables such as ``ROS_DISTRO``, ``ROS_ROOT``, or ``ROS_PACKAGE_PATH``:

.. code-block:: none
   :class: command-line

   printenv ROS_PACKAGE_PATH

.. code-block:: none

   /opt/ros/noetic/share

.. code-block:: none
   :class: command-line

   env | grep ROS

.. code-block:: none

   ROS_VERSION=1
   ROS_PYTHON_VERSION=3
   ROS_PACKAGE_PATH=/opt/ros/noetic/share
   ROSLISP_PACKAGE_DIRECTORIES=
   ROS_ETC_DIR=/opt/ros/noetic/etc/ros
   ROS_MASTER_URI=http://localhost:11311
   ROS_ROOT=/opt/ros/noetic/share/ros
   ROS_DISTRO=noetic

.. important::
   If the commands above do not return anything, it means that the sourcing was not done.
   If, later, you have unexpected errors like ``roscore: command not found``, then that is most probably the issue.


Catkin workspace
----------------

ROS works with packages put into workspaces, or `catkin workspaces`.
The ROS packages installed on the system are in the distribution workspace (under ``/opt/ros/noetic``).
But it is not where we will work.

Workspaces can be overlayed; that is, packages are looked for in a first workspace and, if it is not found, the search continues in the second workspace and so on.
Usually, we have only a user workspace and the system one but it could be more complex.


Creating the workspace
^^^^^^^^^^^^^^^^^^^^^^

A catkin workspace has a specific structure described in `REP 128 <https://www.ros.org/reps/rep-0128.html>`__.
This structure is complex but, fortunately, tools allow us to handle it easily.
The main directories we need to be concerned with are the directory where the workspace is located and where we will put source files.

We will first create an empty catkin workspace called ``catkin_ws``.
At the minimum, it needs to have a ``src`` subdirectory for the sources.
It will look like::

    /home
    └-- <user>
        └-- catkin_ws
            └-- src

These directories can be created using the `mkdir <https://manpages.ubuntu.com/manpages/focal/en/man1/mkdir.1.html>`__ command:

.. code-block:: none
   :class: command-line

   mkdir -p ~/catkin_ws/src

Now, it needs to be initialized.
We will use `catkin_tools <https://catkin-tools.readthedocs.io/en/latest/index.html>`__ commands:

.. code-block:: none
   :class: command-line

   catkin init -w ~/catkin_ws

This command doesn't seem to do much but it initializes some stuff under ``~/catkin_ws/.catkin_tools``.


Building the workspace
^^^^^^^^^^^^^^^^^^^^^^

A catkin workspace is made to host packages so that they can be compiled then used.
This compilation is, of course, necessary for C++ nodes, but also for defining messages or services and, also, for packages to be found by ROS commands.

Building a catkin workspace at least once is therefore mandatory even for pure Python packages.
This is done with:

.. code-block:: none
   :class: command-line

   catkin build -w ~/catkin_ws

.. hint::
   If you are in the catkin workspace (you can go there using `cd <https://manpages.ubuntu.com/manpages/focal/en/man1/cd.1posix.html>`__: ``cd ~/catkin_ws``), you don't need to specify the workspace with the ``-w ~/catkin_ws`` argument.


You can check (for instance with `ls <https://manpages.ubuntu.com/manpages/focal/en/man1/ls.1.html>`__: ``ls ~/catkin_ws``) that more directories have been created::

    catkin_ws
    ├-- build
    ├-- devel
    ├-- logs
    └-- src

There are also various files and, in particular there is a new ``catkin_ws/devel/setup.bash``.
This file serves the same purpose as ``/opt/ros/noetic/setup.bash``, that is, activating the workspace so that its packages can be used.
Therefore, we also need to source it in the same way:

.. code-block:: none
   :class: command-line

   source ~/catkin_ws/devel/setup.bash
   
This sourcing should be done after all compilations that changed significantly the contents of the workspace: new package, for instance.

As for ``/opt/ros/noetic/setup.bash``, put it in your ``.bashrc``:

.. code-block:: none
   :class: command-line

   echo -e "\n# ROS catkin workspace\nif [ -f ~/catkin_ws/devel/setup.bash ]; then\n    source ~/catkin_ws/devel/setup.bash\nfi" >> ~/.bashrc

.. hint::
   Now, the last 9 lines of your ``~/.bashrc`` should look like:

   .. code-block:: none
      :class: command-line

      tail -9 ~/.bashrc

   .. code-block:: none

      # ROS setup
      if [ -f /opt/ros/noetic/setup.bash ]; then
          source /opt/ros/noetic/setup.bash
      fi

      # ROS catkin workspace
      if [ -f ~/catkin_ws/devel/setup.bash ]; then
           source ~/catkin_ws/devel/setup.bash
      fi


Pepper driver packages
----------------------

Now that the catkin workspace is ready, we can finish with the installation of the ``pepper_driver_ext_msgs`` and ``pepper_examples`` packages.


Getting the packages
^^^^^^^^^^^^^^^^^^^^

The easiest is to clone the whole git repository:

.. code-block:: none
   :class: command-line

   cd && git clone https://gitlab.inria.fr/colas/pepper_driver_ext.git

.. important::
   You should avoid cloning the repository directly in the ``catkin_ws/src`` directory since your catkin workspace would then include the ``pepper_driver_ext`` package which is only meant for Ubuntu 16.04.

Now, you can include the ``pepper_driver_ext_msgs`` and ``pepper_examples`` packages in your catkin workspace using a symbolic link:

.. code-block:: none
   :class: command-line

   ln -s ~/pepper_driver_ext/pepper_driver_ext_msgs ~/catkin_ws/src/

and:

.. code-block:: none
   :class: command-line

   ln -s ~/pepper_driver_ext/pepper_examples ~/catkin_ws/src/


Recompiling the workspace
^^^^^^^^^^^^^^^^^^^^^^^^^

Now that the package is in its proper place, we can recompile the workspace:

.. code-block:: none
   :class: command-line

   catkin build -w ~/catkin_ws

The output should look like (with more colors)::

    ----------------------------------------------------------
    Profile:                     default
    Extending:          [cached] /opt/ros/noetic
    Workspace:                   /home/<user>/catkin_ws
    ----------------------------------------------------------
    Build Space:        [exists] /home/<user>/catkin_ws/build
    Devel Space:        [exists] /home/<user>/catkin_ws/devel
    Install Space:      [unused] /home/<user>/catkin_ws/install
    Log Space:          [exists] /home/<user>/catkin_ws/logs
    Source Space:       [exists] /home/<user>/catkin_ws/src
    DESTDIR:            [unused] None
    ----------------------------------------------------------
    Devel Space Layout:          linked
    Install Space Layout:        None
    ----------------------------------------------------------
    Additional CMake Args:       None
    Additional Make Args:        None
    Additional catkin Make Args: None
    Internal Make Job Server:    True
    Cache Job Environments:      False
    ----------------------------------------------------------
    Buildlisted Packages:        None
    Skiplisted Packages:         None
    ----------------------------------------------------------
    Workspace configuration appears valid.
    ----------------------------------------------------------
    [build] Found '2' packages in 0.0 seconds.
    [build] Package table is up to date.
    Starting  >>> pepper_driver_ext_msgs
    Finished  <<< pepper_driver_ext_msgs                [ 0.6 seconds ]
    Starting  >>> pepper_examples
    Finished  <<< pepper_examples                       [ 0.2 seconds ]
    [build] Summary: All 2 packages succeeded!
    [build]   Ignored:   None.
    [build]   Warnings:  None.
    [build]   Abandoned: None.
    [build]   Failed:    None.
    [build] Runtime: 0.9 seconds total.
    [build] Note: Workspace packages have changed, please re-source setup files to use them.

.. important::
   Make sure you don't have errors in the compilation.
   If it complains with an error such like ``Could not find a package configuration file provided by "whatever"``, then it means there are some missing dependencies.


And, since we added a new ROS package, we need to source again the corresponding ``setup.bash``:

.. code-block:: none
   :class: command-line

   source ~/catkin_ws/devel/setup.bash


Checking setup
--------------

Once that is done, our setup should be ready and our environment should be populated with the paths of the ``pepper_driver_ext_msgs`` and ``pepper_examples`` packages:

.. code-block:: none
   :class: command-line

   printenv ROS_PACKAGE_PATH

::

   /home/<user>/catkin_ws/src/pepper_driver_ext_msgs:/home/<user>/catkin_ws/src/pepper_examples:/opt/ros/noetic/share

.. note::
   If the packages are not there, you missed a step somewhere.
   Check you didn't have any compilation issue and the ``pepper_driver_ext_msgs`` and ``pepper_examples`` links are in their right place in ``~/catkin_ws/src/`` and correctly point to the sources.

   Remember also that the sourcing needs to happen after the compilation in every terminal window.

The ``pepper_driver_ext_msgs`` defines a series of messages and services (see :doc:`/messages`).
You can check that they are properly found:

.. code-block:: none
   :class: command-line

   rosmsg show pepper_driver_ext_msgs/AutonomousLifeLevel

::

   uint8 REST=0
   uint8 WAKE_UP=1
   uint8 NO_AUTONOMOUS_LIFE=2
   uint8 FULL_AUTONOMOUS_LIFE=3
   uint8 level


.. admonition:: Take-home message

   You've learned to create a catkin workspace, place a new ROS package in the ``src`` directory, and compile it with the ``catkin build`` command.

   .. important::
      During your work, remember to build the workspace but also ``source`` the ``devel/setup.bash`` script whenever you make significant workspace changes (new package).
