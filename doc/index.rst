pepper_driver_ext
=================

Additional ROS drivers for the Pepper robot.

The drivers extend the robot functionalities accessible in ROS.
Namely, it adds:

- access to the robot text-to-speech functionality,
- handling of the autonomous state of the robot,
- (un)loading and (de)activating dialog topics,
- access to ALMemory,
- publication of detected landmarks,
- handling of the tablet,
- access to touch events,
- LED control,
- fixed `/cmd_vel` driver.

The driver is in ROS 1 but a specific docker image for ROS 2 is provided to bridge services and topics.

.. note::
   Note that there are :doc:`limitations <ros2_limitations>` to the ROS 2 setup.


Contents
--------

.. toctree::
   :maxdepth: 1

   installation
   launching
   reference
   tutorials_ros1
   tutorials_ros2
   development
