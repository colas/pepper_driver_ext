.. sectnum::
   :prefix: T
   :depth: 3
   :start: 2

.. highlight:: none


Pepper teleoperation
====================

.. note::
   This tutorial is only for ROS 2; for ROS 1, check :doc:`/tutorials_ros1/t2_pepper_teleop`.

The aim of this tutorial is to create a ROS package with a Python node and a launch file to start it.
The node will allow joypad teleoperation of the Pepper robot base.

This tutorial is split into four parts:

- `general design`_ of the functionality,
- creation of a `ROS package`_,
- implementation of the `teleoperation node`_,
- and creation of a `launch file`_.

.. important::
   You need to have done the previous two tutorials: :doc:`/tutorials_ros2/t0_basic_setup` and :doc:`/tutorials_ros2/t1_pepper_discovery`.
   
   You will also need a joypad or a joystick and access to a Pepper robot (with its IP address).


General design
--------------

We want to be able to move the Pepper robot around using a joypad.

In the :doc:`/tutorials_ros2/t1_pepper_discovery` tutorial, we have seen that we can move the robot by sending a `geometry_msgs/msg/Twist <https://docs.ros2.org/latest/api/geometry_msgs/msg/Twist.html>`__ message on the ``/cmd_vel`` topic.
How can we send such messages with a joypad?


Joystick
^^^^^^^^

First, we need to be able to receive events from the joypad.
Luckily, there is generic ROS driver for joysticks: ``joy_node`` in the `joy <https://index.ros.org/p/joy>`__ package.

#. Start the joystick node in a terminal window:

   .. code-block:: none
      :class: command-line

      ros2 run joy joy_node

#. In another terminal window, inspect the node:

   #. Which topic(s) does it publish on?
   #. With which message type?

#. Look at the messages while you move the axes and press the buttons.

   #. Which axis do you want to use to make the robot move forward/backward? Note down its index.
   #. Same question for left/right rotation.
   #. Same question for left/right translation.

#. The maximum translation velocity of the Pepper is 0.35 m/s and its maximum angular velocity is 1 Rad/s.
   Deduce the formulas to compute the following ``Twist`` attributes from the ``Joy.axes`` values:

   - ``linear.x``
   - ``linear.y``
   - ``linear.z``
   - ``angular.x``
   - ``angular.y``
   - ``angular.z``

   .. hint::
      Always be mindful of units.
      ``Joy`` values are unitless between -1 and 1, while ``Twist`` values are in m/s or rad/s.
      Therefore, you need multiplicative constants to ensure unit homogeneity of your equations.


Design
^^^^^^

We want to listen to the ``/joy`` messages and publish commands on the ``/cmd_vel`` topic.
For this, we will create a node which subscribes to ``/joy`` (with type `sensor_msgs/msg/Joy <https://docs.ros2.org/latest/api/sensor_msgs/msg/Joy.html>`__) computes a relevant command (based on parameters) and publishes it on ``/cmd_vel`` (with type `geometry_msgs/msg/Twist <https://docs.ros2.org/latest/api/geometry_msgs/msg/Twist.html>`__.

The simplified design is illustrated below:

.. math::
   :nowrap:

   \begin{tikzpicture}[node distance=2cm]
       \node (teleop) [rectangle split, draw, rectangle split parts=3, text width=4.5cm, align=center]
           {
             \texttt{\textbf{teleop\_joy}}
             \nodepart[align=left]{two}
             \texttt{joy\_cb}
             \nodepart[align=right]{three}
             \texttt{cmd\_vel\_pub}
           };

       \path (teleop.two west) node (joy) at +(-2cm,0) [anchor=east] {\texttt{/joy}};
       \path (teleop.three east) node (cmd_vel) at +(2cm,0) [anchor=west] {\texttt{/cmd\_vel}};

       \draw [->, thick] (joy.east) .. controls +(right:0.5cm) and +(left:0.5cm) .. (teleop.two west);
       \draw [->, thick] (teleop.three east) .. controls +(right:0.5cm) and +(left:0.5cm) .. (cmd_vel.west);
   \end{tikzpicture}

Concretely, after initialization, the node will wait for messages and, when one arrives, a `callback <https://en.wikipedia.org/wiki/Callback_(computer_programming)>`__ function will be called to handle that `event <https://en.wikipedia.org/wiki/Event-driven_programming>`__.
In our case, we want that function to compute a new command and to publish it immediately.
This event and callback mechanism is handled by a `rclpy.subscription.Subscription <https://docs.ros2.org/galactic/api/rclpy/api/topics.html#rclpy.subscription.Subscription>`__, which should be created.
Publication is made using a `rclpy.publisher.Publisher <https://docs.ros2.org/galactic/api/rclpy/api/topics.html#rclpy.publisher.Publisher>`__ object, which should be created first and used for every message (instead of recreating it for every message).
Both ``Subscription`` and ``Publisher`` should therefore be retained as members of a class.

The class design is therefore:

.. math::
   :nowrap:

   \begin{tikzpicture}[node distance=2cm]
       \node (teleop_class) [rectangle split, draw, rectangle split parts=3, text width=6cm, align=center]
           {
             \texttt{\textbf{PepperTeleop}}
             \nodepart[align=left]{two}
             \texttt{cmd\_vel\_pub: Publisher}
             \texttt{joy\_sub: Subscription}
             \nodepart[align=left]{three}
             \color{mygrey}
             \texttt{\_\_init\_\_()}\\
             \color{black}
             \texttt{joy\_cb(msg)}
           };
   \end{tikzpicture}


.. _ros2_package:

ROS Package
-----------

We have a general design and the algorithm (the formulas).
However, before we start the implementation, we need to layout the infrastructure to properly integrate our node in the ROS ecosystem, starting with the ROS workspace.
Concretely, it means creating a ROS package, which entails specifying several files.


File hierarchy
^^^^^^^^^^^^^^

A ROS package is a directory with at least 2 required files:

- ``package.xml``: metadata on the ROS package (name, description, authors, dependencies...),
- ``CMakeLists.txt`` (CMake) or ``setup.py`` (Python): compilation directives for the package (message generation, compilation, installation...).

.. hint::
   In addition, a package with only Python code must have a ``resource/<package_name>`` marker file (empty) for the `ament` resource index.

.. note::
   Python has a concept of `package <https://docs.python.org/3/tutorial/modules.html#packages>`__, which is independent from ROS packages.
   A ROS package can include a Python package but they are conceptually distinct.
   A ROS package can have nodes, whereas a Python package will have modules which can be imported.
   In this documentation, we sometimes use `package` as a shortcut for `ROS package`.

Besides these necessary files, a ROS package can host several kinds of files: nodes, launch files, configuration, message or service definition...
While those can be placed in arbitrary locations, there are conventions to guide us.
A full ROS package in Python can therefore look like::

    <ros_ws>/src/
    ├-- other_package/
    |   └-- ...
    └-- package_name/
        ├-- config/
        |   └-- config.yaml
        ├-- launch/
        |   ├-- some_launch.py
        |   └-- some_launch.xml
        ├-- msg/
        |   └-- some_message.msg
        ├-- package_name/
        |   ├-- __init__.py
        |   └-- some_node.py
        ├-- resource
        |   └-- package_name
        ├-- srv/
        |   └-- some_service.srv
        ├-- package.xml
        ├-- setup.cfg
        └-- setup.py

Step-by-step
^^^^^^^^^^^^

These files could be filled by hand but there are tools to provide templates.

#. In a terminal window, go to your ROS workspace:

   .. code-block:: none
      :class: command-line

      cd ~/ros2_ws/src

#. Initialize a package named ``pepper_teleop``:

   .. code-block:: none
      :class: command-line

      ros2 pkg create --build-type ament_python --dependencies sensor_msgs geometry_msgs --license MIT pepper_teleop

   .. hint::
      This command creates the required files and directory for a ROS package named ``pepper_teleop`` which depends on the ROS packages ``geometry_msgs``, and ``sensor_msgs``.

      These dependencies are specified in the ``package.xml`` metadata file and can always be modified later by editing this file.

#. Edit the ``pepper_teleop/package.xml`` file to fill in the:

   - description,
   - maintainer (with your own name and email).

#. Create the ``pepper_teleop/pepper_teleop.py`` file for the node with the following (minimal) content:

   .. code-block:: python

      def main():
          pass

   .. hint::
      This ``main`` function will be what will be run as node (see next step).

#. Edit the ``setup.py`` file to include the node in the ``'console_scripts'`` list of the ``entry_points`` argument:

   .. code-block:: python
 
          entry_points={
              'console_scripts': [
                  'pepper_teleop = pepper_teleop.pepper_teleop:main',
              ],
          },

   .. hint::
      Find the correct place to add the single missing line.

   .. hint::
      Note that it refers to the ``main`` function in the ``pepper_teleop`` module from the ``pepper_teleop`` package.

#. Now your first ROS package is ready but not yet known from the ROS tools:

   .. code-block:: none
      :class: command-line

      ros2 pkg list | grep pepper

   .. code-block:: none

      pepper_driver_ext_msgs2
      pepper_examples_ros2

   .. hint::
      If the above packages are not there, you missed sourcing the ``~/ros2_ws/install/local_setup.bash`` file from :doc:`/tutorials_ros2/t0_basic_setup`.

   We need to:

   - rebuild the ROS workspace (from the ``ros2_ws`` directory):

     .. code-block:: none
        :class: command-line

        colcon build

   - source the updated ``local_setup.bash``:

     .. code-block:: none
        :class: command-line

        source ~/ros2_ws/install/local_setup.bash

#. Check ``pepper_teleop`` is now known:

   .. code-block:: none
      :class: command-line

      ros2 pkg list | grep pepper_teleop

#. Run your (empty) node:

   .. code-block:: none
      :class: command-line

      ros2 run pepper_teleop pepper_teleop

   .. hint::
      Obviously it should not do anything but it should not fail either.

.. hint::
   You can change the dependencies or add new Python nodes or messages, etc. at any time: you just need to update the ``package.xml`` and ``setup.py`` files as required.
   When that happens, you need to rebuild the workspace and source again the ``setup.bash``.


.. _ros2_package_checklist:

ROS Package checklist
^^^^^^^^^^^^^^^^^^^^^
For reference for future packages, here are the things to complete and check after the creation of a package with ``ros2 pkg create --build-type ament_python --dependencies dep1 dep2 ... depN --license <license> <package_name>``:

- dependencies in ``package.xml``
- ``setup.py`` with proper content for each node
- rebuild the workspace: ``colcon build``
- source the ``local_setup.bash``: ``source ~/ros2_ws/install/local_setup.bash``



Teleoperation node
------------------

Now that we have a properly recognized package we can finally implement our node.
It will be composed of a single class instantiated by our ``main`` function.
That function will also keep the node alive by sleeping until the node is asked to terminate, so that messages can be processed by the class.

In the first part, we will write the class before, in the second part, writing the rest of the node.


Class
^^^^^

Recall the design of the class:

.. math::
   :nowrap:

   \begin{tikzpicture}[node distance=2cm]
       \node (teleop_class) [rectangle split, draw, rectangle split parts=3, text width=6cm, align=center]
           {
             \texttt{\textbf{PepperTeleop}}
             \nodepart[align=left]{two}
             \texttt{cmd\_vel\_pub: Publisher}
             \texttt{joy\_sub: Subscription}
             \nodepart[align=left]{three}
             \color{mygrey}
             \texttt{\_\_init\_\_()}\\
             \color{black}
             \texttt{joy\_cb(msg)}
           };
   \end{tikzpicture}

There are two attributes and two methods.
The ``__init__`` method is a `special method <https://docs.python.org/3/reference/datamodel.html#object.__init__>`__ in Python called at the object creation in order typically to create its attributes.
In our case, we will use it to create our `publisher <http://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers#Publishing_to_a_topic>`__ and to `subscribe <https://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers#Subscribing_to_a_topic>`__ to the ``/joy`` topic.

The second method ``joy_cb`` will be called by the ROS event loop when a message arrives and will compute the command and send it as a message using the publisher.

#. Edit the ``pepper_teleop/pepper_teleop.py`` file to create the class with the following ``__init__`` code:

   .. code-block:: python

      class PepperTeleop(Node):
          """Joystick teleoperation for Pepper robot."""

          def __init__(self):
              """Create publisher and subscriber."""
              super().__init__('pepper_teleop')
              # publisher
              self.cmd_vel_pub = self.create_publisher(Twist, '/cmd_vel', 1)
              # subscription
              self.joy_sub = self.create_subscription(Joy, '/joy', self.joy_cb, 1)

   .. important::
      Be mindful of indentation, we *strongly* advise to stick to 4-space indentation.

#. Complete the class with the ``joy_cb`` method:

   .. code-block:: python

      class PepperTeleop(Node):
          """Joystick teleoperation for Pepper robot."""

          ...

          def joy_cb(self, msg: Joy):
              """Compute and publish twist command."""
              # initialize message
              cmd_vel_msg = Twist()
              # fill message
              cmd_vel_msg.linear.x = ...
              cmd_vel_msg.linear.y = ...
              cmd_vel_msg.linear.z = ...
              cmd_vel_msg.angular.x = ...
              cmd_vel_msg.angular.y = ...
              cmd_vel_msg.angular.z = ...
              # publish message
              self.cmd_vel_pub.publish(cmd_vel_msg)

   .. hint::
      To get the value of, for instance, the third axis in the ``msg`` message of type `sensor_msgs/msg/Joy <http://docs.ros2.org/latest/api/sensor_msgs/msg/Joy.html>`__, you can simply write ``msg.axes[2]``.
      Similarly, the first button is ``msg.buttons[0]``.

#. Complete the message-filling part according to your equations from `Joystick`_.


Node
^^^^

In Python, a class or function definition is a statement that is executed when the module is executed.
A class definition makes the class known to Python but does not create an instance of the class in the same way that a function definition does not call the function.

The rest of the ``pepper_teleop/pepper_teleop.py`` file is therefore responsible for instantiating the class and importing the relevant modules and classes.
In particular, we will populate our ``main`` function to initialize the node, instantiate an object of the class ``PepperTeleop``, and wait until the node is closed.
We will call this function in an idiomatic way.

#. Start by inserting the documentation of your node at the start of the file:

   .. code-block:: python

      """Joystick teleoperation node for the Pepper robot.

      This node computes velocity commands from joystick messages.

      It subscribes to:
          - `/joy` (sensor_msgs/msg/Joy): joystick message.

      It publishes to:
          - `/cmd_vel` (geometry_msgs/msg/Twist): command velocity.
      """

   .. hint::
      This is called a `docstring <https://peps.python.org/pep-0257/#what-is-a-docstring>`__.
      When coding, it is important to document our code and update the documentation when changes to the code are made.

#. Then import the ``rclpy`` module and the ``Joy`` and ``Twist`` classes:

   .. code-block:: python

      import rclpy
      from geometry_msgs.msg import Twist
      from sensor_msgs.msg import Joy
      from rclpy.node import Node

#. At the end of the file, define the main function:

   .. code-block:: python

      def main(args=None):
          """Instantiate node and class."""
          rclpy.init(args=args)
          # create node
          pepper_teleop_node = PepperTeleop()
          # run
          try:
              rclpy.spin(pepper_teleop_node)
          except KeyboardInterrupt:
              pass
          # end
          rclpy.shutdown()

#. Finally, conclude your file with the call to your ``main`` function:

   .. code-block:: python

      if __name__ == '__main__':
          main()

   .. hint::
      This condition basically states to call the function if the file is executed as a standalone program but not when it is loaded as a module.
      See `this answer <https://stackoverflow.com/a/419185>`__ for more details.
      It is not necessary for ROS nodes (as you've seen since we where able to run it before), but it is customary for Python scripts.

#. Save your file.

   .. hint::
      You need to compile the ROS workspace again.
      However, it shouldn't be necessary to source the ``local_setup.bash`` file again.


Tests
^^^^^

.. role:: command(code)
   :class: command-line

We will first test our node in isolation before using it to make the robot move.

#. Normally, you should have a terminal window with ``joy_node`` running.
   If it is not the case any more, make it so.
#. Make sure the Pepper is not connected by listing the nodes: you should only have ``/joy_node``.
#. Ready three additional terminal windows.
   In the first two run respectively:

   - :command:`ros2 topic echo /joy`
   - :command:`ros2 topic echo /cmd_vel`

#. Press buttons and move joypad axes.
   Check that the ``/joy`` messages are published as expected.
#. In the last terminal window, start your node:

   .. code-block:: none
      :class: command-line

      ros2 run pepper_teleop pepper_teleop

#. Move the joypad axes and check values are correct.

   .. important::
      In particular, make sure the velocity commands published are not too large.
      For the Pepper, it should not exceed 0.35 m/s and 1.0 rad/s for respectively the linear and angular components of the twist.

#. If everything is correct, stop both ``ros2 topic echo`` commands and, instead, launch the robot drivers and the bridge.
#. Use the joypad to make the robot move.

   .. danger::
      Be very careful when moving the robot.

#. Terminate all nodes and programs.

   .. hint::
      Congratulations for your first ROS node!


Launch file
-----------

Now, it is a bit tedious to have to run separately all these commands.
We run the robot drivers using one of the provided :doc:`/launch_files`, which prevents starting dozens of commands in as many terminals.
The objective of this last part is to write a small launch file to launch the joystick teleoperation.
Doing so, we will learn about ROS parameters and launch file arguments.


Basic launch file
^^^^^^^^^^^^^^^^^

`ROS 2 launch files <https://docs.ros.org/en/galactic/Tutorials/Intermediate/Launch/Launch-Main.html>`__ can be in Python, in `YAML <https://en.wikipedia.org/wiki/YAML>`__, or in `XML <https://en.wikipedia.org/wiki/XML>`__ file with a specific `set of tags and attributes <https://docs.ros.org/en/galactic/How-To-Guides/Launch-files-migration-guide.html#migrating-tags-from-ros-1-to-ros-2>`__, which is interpreted by the ``ros2 launch`` command.

.. hint::
   For the sake of simplicity, we will use the XML variant, which is often the most compact.


#. Create a ``launch`` directory at the root of your ROS package to put your launch file.
#. The root element of launch files always has the `<launch> <https://wiki.ros.org/roslaunch/XML/launch>`__ tag.
   In your ``launch/`` directory, create a ``pepper_teleop_launch.xml`` file with the following content:

   .. literalinclude:: ../../pepper_teleop2/launch/pepper_teleop_launch.xml
      :language: xml
      :lines: 1,17

   .. hint::
      Notice how, in XML, an element is closed with a ``/``.
      It is similar to `HTML <https://en.wikipedia.org/wiki/HTML>`__ as both derive from `SGML <https://en.wikipedia.org/wiki/Standard_Generalized_Markup_Language>`__.

      In these languages, there are elements denoted with tags in a tree-like hierarchy.
      Each element can have attributes which have values (in quotes).
      An example can be:

      .. code-block:: xml

         <course id="M2_AVR">
             <name>Situation Intégratrice</name>
             <!-- TODO: check duration -->
             <duration>36 heures</duration>
             <lecturer name="Colas" firstname="Francis"/>
             <lecturer name="Colotte" firstname="Vincent"/>
             <sessions>
                 <session id="1">Introduction + Interaction multimodale</session>
                 <session id="2">ROS + Navigation</session>
             </sessions>
         </course>

      In this example, the root element has the tag ``course`` with the attribute ``id`` which has the value ``M2_AVR`` (quoted).
      This element has five children.
      The ``<!-- ... -->`` construct is an XML comment.

      Note that the elements with children are written ``<tag>...</tag>`` whereas empty elements (no children but can have attributes) can be written ``<tag ... />``.

#. In order for the launch file to be known to the ROS system, it needs to be installed.
   Modify the ``data_files`` argument in the ``setup.py`` file such that it exports the launch file:

   .. code-block:: python

          data_files=[
              ('share/ament_index/resource_index/packages',
                  ['resource/' + package_name]),
              ('share/' + package_name, ['package.xml']),
              ('share/' + package_name + '/launch',
                  ['launch/pepper_teleop_launch.xml']),
          ],

#. The first node we want to run is ours.
   This can be done using the `<node> <https://docs.ros.org/en/galactic/How-To-Guides/Launch-files-migration-guide.html#node>`__ with the package and the type as attribute.

   #. Insert the following as child of the ``<launch>`` tag:

      .. literalinclude:: ../../pepper_teleop2/launch/pepper_teleop_launch.xml
         :language: xml
         :lines: 12,13,16

      .. hint::
         XML is like code, you want to put comments to make things clearer.

   #. Save the file and recompile your workspace.
   #. Launch your file with (make sure no other ROS node is running):

      .. code-block:: none
         :class: command-line

         ros2 launch pepper_teleop pepper_teleop_launch.xml

   #. List the nodes in another terminal window.
   #. Stop the launch file by pressing ``ctrl+c`` in the terminal window you launched it.

#. Our node is useless without the joystick driver.

   #. Add the ``<node>`` element to launch the joystick driver.
   #. Test that it is properly launched and that the nodes are connected.

      .. hint::
         You can opt to set the ``log`` attribute to the value ``screen`` in this node.
         This will display all log output of the node in the terminal.
         This can help debug.


Parameters
^^^^^^^^^^

ROS provides a mechanism for nodes to have parameters, which can be manipulated or provided from outside, without changing the source code.

#. Launch your launch file and check the parameters using ``ros2 param list``.

   .. hint::
      You should see several nodes with parameters in each of them.
      In particular, notice ``autorepeat_rate`` in ``/joy_node``, which controls the frequency of repeat of the same value, and ``deadzone``, which limits the values due to small noise close to the resting position.

#. There are two ways to set parameters in a launch file: with the `<param> <https://docs.ros.org/en/galactic/How-To-Guides/Launch-files-migration-guide.html#param>`__ and `<rosparam> <https://docs.ros.org/en/galactic/How-To-Guides/Launch-files-migration-guide.html#rosparam>`__ tags.
   The first one specifies a single parameter while the second loads parameters from a YAML file.

   We will specify some parameters for `joy_node <https://index.ros.org/p/joy/#galactic>`_.
   In particular, we want to provide the right device, but also tell it to not be too sensitive to small values and to slow down publishing.

   #. Insert a ``param`` element in your `joy_node` ``<node>`` element to set the ``deadzone`` to a value of ``0.1``:

      .. literalinclude:: ../../pepper_teleop2/launch/pepper_teleop_launch.xml
         :language: xml
         :lines: 8

      .. hint::
         This tells the joystick driver to not consider axes values of less than 0.1.
         This is useful for not constantly sending very small commands.

      .. hint::
         A child in XML must be put before the closing of the element with ``</tag>``.
         Remember that ``<tag />`` is a shortcut for ``<tag></tag>`` so make sure to check your elements are not closed too early.

      .. hint::
         Make use of indentation to have a visual indication of the nesting levels of your elements.

         .. hint::
            Code editors such as Visual Studio Code often propose a way to automate indentation.
            With Visual Studio Code, the ``ctrl+shift+I`` key combination will fix indentation of the current file.
            If no formatter is available for your current file type, it will ask you to install one.

   #. Insert another ``param`` element to set ``autorepeat_rate`` to 10. Hz.
   #. In the same way, insert a last ``param`` element for the ``device_name`` ROS parameter.
   #. Save, compile and launch your new launch file.
   #. Check that the joystick node is running with the correct device and parameters (it tells you).

#. With this launch file, we pass parameters to the ``joy_node`` node.
   Let's do the same to pass the maximum velocities to our node.

   #. Define two parameters ``max_lin_vel`` and ``max_ang_vel`` in your launch file (with proper value).
   #. Before being able to use them, the node must declare its parameters using `self.declare_parameter <https://docs.ros2.org/galactic/api/rclpy/api/node.html#rclpy.node.Node.declare_parameter>`__.
      In the ``__init__`` method add the following:

      .. code-block:: python

         # parameters
         self.declare_parameter('max_lin_vel', 0.35)
         self.declare_parameter('max_ang_vel', 1.0)

   #. Now, we can get the parameter using `self.get_parameter <https://docs.ros2.org/galactic/api/rclpy/api/node.html#rclpy.node.Node.get_parameter>`__ and its value via its `value <https://docs.ros2.org/galactic/api/rclpy/api/parameters.html#rclpy.parameter.Parameter.value>`__ property.
      Modify your ``joy_cb`` callback method to use those parameters:

      .. code-block:: python

         # get current parameter values
         max_lin_vel = self.get_parameter('max_lin_vel').value
         max_ang_vel = self.get_parameter('max_ang_vel').value

      .. hint::
         It is actually good practice to have as few literal constants in the code but use parameters whenever relevent, even for constants that will not be modified in practice.

   #. Modify the module docstring to include::

          Parameters:
              - `max_lin_vel`: maximum linear velocity in m/s
                  (default 0.35).
              - `max_ang_vel`: maximum angular velocity in rad/s
                  (default 1.0).

      .. hint::
         It is very important to keep the documentation synchronized with the code.

   #. Save, compile, then launch your launch file and monitor the ``/cmd_vel`` topic.
      
      #. Watch the range of values when moving the axes of the joypad.
      #. Lower the maximum linear velocity using:

         .. code-block:: none
            :class: command-line

            ros2 param set /pepper_teleop max_lin_vel 0.1

      #. Observe the range of values again.
      #. Set back the parameter to its default value.


Launch file arguments
^^^^^^^^^^^^^^^^^^^^^

Now, we can set the parameters by changing the launch file or using the ``ros2 param set`` command.
Another way to provide parameters is through launch arguments (this is used for the ``pepper_ip`` value for the drivers, for instance).

This can be done by declaring an argument with the `<arg> <https://docs.ros.org/en/galactic/How-To-Guides/Launch-files-migration-guide.html#arg>`__ tag and using the values of this argument to set the parameter.

#. Declare both ``max_lin_vel`` and ``max_ang_vel`` arguments as children of the root ``<launch>`` tag:

   .. literalinclude:: ../../pepper_teleop2/launch/pepper_teleop_launch.xml
      :language: xml
      :lines: 2-4

   .. hint::
      It is a good idea to group all argument declarations near the top of the launch file.

   .. hint::
      Arguments do not need to have a default value.
      In that case, an error will be raised if the value is not provided at launch time (as is the case with the ``pepper_ip`` argument for the drivers).

#. Change the value of the corresponding parameters so that they use the value of the argument by substitution:

   .. literalinclude:: ../../pepper_teleop2/launch/pepper_teleop_launch.xml
      :language: xml
      :lines: 14-15

   .. hint::
      This ``$(...)`` syntax (called `substitution <https://design.ros2.org/articles/roslaunch_xml.html#substitution-syntax>`__) is specific to launch files (it is not generic XML) and allows us to use some convenient functions (``var foo`` to get the value of the ``foo`` launch-file argument in the present case).

#. Launch your saved launch file to `pass <https://wiki.ros.org/roslaunch/Commandline%20Tools#Passing_in_args>`__ the ``max_lin_vel`` and ``max_ang_vel`` arguments:

   .. code-block:: none
      :class: command-line

      ros2 launch pepper_teleop pepper_teleop_launch.xml max_lin_vel:=0.2 max_ang_vel:=0.6

#. Check that the correct values are used.


.. hint::
   Your final launch file should look like:

   .. literalinclude:: ../../pepper_teleop2/launch/pepper_teleop_launch.xml
      :language: xml

   Your node should look like:

   .. literalinclude:: ../../pepper_teleop2/pepper_teleop/pepper_teleop.py
      :language: python

.. admonition:: Take-home message

   In this tutorial, you've learned to create a ROS package, to write a simple node, and to write a launch file including parameters and arguments.
