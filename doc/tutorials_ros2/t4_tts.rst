.. sectnum::
   :prefix: T
   :depth: 3
   :start: 4

.. |duration| image:: /_static/images/Clock.png
   :height: 40px


How to use pepper TTS with ROS 2
================================

.. note::
   This tutorial is only for ROS 2; for ROS 1, check :doc:`/tutorials_ros1/t4_tts`.

|duration| 10 min

This tutorial aims to learn how to use the text-to-speech functionality (TTS) with ROS, by using :doc:`/api_nodes/pepper_tts_driver`.
As you know, two ways are possible (with a ROS *topic* or *service*) but the prefered one is presented here: with a ROS *topic*.


TTS with a ROS topic
--------------------

#. Before creating a node, we will publish manually a text on the right topic. However, we need to be sure that :doc:`/api_nodes/pepper_tts_driver` has been launched before. 
   Run the docker with :ref:`pda_launch` for instance. Check with ``ros2 node list`` and  ``ros2 node info /pepper_tts_driver`` if the *text_to_speech* topic and service of node :doc:`/api_nodes/pepper_tts_driver` are here.

#. Now, you can publish manually yourself a text to be pronounced by the robot.

   .. hint::
      Remember: ``ros2 topic pub /your_topic message_format "formatted_message"``  (here, the *message_format* is `std_msgs/msg/String`)
      Remember: What is your topic? ``ros2 topic list``

      Answer: The topic is ``/text_to_speech``

#. Is it ok? So, we will use a package to build our node. Make your package with one node (or use a previous one).

   .. note::
      Once you have completed the package with a empty node file (called ``tts_topic.py``), don't forget to build it.

#. Now, let's write the node: ``tts_topic.py``

   .. hint::
      We will make the file piece by piece, so identify, for each piece, the key point (don't just copy-paste!).

   - At first, add ROS import.

     .. code-block:: python

        #!/usr/bin/env python

        # ROS imports
        import rclpy
        from rclpy.node import Node
        from std_msgs.msg import String

   - Add the ``__init__`` function to prepare the publisher

     .. code-block:: python

        class TTSTopic:
            """Main class of the node."""

            def __init__(self):
                """Create node and publisher."""
                # create node
                super().__init__('tts_topic')
                # publisher
                self.tts_pub = self.create_publisher(String, '/text_to_speech', 1)
                

   - Then we need to create a function to publish the text, pronounced by the robot.

     .. code-block:: python

        def say_text(self, text):
            """Send message"""
            self.tts_pub.publish(text)

   - And the ``main`` function

     .. code-block:: python

        def main(args=None):
            """Instantiate then run class."""
            rclpy.init(args=args)
            
            node = TTSTopic()
            
            text = "Ceci est un essai de synthèse à partir d'un texte."
            node.say_text(text)
            
            try:
                print("\nLet's speak to Pepper. Press ^C to stop dialog")
                rclpy.spin(node)
            except KeyboardInterrupt:
                pass
        # end
        rclpy.shutdown()


        if __name__ == '__main__':
            main()

#. That's it ;-) ... No! We forgot the comment of the node itself at the beginning!

   .. code-block:: python

      #!/usr/bin/env python
      """
      This node makes the Pepper say a single sentence using the
      `/text_to_speech` topic published by `pepper_tts_driver`.

      It publishes to:
         - `/text_to_speech` (std_msgs/String): the sentence to say.
      """


#. Now we can launch our node!

   .. code-block:: none
      :class: command-line

      ros2 run your_package tts_topic.py

   .. warning::

      Did you have done the 3 steps for using your beautiful node?

      (Before a tip: go to package folder --> ``roscd your_package``)

      1. Put the file **in the right place** (in ``ros2_ws`` folder)

      2. **Build** the package (``colcon build``)

      3. **Source** the code: ``source install/local_setup.bash``

.. hint::

   You can also retrieve this example of TTS with a ROS topic in the package ``pepper_examples``.


TTS with a ROS service
----------------------

The way with a ROS `service` is possible. But, unlike in ROS 1, a ROS `service` in ROS 2 is a bit tricky to use when we would like to obtain a synchronous error log message. So it gives us no better way than with a ROS `topic`.



.. admonition:: Take-home message

   What you have learnt in this tutorial:

   1. How to make Pepper say a text by using a ROS topic.
   2. Why you won't make Pepper say a text by using a ROS service.

