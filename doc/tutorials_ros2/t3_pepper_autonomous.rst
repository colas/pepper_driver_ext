.. sectnum::
   :prefix: T
   :depth: 3
   :start: 3

.. |duration| image:: /_static/images/Clock.png
   :height: 40px


How to manage the autonomous life of Pepper
===========================================

.. note::
   This tutorial is only for ROS 2; for ROS 1, check :doc:`/tutorials_ros1/t3_pepper_autonomous`.


|duration| 5 min


This tutorial aims to learn how to manage the *autonomous life* of Pepper, by using :doc:`/api_nodes/pepper_autonomous_life_driver`.
Pepper is an interactive robot. Several levels of interaction have been developt. 
By default, the robot starts with a full *autonomous* life level, it means that a dialog is activated and several services for interaction are launched (facial tracking, motions for tracking, arms and head motions for speech gestures, etc. )  
Usually, we need to stop these services to avoid conflict with our own interaction dialog.

Levels of autonomous life
-------------------------

As explained in :doc:`/api_nodes/pepper_autonomous_life_driver`, there are four levels:
    - **REST**: no autonomous life nor motion (joint stiffness is 0)
    - **WAKE_UP**: no autonomous life but stiff
    - **NO_AUTONOMOUS_LIFE**: some autonomous motion but don't try to
        interact
    - **FULL_AUTONOMOUS_LIFE**: full autonomous life as when started up

If we want to develop a project with interaction and to keep a realistic behavior, the level **no_autonomous_life** is recommanded.
For this level, the autonomous life dialog is deactived but realistic body motions (arms and head) are kept. 
But the **wake_up** level is frequently used too for avoiding arm motion.
We will see in the next section how to set a level. 


How to change the level of autonomous life
------------------------------------------

We will see how to change the level of automous life by two ways: with an inline command and with a node.
Both ways use the services ``get/set_autonomous_life_level`` with the message format :doc:`/api_msgs/srv/GetAutonomousLifeLevel` and :doc:`/api_msgs/srv/SetAutonomousLifeLevel` respectively.

#. So with an inline command the level is a integer value (from 0 for **REST** to 3 for **FULL_AUTONOMOUS_LIFE** level):

   .. code-block:: none
      :class: command-line
      
      ros2 service call /set_autonomous_life_level pepper_driver_ext_msgs2/srv/SetAutonomousLifeLevel "{level: 2}"

   
   .. warning::

      Be careful when you put the **REST** level (=0), the robot takes is rest position. So the head is going to be lowered: don't forget to hold its head.


#. With a *ros node*, prepare your empty node: ``autonomous_life_test.py`` (see one of the previous tutorials to create/use a ROS package).

   We will build a class for managing these services.


   - At first, let's start by the imports of the service definition and format.

   .. code-block:: python

      # ROS imports
      import rclpy
      from rclpy.node import Node
      from pepper_driver_ext_msgs2.srv import SetAutonomousLifeLevel
      from pepper_driver_ext_msgs2.srv import GetAutonomousLifeLevel

   - And, the creation of clients which will be connected to services. In the ``__init__`` function of our class, we create both clients.

   .. code-block:: python

      class PepperTestAutonomousLife(Node):

         def __init__(self):
            super().__init__('pepper_test_autonomous_life')
            self.client_get = self.create_client(GetAutonomousLifeLevel, 'get_autonomous_life_level')
            while not self.client_get.wait_for_service(timeout_sec=1.0):
                self.get_logger().info('service not available, waiting again...')
            self.client_set = self.create_client(SetAutonomousLifeLevel, 'set_autonomous_life_level')
            while not self.client_set.wait_for_service(timeout_sec=1.0):
                self.get_logger().info('service not available, waiting again...')

   - Then, we build our function for sending a request. Let's start by the ``get_autonomous_life_level``. We create a empty request with the good type and we call the service with the asynchronous call function ``call_async(self.request)``. As it's asynchronous we need to wait the reponse by a ``spin`` fonction of ``rclpy``. and we get the result.

   .. code-block:: python
   
           def send_request_getautonomouslife(self):
               self.request = GetAutonomousLifeLevel.Request()
               self.future = self.client_get.call_async(self.request);
               rclpy.spin_until_future_complete(self, self.future)
               return self.future.result()

   - Now, the call in the main function (with the node management):

   .. code-block:: python

           def main(args=None):
               """Instantiate node and class."""
               rclpy.init(args=args)
         
               client = PepperTestAutonomousLife()
               response = client.send_request_getautonomouslife()
               client.get_logger().info("Result: %r" % (response))
           
               client.destroy_node()
               rclpy.try_shutdown()

       if __name__ == '__main__':
           main()

   - You can run your node (don't forget to build it) and check the result for the current level of autonomous life level.

   - Now, for modifying the autonomous life level we do the same process with ``set_autonomous_life_level``. At first, we start with the function for sending the request. Unlike inline command, we can use ``SetAutonomousLifeLevelRequest.*`` with REST, WAKE_UP, NO_AUTONOMOUS_LIFE, FULL_AUTONOMOUS_LIFE values (see :doc:`/api_msgs/srv/SetAutonomousLifeLevel`). It's more readable!

   .. code-block:: python

          def send_request_setautonomouslife(self):
              self.request = SetAutonomousLifeLevel.Request()
              self.request.level=SetAutonomousLifeLevel.Request().NO_AUTONOMOUS_LIFE
              self.future = self.client_set.call_async(self.request)
              rclpy.spin_until_future_complete(self, self.future)
              return self.future.result()   

     
   - And in the main function, let's add these lines:

   .. code-block:: python

       response = client.send_request_setautonomouslife()
       client.get_logger().info("Result: %r" % (response))
         
   - You can run your node.

#. *That's all folks !*


.. admonition:: Take-home message

   You learnt with this tutorial that the Pepper robot has got **four levels** of *autonomous* life. And, you know how to change it with the service ``set_autonomous_life_level`` with a inline command or with a ros node (by using :doc:`/api_msgs/srv/SetAutonomousLifeLevel` format).
   
