.. sectnum::
   :prefix: T
   :depth: 3
   :start: 5

.. |duration| image:: /_static/images/Clock.png
   :height: 40px


How to manage a dialog with ROS 2
=================================
  
.. note::
   This tutorial is only for ROS 2; for ROS 1, check :doc:`/tutorials_ros1/t5_dialog`.


|duration| 45 min


This tutorial aims to learn how to manage a dialog with ROS, by using :doc:`/api_nodes/pepper_dialog_driver`.
We will use service approach. This tutorial presents main services, syntax and a link with the memory. For further details, please go to the dialog API reference.



What is a *dialog*
------------------

Before to start we need to recall what is a *dialog* (with Pepper).
Aldebaran provides a framework to manage a dialog between a human and the robot.

A dialog is a list of `rules` which link user input with robot output (*user rules*) and some independent outputs of the robot (*proposal rules*).

These rules concerning one conversation are grouped by *topic* (1 file = 1 topic).

   .. warning::
      You read right, we talked about *topic*!
      BE CAREFUL, don't confuse *ROS topics* and *dialog topics*!
      It's easy to avoid because to manage a dialog with ROS we will use a *ROS service*, no ROS topic in the dialog business!
      So, for dialog with pepper, when we use the **topic** wording, it's for **collection of rules** for a conversation.
      It could be seen as a synonym of **conversation**.
      Is it OK for you? Well, we can go on.

The writing of rules for a dialog obviously follows specific syntax, but we will see how to do that later. At first we need to know how to start a dialog.


How to start a dialog: simplest way
-----------------------------------

#. As said just above, we need another file which contains a topic. Create a file (wherever you want, but in a *resource* folder of package is a good idea) with file extension ``.top`` and fill it with this simple conversation (choose the French or English example).

   .. code-block:: none

      topic: ~bonjour()
      language: frf

      u: (Bonjour) Bonjour ! Je m'appelle Pepper.

   or

   .. code-block:: none

      topic: ~hello()
      language: enu

      u: (Hello) Hello! My name is Pepper.

   In few words, we have the *name* of the topic, the *language* and when the user says what is between parentheses, the robot should answer by the remaining of the line (if it manages to recognize what the user said :-) ).
   The name of the file doesn't matter (even if the best way is to use the topic name itself)

#. Now, prepare your empty node: ``dialog_test.py`` (see one of the previous tutorials to create/use a ROS package)
   
#. We can start to manage the dialog. At first, let's start by the imports of the service definitions.

   .. code-block:: python

      # ROS imports
      import rclpy
      from rclpy.node import Node
      from pepper_driver_ext_msgs2.srv import LoadDialogTopic
      from pepper_driver_ext_msgs2.srv import ManageDialogTopic
      from pepper_driver_ext_msgs2.srv import SetAutonomousLifeLevel

   You will notice that the last one is not directly related to dialog management, but we will use it

#. Now, the first step is the initialization of services. We will connect to each of them.
   
   .. code-block:: python

      class DialogTest(Node):
          """Main class of the node."""

          def __init__(self):
              """Create node and client for services."""
              super().__init__('dialog_test')

              # prepare services for loading/unloading
              self.load_dialog_topic = self.create_client(LoadDialogTopic, 'load_dialog_topic')
              while not self.load_dialog_topic.wait_for_service(timeout_sec=1.0):
                  self.get_logger().info('service not available, waiting again...')
              self.manage_dialog_topic = self.create_client(ManageDialogTopic, 'manage_dialog_topic')
              while not self.manage_dialog_topic.wait_for_service(timeout_sec=1.0):
                  self.get_logger().info('service not available, waiting again...')
                   
              
   .. hint::
      As you know, Pepper has got an autonomous life which gives a lot of interaction possibilities.
      But your dialog topic risks to enter in conflict with the dialog topic of the autonomous life!

      So, let's take advantage of the initialization phase to stop the autonomous life. We will use the ``set_autonomous_life_level`` service (with :doc:`/api_msgs/srv/SetAutonomousLifeLevel` format) 

      .. code-block:: python

         # stop autonomous life (to avoid default dialog interaction)
         self.set_autonomous_life_level = self.create_client(SetAutonomousLifeLevel, 'set_autonomous_life_level')
         while not self.set_autonomous_life_level.wait_for_service(timeout_sec=1.0):
             self.get_logger().info('service not available, waiting again...')
         self.request = SetAutonomousLifeLevel.Request()
         self.request.level=SetAutonomousLifeLevel.Request().NO_AUTONOMOUS_LIFE
         self.future = self.set_autonomous_life_level.call_async(self.request)
         rclpy.spin_until_future_complete(self, self.future) 

#. We will now build the function to load a topic, i.e. load the content of a topic file. The *activate* argument is explained in the box below the code.

   - The first step is the retrieving of the content of the topic file:

     .. code-block:: python

        def load_dialog_topic_file(self, topic_file_name, activate):
            """Load a topic with activating or not

            parameter:
                topic_file_name: text file with a topic
                activate (with loading): bool

            return: response of service
            """
            # open the file
            try:
                with open(topic_file_name, "r") as file:
                    content = file.read()
            except OSError:
                self.get_logger().warning(f'[DialogTest] could not open topic file: {topic_file_name}')

   - The second step is the loading itself by the service ``load_dialog_topic``:

     .. code-block:: python

            ...
            # load content topic
            self.request = LoadDialogTopic.Request()
            self.request.topic_content = content
            self.request.activate = activate
            self.future = self.load_dialog_topic.call_async(self.request)
            rclpy.spin_until_future_complete(self, self.future)
            response = self.future.result()
            
            return response

   .. important::
      As you can see the ``load_dialog_topic`` takes the *content* of the file and also a boolean argument **activate**. So we need to talk about the difference between *load* and *activate* a topic.
      To be used a topic need to be *loaded* in the (robot) memory and *activated*. Don't forget also the fact that when a topic is activated, so the rules are used by the robot.
      It also means that you can *activate* or *deactivate* a topic during all the work with the robot according to the topic that you want to focus.

      As usually we use only one topic, we can load and activate at once with ``load_dialog_topic`` (that we will do for this simplest version).
      During the process, if we need to *activate*, *deactivate*, we will use the ``manage_dialog_topic`` service (with :doc:`/api_msgs/srv/ManageDialogTopic`). It will be showed in the next part.

   .. important::
      At the end of this function we have used a *spin* process (to wait the end of the call). It's possible if it's not in a callback function. We will talk a bit more about it later on.
      

#. At last, if we *load* something, it's a good way of coding to foreseen a function to unload (also with ``manage_dialog_topic`` and :doc:`/api_msgs/srv/ManageDialogTopic`).

   .. code-block:: python

      def unload_dialog_topic(self, topic_name):
         """Unload a topic
         
         parameter:
                topic_name: name of the topic

         return: response of the service
         """
         
         self.request = ManageDialogTopic.Request()
         self.request.command = ManageDialogTopic.Request().UNLOAD_TOPIC
         self.request.topic_name = topic_name
         self.future = self.manage_dialog_topic.call_async(self.request)
         rclpy.spin_until_future_complete(self, self.future)
         response = self.future.result()

         return response

   .. hint::
      Unload your topic is not only a good way of coding but also avoid side effect.
      Indeed, when we code, test, code, test,... a topic, we need to be sure that it's the last version of the topic which is in the memory of the robot (from many years of experience ;-)).


#. The final point is the `main function``: the sequence with these 2 main steps *load+activate* / (*use*) / *unload*.

   .. code-block:: python

      def main(args=None):
         """Instantiate then launch a dialog process"""
         rclpy.init(args=args)

         node = DialogTest()

         # load a topic (with activating it)
         topfilename = "/home/...../resource/bonjour.top"

         response = node.load_dialog_topic_file(topfilename, True)
         node.get_logger().info("Result: %r" % (response))
         topicname = response.topic_name

         # run
         try:
            # you can now use the dialog
            print("\nLet's speak to Pepper. Press ^C to stop dialog")
            rclpy.spin(node)
         except KeyboardInterrupt:
            pass
         # end

         # unload the topic
         response = node.unload_dialog_topic(topicname)
         node.get_logger().info("Result: %r" % (response))

         node.destroy_node()

         rclpy.try_shutdown()

      if __name__ == '__main__':
         main()

   .. important::
      
      If you launch this node by a command-line from yu package don't forget to add lines in the 'setup.py' of your package. And don't forget to give the (absolute) path for your topic file.

      .. code-block:: none
         :class: command-line

         ros2 run your_package setup_name_to_launch_dialog_test

      .. hint::

         You can use a parameter for your node for the topic file name. In the '__init__' function, we declare the parameter with the default value.

         .. code-block:: python

            self.declare_parameter('topfilename', "/home/profil/....../resource/bonjour.top")

         In the 'main' function, we retrieve the parameter.

         .. code-block:: python

            topfilename = node.get_parameter('topfilename').value


      .. hint::

         In a launch file you can do better to find easily the right path (with the *find* shell command):

         .. code-block:: xml

            <launch>
               <!-- Arguments -->
               <arg name="topfilename" default="$(find your_package)/resource/bonjour.top" doc="text file with a topic"/>

               <!--  dialog test  -->
               <node pkg="your_package" exec="dialog_test" output="screen">
                  <param name="topfilename" value="$(arg topfilename)"/>
               </node>

            </launch>


         and by:

      .. code-block:: none
         :class: command-line

         roslaunch your_package your_launch_file.launch




How to start a dialog: the full way (with activate/deactivate)
--------------------------------------------------------------

As explained in the previous section, there is a difference between *load* and *activate*. In this section, we will add, to the previous example, a function to activate/deactivate with the ``manage_dialog_topic``.

#. Let's write the function fot the activation/deactivation with ``manage_dialog_topic``. For this, it works with a command message (:doc:`/api_msgs/srv/ManageDialogTopic`). Moreover, to build the topic activation/deactivation function, we could do like the ``load_dialog_topic`` function if we use it out of a *spin* process. But if we want to use it in a callback function (e.g. we would like to activate a new dialog topic when an event appears), it raises an issue due to the management of a *spin* process: we cannot go to inside a new *spin* we are already in a *spin* (it could trigger a deadlock). That why we will see an other solution (with a callback function to receive the response of the *ros service*): 

   .. code-block:: python

      def activate_dialog_topic_cb(self, future):
          response=future.result()
          self.get_logger().info("Result of topic activation: %r" % (response))
          return

      def activate_dialog_topic(self, topic_name, activate):
         """Activate/Deactivate a topic
         (with a callback function)        
         parameter:
                topic_name: name of the topic
                activate: true (Activate) or false (Deactivate)

         return: response of the service
         """
         self.request = ManageDialogTopic.Request()
         if activate:
            self.request.command = ManageDialogTopic.Request().ACTIVATE_TOPIC
         else:
            self.request.command = ManageDialogTopic.Request().DEACTIVATE_TOPIC
         
         self.request.topic_name = topic_name
         self.future_activate = self.manage_dialog_topic.call_async(self.request)
         self.future_activate.add_done_callback(self.activate_dialog_topic_cb)
         
         return 

#. We will rewrite the ``main`` function: the sequence with now these steps *load* / *activate* / (*use*) /  *unload*.

   .. 
      Note We take the opportunity to present how to list loaded/activated topics with ``list_dialog_topics`` ( with a command :doc:`/api_msgs/srv/ListDialogTopics`). (See ROS A to see what put here)
      
   (you can noticed that we load the topic file without activating the topic):

   .. code-block:: python

      def main():
         """Instantiate then launch a dialog process"""
         rclpy.init(args=args)

         node = DialogTest()

         # load a topic (without activating it)
         topfilename = node.get_parameter('topfilename').value
         response = node.load_dialog_topic_file(topfilename, False)
         node.get_logger().info("Result: %r" % (response))

         # activate the topic
         topicname = response.topic_name
         node.activate_dialog_topic(topicname, True)
                    
         # run
         try:
            # you can now use the dialog
            print("\nLet's speak to Pepper. Press ^C to stop dialog")
            rclpy.spin(node)
         except KeyboardInterrupt:
            pass
         # end

         
         # unload the topic
         response = node.unload_dialog_topic(topicname)
         node.get_logger().info("Result: %r" % (response))

         node.destroy_node()

         rclpy.try_shutdown()

      if __name__ == '__main__':
         main()

   It would have been better to use 2 topic files, to play with activate/desactivate, but the main point is here (and you can easily build your own example to test it).

   
Dialog Syntax
-------------

The section presents some basic points about syntax of a topic.

Come back to the topic file. To test the syntax, you need to have already done the previous part of this tutorial.

#. Let recall the basic structure:

   .. code-block:: none

      topic: ~bonjour()
      language: frf

      u: (Bonjour) Bonjour ! Je m'appelle Pepper.

   A *name* of topic (``bonjour``), a *language* (``frf`` --> French) and *rules*.

   A *user rule* is composed of ``u:`` followed by the input of the user ``(input)`` and the remaining is the output pronounced by the robot (triggered by the recognition of the input of the user).

#. About the input, several operators exist to propose choices in the input. You can test all of these:

   .. code-block:: none

      u: ([Bonjour Salut "Hé Pepper"]) Bonjour à vous !
      # with [ ] : choice of words to trigger the response.

      u: (Bonjour je suis {vraiment} content de vous rencontrer) Moi aussi !
      # with { } : the word can be missed.

      u: (Bonjour je suis {[vraiment très]} content de vous rencontrer) Moi aussi !
      # mixing [ ] and { }

      u: (Bonjour je suis * content de vous rencontrer) Moi aussi !
      # with * : replace any part of speech.

      u: (!Bonjour je suis content de vous rencontrer) Moi aussi !
      # with ! : the response will be triggered with "je suis content de vous rencontrer" 
      #          only it's not preceeded by "Bonjour".

   .. hint::
      A set of choice/words can be grouped in a *concept* to be easily used.

      .. code-block:: none

         concept: (salutation) [Bonjour Salut "Hé Pepper"]

         u: (~salutation) Bonjour à vous !

      The robot can also use *concept* : it takes words in the order. It can choose randomly (``^rand``).

      .. code-block:: none

         concept: (salutation) [Bonjour Salut "Bonjour à vous"]

         u: (~salutation) ^rand ~salutation

#. The rules can be composed by a tree structure. Look at this example:

   .. code-block:: none

      u: (Salut) Comment vas tu ?
         u1: (très bien) Parfait !
         u1: (pas très bien) Dommage ! Tu veux en parler ?
            u2: (oui) très bien, raconte moi.
            u2: (non) D'accord, c'est comme tu veux.

   - The *u1* are usable only if the (parent) *u* has been used.
   - The *u2* are usable only if the second *u1* has been used. The *u1: (très bien) Parfait !* doesn't open *u2* rules.
     When we are into a *u2* level, the *u1* are not active any more.
   - All *u* are always reachable.

#. It is also possible to structure the answers of Pepper with *proposal*. In our concern, we will mainly use it in this following way.

   .. code-block:: none

      u: (Salut) Comment vas tu ?
         u1: (très bien) Parfait ! ^goto(demande_nom)
         u1: (pas très bien) Dommage ! Tu veux en parler ?
            u2: (oui) très bien, raconte moi.
            u2: (non) D'accord, c'est comme tu veux.

      proposal: %demande_nom Quel jour de la semaine  sommes-nous ?
         u1: ([Lundi Mardi Mercredi Jeudi Vendredi]) Cool ! J'aime bien ce jour.
         u1: ([Samedi Dimanche]) Super ! J'aime bien le week-end.

   - ``^goto()``: the answer will be completed by the *proposal*
   - ``demande_nom``: is the tag of the *proposal* (``%demande_nom``)
   - ``proposal:``: start a new part of the conversation, not reachable by another way.

   .. note::
      ``proposal:`` can be used with other operators as ``^nextproposal``. See the original page for further explanation.


#. Now we are going to present how to store a value to use it in the dialog. It a key point of the dialog. In the next section we will see how to retrieve and use it in a node!

   An example of dialog with a variable:

      .. code-block:: none

         u: (Salut) Bonjour ! Quel jour de la semaine sommes-nous ?
            u1: (_[Lundi Mardi Mercredi Jeudi Vendredi]) Cool ! J'aime bien le $1 $jour=$1
            u1: (_[Samedi Dimanche]) Super ! J'aime bien le week-end $jour=$1

         u: (Quel jour t'ai-je dit) "Tu m'as indiqué que nous étions $jour"

         u: (Oublie le jour que je t'ai indiqué, s'il te plait) ^clear(jour) c'est oublié !

      - Operator ``_``: store the pronounced word in $1 and can be used in the robot response of the rule. We can store several words with several ``_[...]``. It will be ``$2``, ``$3``, etc.

        .. important::
           For the **English** language, ``_*`` is possible, to store anything! But unfortunately, with **French** language it is **not allowed**!

      - ``$jour``: is the name of the stored variable. We can reuse it in a other *rule* (as you can see in the second *u:*)
      - In the third *u:*, we can remove the variable with ``^clear`` (notice that there is non '$').
         .. important::
            If a variable does not exist (not filled or deleted), a robot answer, which uses this variable, will not be pronounced (neither only the beginning). 
            Here, "``Tu m'as indiqué que nous étions``" will not be pronounced.
       
   We can also conditioned the answer of the robot (even the input). Look at these examples:

      .. code-block:: none

         1. u: (Quel jour t'ai-je dit) ^first["Tu m'as indiqué que nous étions $jour" "Je ne me souviens pas que tu m'aies indiqué un jour"]

         2. u: (J'aime le lundi $jour=="Lundi") Moi aussi j'aime le Lundi.

         3. u: (Mon jour était le Lundi) ["Oui tout à fait $jour=="Lundi" " "Je ne pense pas"]

      1. The first answer is given if the variable exists. If ``$jour`` doesn't exist, therefore the second answer will be used. In other words, ``^first`` asks to evaluate all proposition by starting with the first until one is valid.
      2. The answer will be triggered by ``J'aime le lundi`` if the variable is equal to ``lundi``
      3. Like 2., but for the robot answer.

.. note::

   For more details or other functionalities (for integrating events, tag, focus, etc.) you can go to the original pages of Aldebaran:

   - An `overview <http://doc.aldebaran.com/2-5/naoqi/interaction/dialog/aldialog_syntax_overview.html>`__

   - A `cheat sheet <http://doc.aldebaran.com/2-5/naoqi/interaction/dialog/aldialog_syntax_cheat_sheet.html>`__

   - The `full syntax <http://doc.aldebaran.com/2-5/naoqi/interaction/dialog/dialog-syntax_full.html>`__


How to retrieve a user response to use it in a function
-------------------------------------------------------

For this tutorial we will take the topic example of the choice of a day (used above). So, create your ``choixjour.top`` file with:

   .. literalinclude:: ../../pepper_examples/resources/choixjour.top
      :language: none
      :lines: 1-15

#. For the node to test, you can use the simplest version of the dialog tutorial (i.e when the *loading* is done with activation.)
      We will use the :doc:`/api_nodes/pepper_memory_driver` with one of its services: ``manage_data_stream``. 
      The service will interact with a ros topic to publish value set into Pepper memory. To read and write in the Pepper memory we can use ``get/set_memory_*`` services. But we will see how we can react when a value of memory will modified during a dialog.  

#. Add the imports related to these two services with format: :doc:`/api_msgs/srv/ManageDataStream` and :doc:`/api_msgs/srv/GetMemoryString`. And we need to add 4 extra imports for messages format: :doc:`/api_msgs/msg/MemoryEventInt`, :doc:`/api_msgs/msg/MemoryEventString`, :doc:`/api_msgs/msg/MemoryEventFloat` and :doc:`/api_msgs/msg/MemoryEventBool`

#. Let's recall the story:
   The service ``manage_data_stream`` allows us to be warn when a **variable in memory** is modified (for instance by the dialog). It can create a **ros topic** for each variable. Theoretically, we can retrieve the value of the variable which has been modified and use this variable in a node.

   Unfortunately, we cannot directly access to this **ros topic**. So, the service ``manage_data_stream`` creates 4 **ros topics** to allow us to be warn when a variable in memory with a specified type is modified (Integer, string, float or boolean). We subscribe to these *ros topics* and by filtering messages to keep the only ones corresponding to our **variable in memory**. 
   
   In other terms, the *dialog topic* will "publish" in a *ros topic* (inaccessible to us) when variable will be filled and ``ManageDataStream`` will publish an event to transfer information. The message on this event topic will be the key/value of the variable. So we just need to subscribe to these *ros topics* and when a event is triggered then we can retrieve the content of the variable (in the callback function). Nevertheless, as it is a unique topic for all variables with a specific type, we need to filter the messages.
   
   In the node, we can also retrieve the variable value by ``get_memory_string``. You have noticed that we use :doc:`/api_msgs/srv/GetMemoryString` because the variable is a 'String' (a day of a week). There are other :doc:`types </api_msgs/srv/ManageDataStream>` (int, float and bool).
         
   It could give the following figure:

      .. figure:: /_static/images/dialog_memory2.png
               :alt: simplified memory diagram
               :width: 100%
               :align: center

               *Figure 1 : Simplified memory diagram.*


      .. note::

         There is obviously a service to *set* a variable: ``set_memory_string`` with :doc:`/api_msgs/srv/SetMemoryString` (and other :doc:`types </api_msgs/srv/ManageDataStream>` too). We won't use it in this tutorial.

         Question: *Does ``set_memory_`` trigger a publishing on the ros topic of the variable?*
            
         . 
            
         . 
            
         . 
         
         . 
            
         Answer: *Yes, it does. The variable was modified in memory so a event was published.*   
               

#. At first, in ``__init__``, after loading/unloading dialog topic service, we start with:

   -  Preparing the *ros topic* for our variable *jour* by the ``manage_data_stream``. After advertising the service as usual, we will use it to create the associated *ros topic*.
      
      This service requires a *command* (``START``), name of variable ('jour'),  name of a *ros topic* ('jour_ros_topic')  and *type* of the variable (``STRING``).
        
        .. code-block:: python
            
            # Manage data stream for memory
            self.manage_data_stream = self.create_client(ManageDataStream, 'manage_data_stream')
            while not self.manage_data_stream.wait_for_service(timeout_sec=1.0):
                self.get_logger().info('service not available, waiting again...')
            # Create a ros topic for the variable 'jour'
            self.request = ManageDataStream.Request()
            self.request.command = ManageDataStream.Request().START
            self.request.key = 'jour'
            self.request.topic_name = 'jour_ros_topic'
            self.request.type = ManageDataStream.Request().STRING
            self.future = self.manage_data_stream.call_async(self.request)
            rclpy.spin_until_future_complete(self, self.future)

            # List of variables (used in callback function to check)
            self.list_key_variables =  ("jour")
      
        .. important::

            Be careful, in the request of ``manage_data_stream``, the documentation talk about *topic_name* which is a *ros_topic*, not a *dialog topic*.

            Moreover, if we have got several variables, we need to create several *ros topics*.

   - As we cannot access to this *ros topic*, we will subscribe to the *ros topic* for the type of our variable *jour*. So it will be a :doc:`/api_msgs/msg/MemoryEventString` topic (we don't need to subscribe to other *ros topics* for MemoryEventInt, MemoryEventFloat or MemoryEventBool for this tutorial). We will write our callback function later on.
                
      .. code-block:: python
            
         # Subscription for topic (events: int, float, string or/and bool)
         self.memoryenventstring_sub = self.create_subscription(MemoryEventString, '/memory_event_string', self.memory_event_string_cb, 1)
   
      .. hint::

         As we will filter on our variables (in the callback function), we can create a list of variable keys/names. In this tutorial, we have only one variable: *jour*.

         .. code::python

            self.list_key_variables =  ('jour')

           
   - At last, as we will test ``get_memory_string``,  we create a client for the service ``get_memory_string`` (with :doc:`/api_msgs/srv/GetMemoryString`, don't forget the import) to be able to retrieve value of a **variable in memory**.

      .. code-block:: python

         # Get memory
         self.get_memory_string = self.create_client(GetMemoryString, 'get_memory_string')
         while not self.get_memory_string.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        
#. It remains to create the **callback function** attached to the subscriber. In this function, we have chosen to  display the variable value (contained in the message). 
       
      .. code-block:: python

         def memory_event_string_cb(self, msg: MemoryEventInt):
            """Filter ros topic for a memory event with Int type."""
            if msg.key not in self.list_key_variables:
               return
            elif msg.key == "jour":
               self.jour = msg.value
               self.get_logger().info("Memory modification for %s : %s" % (msg.key , self.jour))
            else :
               self.get_logger().warning(f"Memory event with an unknown key ({msg.key})")
            return

#. As we want to check by ``get_memory_string`` if the variable exists in the memory and display the variable value, even these two steps are redondant (and useless... but it's for the tuto!). Remember that during the dialog the variable can be removed (``^clean()``). So we start by completing the previous callback, just before ``return``:

   .. code-block:: python

      # test of get_memory_string
      self.request = GetMemoryString.Resquest()
      self.request.key = "jour"
      self.future_getmemorystring = self.get_memory_string.call_async(self.request);
      self.future_getmemorystring.add_done_callback(self.get_memory_string_cb)

   And we create the callback function:

   .. code-block:: python

         def get_memory_string_cb(self, future):
            response=future.result()
            self.get_logger().info("Result of get_memory_string: %r" % (response))
            if not response.success:
               self.get_logger().info("The variable doesn't exist.")
            else:
               self.get_logger().info("The value of the variable is: %r" % (response.value))                 
            return
            
         
#. The ``main`` function is unchanged.

   .. note::
      There also are 3 useful services that you can try:

      - ``list_dialog_topics`` ( with a command :doc:`/api_msgs/srv/ListDialogTopics`) to list loaded/activated dialog topics.
      
      - ``get_language`` and ``set_language``
           
   
.. admonition:: Take-home message

   What you have learnt in this tutorial:

   1. How to make a dialog by **loading**, **activating**, **deactivating** and **unloading**.
   2. How to build a *topic* for the dialog, i.e. **write rules with the qiChat syntax**: user input, pepper output, variable use. 
   3. How to **retrieve a variable** of a dialog in a function of a node.
