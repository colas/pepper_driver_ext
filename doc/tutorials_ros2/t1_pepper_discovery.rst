.. sectnum::
   :prefix: T
   :depth: 3
   :start: 1

.. highlight:: none


Pepper discovery
================

.. note::
   This tutorial is only for ROS 2; for ROS 1, check :doc:`/tutorials_ros1/t1_pepper_discovery`.

The aim of this tutorial is to get to know the capabilities of the Pepper robot exposed by the ROS drivers.
It will also be an introduction to exploring a ROS system with its nodes, topics, and services.


.. important::
   You need to have done the previous tutorial: :doc:`/tutorials_ros2/t0_basic_setup` and have access to a Pepper robot with its IP address.


Launching drivers
-----------------

The `pepper_drivers_ext` drivers are for ROS 1 and rely on a deprecated environment.
In order to use the Pepper with ROS 2, there are two elements to have:

- the drivers in a :ref:`docker container <launching_docker>`,
- `ros1_bridge <https://github.com/ros2/ros1_bridge>`_ to bridge messages and services between ROS 1 and 2.


Container creation
^^^^^^^^^^^^^^^^^^

.. note::
   This step must only be done once.

We first need to create a docker container from the ``pepper-driver-prod`` image.
We will name it ``pepper-drivers-noweb-<login>`` to be able to start it easily.

.. hint::
   Replace ``<login>`` by your actual login, for instance ``colas``.
   This prevents container name collision on shared computers.

#. Create a docker container for the drivers with:

   .. code-block:: none
      :class: command-line

      docker create -it --network host --name pepper-drivers-noweb-<login> registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-prod


.. _actual_launching_ros2:

Actual launching
^^^^^^^^^^^^^^^^

.. note::
   These three steps must be done each time you want to start the Pepper drivers.

Once the container is created, it must be started and then, inside, we can launch the drivers.

#. Start your ``pepper-drivers-noweb-<login>`` container (make sure you replace ``<login>``, of course):

   .. code-block:: none
      :class: command-line

      docker start -i pepper-drivers-noweb-<login>

   .. hint::
      To distinguish a shell inside the docker from a shell on the host, you can refer to the user in the prompt: inside the container, the prompt starts with ``pepper@``.

#. Inside this container, launch the drivers using ``roslaunch``:

   .. code-block:: none
      :class: command-line

      roslaunch pepper_driver_ext pepper_driver_all.launch pepper_ip:=<pepper_ip>

   .. important::
      Don't forget to provide the ``pepper_ip`` argument and others if relevant (see :doc:`/launch_files`).

   .. hint::
      You don't need to fully understand this command, it will be covered in more details in the next tutorial.

   .. note::
      You should see a lot of messages being displayed.
      If everything goes well, you shouldn't see any red error message and the drivers should not stop by themselves.

.. note::
   More information in the page on :doc:`/launching`.

   .. hint::
      When you get used to launch the drivers with always the same arguments, you can create a dedicated container as shown in :ref:`quick_launching`.

#. Outside this container, in another terminal window, run the bridge container using:

   .. code-block:: none
      :class: command-line

      docker run --rm -it --env ROS_DOMAIN_ID=$ROS_DOMAIN_ID --network host registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-rosbridge-galactic

   .. note::
      This command should directly launch the bridge and you should see a list of messages indicating topics and services being bridged.


Topics
------

A ROS system involves several nodes communicating through topics, services and actions.
When confronted with such a system, it is important to be able to investigate what is running and what is communicated.
In ROS, there are command-line tools to help perform this investigation.


Topic information
^^^^^^^^^^^^^^^^^

#. In another terminal window, list the topics using:

   .. code-block:: none
      :class: command-line

      ros2 topic list

   It lists the topics that the node declared either subscribing or publishing to; you should see (among others):

   - ``/cmd_vel``: velocity command for the Pepper base,
   - ``/imu``: inertial measurement unit values,
   - ``/joint_states``: values for Pepper joint angles,
   - ``/landmark_detection`` and ``/landmarks``: detected visual markers (two different formats),
   - ``/laser/*/*``: laser data (two different formats),
   - ``/move_base_simple/goal``: goal for onboard navigation,
   - ``/odom``: odometry of the Pepper base,
   - ``/pepper_navigation/front``: some laser information?,
   - ``/pepper_robot/camera/*``: camera images and parameters,
   - ``/pepper_robot/pose/*``: actions for pose control,
   - ``/pepper_robot/sonar/*``: sonar sensor values,
   - ``/rosout``: ROS 1 logs,
   - ``/text_to_speech``: topic to make the Pepper speak,
   - ``/tf`` and ``/tf_static``: transformations between frames.

#. You can use ``ros2 topic info <topic_name>`` to get more information.
   Investigate the ``/joint_states`` topic:

   #. What is its type?
   #. How many subscribers and publishers does it have?

      .. hint::
         You can use the ``-v`` flag to get additional information including the nodes connected through this topic with the Quality of Service (QoS) configuration.

   #. Use ``ros2 interface show <message_type>`` to know the content of a such a message.

      .. hint::
         For ``sensor_msgs/msg/JointState``, you should have:

         .. code-block:: none
            :class: command-line
   
            ros2 interface show sensor_msgs/msg/JointState
   
         .. code-block:: none
   
            # This is a message that holds data to describe the state of a set of torque controlled joints.
            #
            # The state of each joint (revolute or prismatic) is defined by:
            #  * the position of the joint (rad or m),
            #  * the velocity of the joint (rad/s or m/s) and
            #  * the effort that is applied in the joint (Nm or N).
            #
            # Each joint is uniquely identified by its name
            # The header specifies the time at which the joint states were recorded. All the joint states
            # in one message have to be recorded at the same time.
            #
            # This message consists of a multiple arrays, one for each part of the joint state.
            # The goal is to make each of the fields optional. When e.g. your joints have no
            # effort associated with them, you can leave the effort array empty.
            #
            # All arrays in this message should have the same size, or be empty.
            # This is the only way to uniquely associate the joint name with the correct
            # states.
            
            std_msgs/Header header
                    builtin_interfaces/Time stamp
                            int32 sec
                            uint32 nanosec
                    string frame_id
            
            string[] name
            float64[] position
            float64[] velocity
            float64[] effort
   
         It tells us that a ``sensor_msgs/msg/JointState`` message is composed of a header (which is itself composed of a time stamp and a frame identifier), a list of joint names, and three lists of numbers: the position, velocity, and effort of each joint.
   
         For most messages, their definition can be also found on the ROS website, for instance: `sensor_msgs/JointState <https://docs.ros2.org/latest/api/sensor_msgs/msg/JointState.html>`__.
         Message types defined in ``pepper_driver_ext_msgs`` are documented in :doc:`/messages`.

#. Same questions for ``/cmd_vel``, ``/odom``, ``/laser/srd_front/scan``, and ``/text_to_speech``


Reading messages
^^^^^^^^^^^^^^^^

It is also possible to check individual messages being published on topics.

#. Look at the ``/odom`` topic using:

   .. code-block:: none
      :class: command-line

      ros2 topic echo /odom

   You should see an endless series of `nav_msgs/msg/Odometry <https://docs.ros2.org/latest/api/nav_msgs/msg/Odometry.html>`__ messages separated by ``---``.
   If you gently push the robot around, you should see the pose and velocities change.

#. Look at a message on the ``/joint_states`` topic and see what information it contains.

#. Same question for ``/pepper_robot/sonar/front/sonar``.


Publishing messages
^^^^^^^^^^^^^^^^^^^

Finally, you can publish a message directly on the command line.


Speech
""""""

Using topics, one can make the robot speak.

#. In one terminal window, monitor the ``/text_to_speech`` topic (keep it visible). 
#. In another terminal window, make the robot say a sentence using:

   .. code-block:: none
      :class: command-line

      ros2 topic pub --once /text_to_speech std_msgs/msg/String "data: 'Hello'"

   You should see the message appear in your ``ros2 topic echo`` terminal and you should hear the robot say “Hello”.

#. Check the options of ``ros2 topic pub`` using:

   .. code-block:: none
      :class: command-line

      ros2 topic pub --help

   .. hint::
      ``--help`` is the most common command-line flag for command-line programs to display usage information.

   #. Make the robot say “Top” every 2 seconds.
   #. Make it stop!


Motion
""""""

Now we will make the robot move.

   .. note::
      The Pepper is designed to not move when plugged in.
      This is done by deactivating the wheel motors when the charging trapdoor is not closed.

   .. danger::
      Be especially careful when moving the robot: somebody should be around to prevent damages (either to people, the environment, or the robot itself).
      Commands should always be thoroughly considered before being sent to the robot and the robot should have space to maneuver.

#. Send a null velocity command once:

   .. code-block:: none
      :class: command-line

      ros2 topic pub --once /cmd_vel geometry_msgs/msg/Twist "{linear: {x: 0.0, y: 0.0, z: 0.0}, angular: {x: 0.0, y: 0.0, z: 0.0}}"

   .. hint::
      It is a good practice to start sending null commands.
      In particular, it allows faster stopping of the robot by recalling the previous command line.

#. Make the robot turn counterclockwise at 0.5 Rad/s using:

   .. code-block:: none
      :class: command-line

      ros2 topic pub --once /cmd_vel geometry_msgs/msg/Twist "{linear: {x: 0.0, y: 0.0, z: 0.0}, angular: {x: 0.0, y: 0.0, z: 0.5}}"

#. Stop it (by sending a null command) then make it turn clockwise then stop it.
#. Make the robot move forward at 0.10 m/s then stop the robot.

   .. hint::
      In ROS, the convention for a mobile robot is for the x axis to point forward, the y axis leftward, and, consequently, the z axis upward.

   .. danger::
      Recall to have someone not far from the robot so as to be able to stop it in case of trouble.


Services
--------

Service information
^^^^^^^^^^^^^^^^^^^

As we did with topics, we can investigate services.

#. List the services using:

   .. code-block:: none
      :class: command-line

      ros2 service list

   It lists the services that are advertised by the nodes; you should see:

   - ``/{del|get|set}_memory_*``: handle Pepper memory,
   - ``/{get|set}_autonomous_life_level``: see or change the level of Pepper's autonomous life,
   - ``/{get|set}_language``: see or change the language of dialogs on Pepper,
   - ``/list_dialog_topics``: list topics of Pepper's dialog engine,
   - ``/{load|manage}_dialog_topic``: load and manage topics of Pepper's dialog engine,
   - ``/manage_data_stream``: connect Pepper memory to ROS topics,
   - ``/pepper_robot/camera/*``: manage Pepper camera,
   - ``/pepper_robot/pose/*``: control Pepper stiffness and motion,
   - ``/text_to_speech``: make Pepper speak.

   .. hint::
      You can use the ``-t`` flag to know the service type.

#. Investigate the ``/text_to_speech`` service:

   #. What is its type?
   #. What are its content?

   .. hint::
      As with messages, you can use ``ros2 interface show <service_type>`` to know the arguments and output of a given service type.
      For instance:

      .. code-block:: none
         :class: command-line

         ros2 interface show pepper_driver_ext_msgs2/srv/TextToSpeech

      .. code-block:: none

         # This service provides text-to-speech conversion on the Pepper robot.

         string text
         ---
         bool success
         string error_message

      It tells us that the arguments of a ``pepper_driver_ext_msgs2/srv/TextToSpeech`` request consist of a single string (the text to speak aloud), and that the response to a request consists in a success Boolean and, if needed an error message.

      For most services, their definition can be also found on the ROS website, for instance: `std_srvs/srv/SetBool <https://docs.ros2.org/latest/api/std_srvs/srv/SetBool.html>`__.
      Service types defined in ``pepper_driver_ext_msgs2`` are documented in :doc:`/messages`.

#. Same questions for ``/get_language``, ``/set_language``, ``/load_dialog_topic``, and ``/list_dialog_topics``.


Service call
^^^^^^^^^^^^

Contrary to topics, one cannot listen to service requests or responses made by other nodes.
However, one can call services from the command line.

#. Check the current language of the dialog engine using:

   .. code-block:: none
      :class: command-line

      ros2 service call /get_language pepper_driver_ext_msgs2/srv/GetLanguage "{}"

   .. code-block:: none

      requester: making request: pepper_driver_ext_msgs2.srv.GetLanguage_Request()
      
      response:
      pepper_driver_ext_msgs2.srv.GetLanguage_Response(success=True, error_message='', language='French')

#. Change the language to ``English`` using the ``/set_language`` service.

   .. hint::
      For service with parameters, the syntax in the brackets is ``{parameter_name: 'value'}``.

#. Use the ``/text_to_speech`` *service* (and not topic) to make the robot say “Hello” then “Bonjour” and listen to its accent.
#. Switch back the language to ``French`` and listen to the robot say “Hello” and “Bonjour” again.
   Did you notice a difference?


Nodes
-----

The launch file in the docker driver starts several ROS 1 nodes, however, from ROS 2, the only visible node is the bridge.

#. List the nodes using:

   .. code-block:: none
      :class: command-line

      ros2 node list

   It lists the ROS 2 nodes launched; you should see:

   - ``/ros_bridge``: the bridge between ROS 1 and 2.

#. Investigate the ``/ros_bridge`` node using the ``ros2 node info <node_name>`` command:

   #. Which topics does it subscribe to (if any)?
   #. Which topics does it publish on (if any)?
   #. Which services does it advertise (if any)?

   .. hint::
      As ``/ros_bridge`` is the only node, the lists of its topics and services is the same as those above.


Stopping drivers
----------------

Care should be taken to always stop the robot drivers at the end of the session.

#. Switch to the docker terminal where the bridge is launched and press ``ctrl+c`` to terminate the bridge (it will automatically exit the docker container).
#. Switch to the docker terminal where the drivers are launched and:

   #. press ``ctrl+c`` to terminate the launch files with the drivers;
   #. when finished press ``ctrl+d`` to exit the docker container.


.. admonition:: Take-home message

   At the start of this tutorial, you learned to :ref:`launch the drivers <actual_launching_ros2>` of the Pepper robot.

   You have also learned to investigate topics using ``ros2 topic list`` and ``ros2 topic info`` and to listen or publish to them using respectively ``ros2 topic echo`` or ``ros2 topic pub``.
   Similarly, you have used ``ros2 service list``, ``ros2 service info``, and ``ros2 service call`` to, respectively, list, get information, and use ROS services.
