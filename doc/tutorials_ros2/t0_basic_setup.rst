.. sectnum::
   :prefix: T
   :depth: 3
   :start: 0

.. highlight:: none


Basic setup and ROS workspace
=============================

.. note::
   This tutorial is only for ROS 2; for ROS 1, check :doc:`/tutorials_ros1/t0_basic_setup`.

The aim of this tutorial is to explain the basic setup to use ``pepper_driver_ext``.
It is a longer and step-by-step version of the :doc:`/installation` instructions aimed at ROS beginners.
In particular, it details the environment and workspace creation for new packages.

The target setup is `Ubuntu 20.04 <https://releases.ubuntu.com/20.04/>`__ with `ROS galactic <https://docs.ros.org/en/galactic>`__ and `docker <https://www.docker.com>`__ (`prerequisites`_) but it has also been tested with `Ubuntu 22.04 <https://releases.ubuntu.com/22.04/>`__ and `ROS humble <https://docs.ros.org/en/humble>`__
The ROS 1 drivers and a ROS bridge for ROS 2 are running in docker containers while user nodes are run on the host.
The latter part requires installing the ``pepper_driver_ext_msgs2`` message definitions on the host in a `ROS workspace`_.

.. hint::
   You don't need a robot for this tutorial: it stops right before launching the robot (see :doc:`/tutorials_ros2/t1_pepper_discovery`).


Prerequisites
-------------

.. important::
   This section requires sudoer rights on the computer.

   In the HP_I240 room, ROS and docker are already installed with additional packages.
   You can therefore skip this whole section to jump to `ROS environment`_.

You need to:

#. Install `Ubuntu 20.04 <https://releases.ubuntu.com/20.04/>`__: for instance, by following `this tutorial <https://ubuntu.com/tutorials/install-ubuntu-desktop>`__.
#. Install `ROS galactic <https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians>`__ using the recommended desktop install of ``ros-galactic-desktop``.
   
   .. note::
      You may skip the `environment setup <https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians.html#environment-setup>`__ step as it will be detailed and explained below.

#. Install `Docker Engine <https://docs.docker.com/engine/install/ubuntu/>`__ and setup your user to run ``docker`` without ``sudo`` by following the first `post-installation step for Linux <https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user>`__.

   .. note::
      Other post-installation steps are not as important for us here.

.. #. Install additional packages:

   .. code-block:: none
      :class: command-line

      sudo apt install ???


ROS environment
---------------


A key question is the location of files and executables and, hence, how tools can find them.
This is mostly handled with `environment variables <https://en.wikipedia.org/wiki/Environment_variable>`__.

By default, ROS 2 commands are not located where the system looks for executables (defined in the ``PATH`` environment variable).
Therefore commands like ``ros2`` are not available at the start event if they are installed.
Moreover package using `colcon` requires some environment variables to be set.
This was done so that one could have several ROS distributions on the same system.

.. hint::
   You can check commands are not accessible by running, for instance:

   .. code-block:: none
      :class: command-line

      ros2
    
   You should see::

       bash: ros2: command not found

   However, with their full path, you can see they are installed:

   .. code-block:: none
      :class: command-line

      ls -l /opt/ros/galactic/bin/ros2

   This should display::

       -rwxr-xr-x 1 root root 379 déc.   7  2022 /opt/ros/galactic/bin/ros2


Therefore, before starting to work in ROS, it is necessary to properly setup the environment.
This is done by the ``/opt/ros/galactic/setup.bash`` script which needs to be `sourced <https://linuxcommand.org/lc3_man_pages/sourceh.html>`__ using:

.. code-block:: none
   :class: command-line

   source /opt/ros/galactic/setup.bash

It needs to be done in each shell, which is easy to forget.
Fortunately, there is a way to have the shell execute a command when it starts: putting that command in the ``.bashrc`` file located in the `home directory <https://en.wikipedia.org/wiki/Home_directory>`__ of the user (its shortcut in the shell is the `~ <https://en.wikipedia.org/wiki/Tilde#Directories_and_URLs>`__ character, which you will find prefixing several paths on this page).
This can be done by editing this file using a text editor (like `vim <https://en.wikipedia.org/wiki/Vim_(text_editor)>`__ or `nano <https://en.wikipedia.org/wiki/GNU_nano>`__) or, simply, using the following command:

.. code-block:: none
   :class: command-line

   echo -e "\n# ROS setup\nif [ -f /opt/ros/galactic/setup.bash ]; then\n    source /opt/ros/galactic/setup.bash\nfi" >> ~/.bashrc

.. hint::

   You can check the last few line of a file with the `tail <https://manpages.ubuntu.com/manpages/focal/en/man1/tail.1.html>`__ command:

   .. code-block:: none
      :class: command-line
      
      tail -4 ~/.bashrc

   .. code-block:: none

      # ROS setup
      if [ -f /opt/ros/galactic/setup.bash ]; then
          source /opt/ros/galactic/setup.bash
      fi

   .. note::
      The ``if [ -f ... ]`` construct checks that the file exists before attempting to source it.
      It prevents errors when sharing the same ``.bashrc`` across different computers.

Once that is done, every subsequent shell should be properly configured.
This can be checked by looking at various environment variables such as ``ROS_DISTRO``, ``ROS_VERSION``, or ``AMENT_PREFIX_PATH``:

.. code-block:: none
   :class: command-line

   printenv AMENT_PREFIX_PATH

.. code-block:: none

   /opt/ros/galactic

.. code-block:: none
   :class: command-line

   env | grep ROS

.. code-block:: none

   ROS_VERSION=2
   ROS_PYTHON_VERSION=3
   ROS_LOCALHOST_ONLY=0
   ROS_DISTRO=galactic

.. important::
   If the commands above do not return anything, it means that the sourcing was not done.
   If, later, you have unexpected errors like ``ros2: command not found``, then that is most probably the issue.


ROS workspace
-------------

ROS works with packages put into ROS workspaces.
The ROS packages installed on the system are in the distribution workspace (under ``/opt/ros/galactic``).
But it is not where we will work.

Workspaces can be overlayed; that is, packages are looked for in a first workspace and, if it is not found, the search continues in the second workspace and so on.
Usually, we have only a user workspace and the system one but it could be more complex.


Creating the workspace
^^^^^^^^^^^^^^^^^^^^^^

A ROS workspace has a specific structure described in `REP 128 <https://www.ros.org/reps/rep-0128.html>`__ (it discusses `catkin` workspaces from ROS 1, but the structure has been kept in ROS 2).
This structure is complex but, fortunately, tools allow us to handle it easily.
The main directories we need to be concerned with are the directory where the workspace is located and where we will put source files.

We will first create an empty workspace called ``ros2_ws``.
At the minimum, it needs to have a ``src`` subdirectory for the sources.
It will look like::

    /home
    └-- <user>
        └-- ros2_ws
            └-- src

These directories can be created using the `mkdir <https://manpages.ubuntu.com/manpages/focal/en/man1/mkdir.1.html>`__ command:

.. code-block:: none
   :class: command-line

   mkdir -p ~/ros2_ws/src


Building the workspace
^^^^^^^^^^^^^^^^^^^^^^

A ROS workspace is made to host packages so that they can be compiled then used.
This compilation is, of course, necessary for C++ nodes, but also for defining messages or services and, also, for packages to be found by ROS commands.

Building a workspace at least once is therefore mandatory, even for pure Python packages.
This is done, from the ``ros2_ws`` directory with:

.. code-block:: none
   :class: command-line

   colcon build

You can check (for instance with `ls <https://manpages.ubuntu.com/manpages/focal/en/man1/ls.1.html>`__: ``ls ~/ros2_ws``) that more directories have been created::

    ros2_ws
    ├-- build
    ├-- install
    ├-- logs
    └-- src

There are also various files and, in particular there is a new ``ros2_ws/install/local_setup.bash``.
This file serves the same purpose as ``/opt/ros/galactic/setup.bash``, that is, activating the workspace so that its packages can be used.
Therefore, we also need to source it in the same way:

.. code-block:: none
   :class: command-line

   source ~/ros2_ws/install/local_setup.bash
   
This sourcing should be done after all compilations that changed significantly the contents of the workspace: new package, for instance.

As for ``/opt/ros/galactic/setup.bash``, put it in your ``.bashrc``:

.. code-block:: none
   :class: command-line

   echo -e "\n# ROS workspace\nif [ -f ~/ros2_ws/install/local_setup.bash ]; then\n    source ~/ros2_ws/install/local_setup.bash\nfi" >> ~/.bashrc

.. hint::
   Now, the last 9 lines of your ``~/.bashrc`` should look like:

   .. code-block:: none
      :class: command-line

      tail -9 ~/.bashrc

   .. code-block:: none

      # ROS setup
      if [ -f /opt/ros/galactic/setup.bash ]; then
          source /opt/ros/galactic/setup.bash
      fi

      # ROS workspace
      if [ -f ~/ros2_ws/install/local_setup.bash ]; then
           source ~/ros2_ws/install/local_setup.bash
      fi


Pepper driver packages
----------------------

Now that the workspace is ready, we can finish with the installation of the ``pepper_driver_ext_msgs2`` and ``pepper_examples_ros2`` packages.


Getting the packages
^^^^^^^^^^^^^^^^^^^^

The easiest is to clone the whole git repository:

.. code-block:: none
   :class: command-line

   cd && git clone https://gitlab.inria.fr/colas/pepper_driver_ext.git

.. important::
   You should avoid cloning the repository directly in the ``ros2_ws/src`` directory since your workspace would then include the ``pepper_driver_ext`` package which is only meant for Ubuntu 16.04 as well as other ROS 1 packages.

Now, you can include the ``pepper_driver_ext_msgs2`` and ``pepper_examples_ros2`` packages in your workspace using a symbolic link:

.. code-block:: none
   :class: command-line

   ln -s ~/pepper_driver_ext/pepper_driver_ext_msgs2 ~/ros2_ws/src/

and:

.. code-block:: none
   :class: command-line

   ln -s ~/pepper_driver_ext/pepper_examples_ros2 ~/ros2_ws/src/


Recompiling the workspace
^^^^^^^^^^^^^^^^^^^^^^^^^

Now that the package is in its proper place, we can recompile the workspace:

.. code-block:: none
   :class: command-line

   colcon build

The output should look like::

    Starting >>> pepper_driver_ext_msgs2
    Finished <<< pepper_driver_ext_msgs2 [17.6s]
    Starting >>> pepper_examples_ros2
    Finished <<< pepper_examples_ros2 [0.77s]
    Summary: 2 packages finished [18.6s]

.. important::
   Make sure you don't have errors in the compilation.
   If it complains with an error such like ``Could not find a package configuration file provided by "whatever"``, then it means there are some missing dependencies.


And, since we added new ROS packages, we need to source again the corresponding ``local_setup.bash``:

.. code-block:: none
   :class: command-line

   source ~/ros2_ws/install/local_setup.bash


Checking setup
--------------

Once that is done, our setup should be ready and our environment should be populated with the paths of the ``pepper_driver_ext_msgs`` and ``pepper_examples`` packages:

.. code-block:: none
   :class: command-line

   printenv AMENT_PREFIX_PATH

::

   /home/<user>/ros2_ws/install/pepper_examples_ros2:/home/<user>/ros2_ws/install/pepper_driver_ext_msgs2:/opt/ros/galactic

.. note::
   If the packages are not there, you missed a step somewhere.
   Check you didn't have any compilation issue and the ``pepper_driver_ext_msgs2`` and ``pepper_examples_ros2`` links are in their right place in ``~/ros2_ws/src/`` and correctly point to the sources.

   Remember also that the sourcing needs to happen after the compilation in every terminal window.

The ``pepper_driver_ext_msgs2`` defines a series of messages and services (see :doc:`/messages`).
You can check that they are properly found:

.. code-block:: none
   :class: command-line

   ros2 interface show pepper_driver_ext_msgs2/msg/AutonomousLifeLevel

::

   # This message defines the 4 levels of autonomous life
   # defined for the Pepper robot.
   # Defined autonomous life levels
   # no autonomous life nor motion (joint stiffness is 0)
   uint8 REST=0
   # no autonomous life but stiff
   uint8 WAKE_UP=1
   # some autonomous motion but don't try to interact
   uint8 NO_AUTONOMOUS_LIFE=2
   # full autonomous life as when started up
   uint8 FULL_AUTONOMOUS_LIFE=3
   
   uint8 level


.. admonition:: Take-home message

   You've learned to create a ROS workspace, place ROS packages in the ``src`` directory, and compile them with the ``colcon build`` command.

   .. important::
      During your work, remember to build the workspace but also ``source`` the ``install/local_setup.bash`` script whenever you make changes.
