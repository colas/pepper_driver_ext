Installation
============

Overview
--------

This project proposes several ROS packages:

- ``pepper_driver_ext``: the additional drivers,
- ``pepper_driver_ext_msgs``: ROS 1 message definitions,
- ``pepper_driver_ext_msgs2``: ROS 2 message definitions,
- ``pepper_examples``: ROS 1 example code using the drivers.
- ``pepper_examples_ros2``: ROS 2 example code using the drivers.

There are two elements which need to be installed: the :ref:`drivers <drivers>` themselves and the :ref:`message definitions <messages>` (along with examples if desired).


.. _drivers:

Driver installation
-------------------

Due to the deprecation of the Pepper library underlying the drivers, it is preferable to use a `docker <https://www.docker.com>`_ container to use the drivers.

.. Note::
   If required, a :doc:`manual installation <manual_installation>` is possible under Ubuntu 16.04 with ROS Kinetic.


ROS 1 docker image
^^^^^^^^^^^^^^^^^^

The only system installation required is `docker`_ itself (see `docker install instructions <https://docs.docker.com/engine/install/>`__).

Several docker images are available in the `container registry <https://gitlab.inria.fr/colas/pepper_driver_ext/container_registry>`_ of the project.
The ``pepper-driver-prod`` image can be used to run the drivers on other systems than Ubuntu 16.04.

You can pull this image locally using:

.. code-block:: none
   :class: command-line

   docker image pull registry.gitlab.inria.fr/colas/pepper_driver_ext/pepper-driver-prod


ROS 2 docker image
^^^^^^^^^^^^^^^^^^

In order to use the drivers with ROS 2, you need the `ROS 1 docker image`_ above as well as a specifically compiled bridge.
There are two docker images provided:

- `pepper-rosbridge-galactic` for use with ROS 2 Galactic (using `Cyclone DDS`),
- `pepper-rosbridge-humble` for use with ROS 2 Humble (using `FastRTPS`).

You can pull the corresponding image locally.


.. _messages:

Messages and examples
---------------------

Contrary to the drivers, the message definitions and examples are compatible with recent (and still supported) versions of ROS and Ubuntu.


ROS 1 messages and examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prerequisites:

- an `installation of ROS 1 <http://wiki.ros.org/noetic/Installation>`__ (Kinetic or above).

Installation instructions:

- create a `catkin workspace <https://catkin-tools.readthedocs.io/en/latest/verbs/catkin_init.html>`_ (or reuse an existing one),
- link or copy the ``pepper_driver_ext_msgs`` and ``pepper_examples`` folders in the ``src/`` directory of your `catkin` workspace,
- `build <https://catkin-tools.readthedocs.io/en/latest/verbs/catkin_build.html>`_ the workspace,
- `source <https://manpages.ubuntu.com/manpages/xenial/en/man1/bash.1.html#shell%20builtin%20commands>`_ the ``devel/setup.bash`` from the workspace in your ``~/.bashrc``.

.. note::
   We advise using `catkin_tools <https://catkin-tools.readthedocs.io/en/latest/index.html>`__ rather than the default `catkin <http://wiki.ros.org/catkin>`__ command line utilities since it is more convenient.


ROS 2 messages and examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prerequisites:

- an `installation of ROS 2 <https://docs.ros.org/en/hummble/installation.html>`__.


Installation instructions:

- create a `ROS 2 workspace <https://docs.ros.org/en/humble/Tutorials/Beginner-Client-Libraries/Creating-A-Workspace/Creating-A-Workspace.html>`__ (or reuse an existing one),
- link or copy the ``pepper_driver_ext_msgs2`` and ``pepper_examples_ros2`` folders in the ``src/`` repository of your ROS 2 workspace,
- `build <https://docs.ros.org/en/humble/Tutorials/Beginner-Client-Libraries/Creating-A-Workspace/Creating-A-Workspace.html#build-the-workspace-with-colcon>`__ the workspace with `colcon <https://colcon.readthedocs.io/en/released/>`__,
- `source`_ the ``install/local_setup.bash`` from the workspace in your ``~/.bashrc``.
