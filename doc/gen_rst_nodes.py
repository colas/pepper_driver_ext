#!/usr/bin/env python

"""Generate a .rst file from a module docstring.

Replaces autodoc in case importing is difficult (as is the
case with ROS and its complex environment).
"""

import argparse
import ast
import glob
import os.path


def gen_rst(input_filename, output_directory):
    """Generate rst in output directory from input_filename."""
    # get docstring
    with open(input_filename, 'r') as f:
        tree = ast.parse(f.read())
    docstring = ast.get_docstring(tree)
    if docstring is None:
        return
    # get module name
    basename = os.path.basename(input_filename)
    modulename = os.path.splitext(basename)[0]
    # write .rst
    with open(os.path.join(output_directory, '%s.rst' % modulename), 'w') as f:
        f.write('``%s``\n' % modulename)
        f.write('='*(4+len(modulename)) + '\n\n')
        f.write(docstring)


def gen_rsts(input_directory, output_directory):
    """Generate all rsts from modules in input_directory."""
    # list files
    for f in glob.glob(os.path.join(input_directory, '*.py')):
        gen_rst(f, output_directory)


def main():
    """Parse args."""
    parser = argparse.ArgumentParser(
        description='Generate rst from module docstring.'
    )
    parser.add_argument('-o', '--output_directory', default='.',
                        help='output directory')
    parser.add_argument('path', nargs='*', default='.',
                        help='path to look for modules')
    args = parser.parse_args()
    for p in args.path:
        if os.path.isdir(p):
            gen_rsts(p, args.output_directory)
        elif os.path.isfile(p):
            gen_rst(p, args.output_directory)


if __name__ == '__main__':
    main()
