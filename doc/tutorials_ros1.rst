Tutorials for ROS 1
===================

These tutorials are meant to help you get started solving basic tasks with the Pepper robot.
The first three also serve as guides through the basics of ROS 1:

- the :doc:`first one </tutorials_ros1/t0_basic_setup>` explains catkin workspaces and ROS package local installation;
- the :doc:`second </tutorials_ros1/t1_pepper_discovery>` presents the command-line tools for nodes, topics, and services;
- the :doc:`third </tutorials_ros1/t2_pepper_teleop>` guides through the creation of a ROS package, a simple node, and the associated launch file.

The other tutorials explain specific usage of specific functionalities of the Pepper robot (without getting back to the basics).


Contents
--------

.. toctree::
   :maxdepth: 1
   :glob:

   tutorials_ros1/*
