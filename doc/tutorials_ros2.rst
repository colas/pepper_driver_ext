Tutorials for ROS 2
===================

These tutorials are meant to help you get started solving basic tasks with the Pepper robot.
The first three also serve as guides through the basics of ROS 2:

- the :doc:`first one </tutorials_ros2/t0_basic_setup>` explains workspaces and ROS package local installation;
- the :doc:`second </tutorials_ros2/t1_pepper_discovery>` presents the command-line tools for nodes, topics, and services;
- the :doc:`third </tutorials_ros2/t2_pepper_teleop>` guides through the creation of a ROS package, a simple node, and the associated launch file.

The other tutorials explain specific usage of specific functionalities of the Pepper robot (without getting back to the basics).


Contents
--------

.. toctree::
   :maxdepth: 1
   :glob:

   tutorials_ros2/*
