"""Joystick teleoperation node.

This node computes velocity commands from joystick messages.

It subscribes to:
    - `/joy` (sensor_msgs/msg/Joy): joystick message.

It publishes to:
    - `/cmd_vel` (geometry_msgs/msg/Twist): command velocity.
"""

# ROS imports
import rclpy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from rclpy.node import Node


class TeleopJoy(Node):
    """Joystick teleoperation."""

    def __init__(self):
        """Create publisher and subscriber."""
        super().__init__('teleop_joy')
        self.lin_vel_scaling = 0.35  # m/s/unit
        self.ang_vel_scaling = 1.0  # rad/s/unit
        # publisher on /cmd_vel
        self.cmd_vel_pub = self.create_publisher(Twist, '/cmd_vel', 10)
        # subscriber to /joy
        self.joy_sub = self.create_subscription(Joy, '/joy', self.joy_cb, 10)
        self.joy_sub  # prevent unused variable warning

    def joy_cb(self, msg: Joy):
        """Callback on joystick message."""
        # initialize message
        cmd_vel_msg = Twist()
        # fill message
        cmd_vel_msg.linear.x = self.lin_vel_scaling * msg.axes[1]
        cmd_vel_msg.linear.y = self.lin_vel_scaling * msg.axes[2]
        cmd_vel_msg.linear.z = 0.
        cmd_vel_msg.angular.x = 0.
        cmd_vel_msg.angular.y = 0.
        cmd_vel_msg.angular.z = self.ang_vel_scaling * msg.axes[0]
        # publish message
        self.cmd_vel_pub.publish(cmd_vel_msg)


def main(args=None):
    """Instantiate node."""
    rclpy.init(args=args)
    # create node
    teleop_joy_node = TeleopJoy()
    # spin forever
    try:
        rclpy.spin(teleop_joy_node)
    except KeyboardInterrupt:
        pass
    # end
    rclpy.shutdown()


if __name__ == '__main__':
    main()
