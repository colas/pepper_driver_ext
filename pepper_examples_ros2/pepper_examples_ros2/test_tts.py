"""Test of the text-to-speech interfaces to the Pepper robot."""

# standard imports
import time
# ROS imports
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from pepper_driver_ext_msgs2.srv import TextToSpeech


class TestTTS(Node):
    """Test of the text-to-speech"""

    def __init__(self):
        """Create publisher service proxy"""
        super().__init__('test_tts')
        self.tts_pub = self.create_publisher(String, '/text_to_speech', 10)
        self.tts_client = self.create_client(TextToSpeech, '/text_to_speech')
        while not self.tts_client.wait_for_service(timeout_sec=2.0):
            self.get_logger().info('/text_to_speech service not available, '
                                   'waiting...')

    def say_msg(self, text: str):
        """Say text by sending a message."""
        msg = String()
        msg.data = text
        self.get_logger().info(f"[TestTTS] sending message: {text!r}")
        self.tts_pub.publish(msg)

    def say_srv(self, text: str):
        """Say text by a service request."""
        req = TextToSpeech.Request()
        req.text = text
        self.get_logger().info(f"[TestTTS] sending request: {text!r}")
        future = self.tts_client.call_async(req)
        rclpy.spin_until_future_complete(self, future)
        response = future.result()
        self.get_logger().info(f"[TestTTS] result: {response!r}")


def main(args=None):
    rclpy.init(args=args)
    test_tts = TestTTS()
    test_tts.get_logger().info("[TestTTS] node initialized")
    time.sleep(1)
    test_tts.say_msg('Message pour dire bonjour')
    time.sleep(5)
    test_tts.say_srv('Service pour dire bonjour')
    time.sleep(1)
    rclpy.shutdown()


if __name__ == '__main__':
    main()
