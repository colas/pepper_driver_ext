from setuptools import find_packages, setup

package_name = 'pepper_examples_ros2'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/launch',
            ['launch/teleop_joy_launch.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Francis Colas',
    maintainer_email='francis.colas@inria.fr',
    description='A few nodes to test the Pepper robot from ROS2',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'test_tts = pepper_examples_ros2.test_tts:main',
            'teleop_joy = pepper_examples_ros2.teleop_joy:main',
        ],
    },
)
