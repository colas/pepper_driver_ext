FROM ros:noetic AS pepper-rosbridge-humble

# Create user
RUN useradd --create-home --shell /bin/bash --home-dir /home/pepper pepper
WORKDIR /home/pepper

# Humble dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    curl \
    software-properties-common \
    && rm -rf /var/lib/apt/lists/*
RUN add-apt-repository universe

RUN curl -SL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" > /etc/apt/sources.list.d/ros2.list

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    python3-flake8-docstrings \
    python3-pip \
    python3-pytest-cov \
    ros-dev-tools \
    && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install \
    flake8-blind-except \
    flake8-builtins \
    flake8-class-newline \
    flake8-comprehensions \
    flake8-deprecated \
    flake8-import-order \
    flake8-quotes \
    "pytest>=5.3" \
    pytest-repeat \
    pytest-rerunfailures

# Humble source
USER pepper
RUN mkdir -p ros2_humble/src
RUN cd ros2_humble && \
    vcs import --input https://raw.githubusercontent.com/ros2/ros2/humble/ros2.repos src

# Humble dependencies, again
USER root
RUN apt-get update && \
    rosdep update && \
    cd ros2_humble && \
    rosdep install --from-paths src --ignore-src -y --skip-keys "fastcdr rti-connext-dds-6.0.1 urdfdom_headers" \
    && rm -rf /var/lib/apt/lists/*
USER pepper

# local packages
COPY --chown=pepper:pepper pepper_driver_ext_msgs2 ros2_humble/src/pepper_driver_ext_msgs2
ADD --chown=pepper:pepper resources/naoqi_bridge_msgs2-2.1.0.tar.gz ros2_humble/src/
# apply mapping rules patch
USER root
RUN chown -R pepper:pepper ros2_humble/src/naoqi_bridge_msgs2-2.1.0
USER pepper
COPY --chown=pepper:pepper resources/naoqi_bridge_msgs_mapping_rules.patch ros2_humble/src/naoqi_bridge_msgs2-2.1.0/
RUN cd ros2_humble/src/naoqi_bridge_msgs2-2.1.0 && \
    patch -p1 < naoqi_bridge_msgs_mapping_rules.patch

# Humble compilation
RUN cd ros2_humble && \
    colcon build --packages-up-to \
        ament_cmake \
        ament_index_python \
        builtin_interfaces \
        rclcpp \
        rcutils \
        ros2run \
        rosidl_cmake \
        rosidl_parser \
        rmw_implementation_cmake \
        diagnostic_msgs \
        geometry_msgs \
        nav_msgs \
        sensor_msgs \
        std_msgs \
        std_srvs \
        tf2_msgs \
        visualization_msgs \
        naoqi_bridge_msgs \
        pepper_driver_ext_msgs2 \
    && rm -rf build/ log/

# additional noetic packages
USER root
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ros-noetic-tf2-msgs \
    ros-noetic-naoqi-bridge-msgs \
    && rm -rf /var/lib/apt/lists/*
USER pepper

# ROS1 workspace
RUN mkdir -p catkin_ws/src
COPY --chown=pepper:pepper pepper_driver_ext_msgs catkin_ws/src/pepper_driver_ext_msgs
RUN . /opt/ros/noetic/setup.sh && \
    cd catkin_ws && \
    catkin_make_isolated --install

# bridge
RUN mkdir -p bridge_ws/src
ADD --chown=pepper:pepper resources/ros1_bridge_3d5328d.tar.gz bridge_ws/src
# bridge compilation
RUN . /opt/ros/noetic/setup.sh && \
    . catkin_ws/install_isolated/setup.sh && \
    unset ROS_DISTRO && \
    . ros2_humble/install/local_setup.sh && \
    cd bridge_ws && \
    colcon build --cmake-force-configure && \
    rm -rf build/ log/

# entrypoint
COPY --chown=pepper:pepper resources/rosbridge_humble_entrypoint.sh /home/pepper
RUN chmod u+x /home/pepper/rosbridge_humble_entrypoint.sh
ENTRYPOINT ["/home/pepper/rosbridge_humble_entrypoint.sh"]

# configuration
COPY --chown=pepper:pepper resources/fastrtps_udp_transport_profile.xml /home/pepper
COPY --chown=pepper:pepper resources/param_bridge_topics.yaml /home/pepper
COPY --chown=pepper:pepper resources/param_bridge_services.yaml /home/pepper

# default command
COPY --chown=pepper:pepper resources/start_parameter_bridge.sh /home/pepper
RUN chmod a+x /home/pepper/start_parameter_bridge.sh
CMD ["/home/pepper/start_parameter_bridge.sh"]
