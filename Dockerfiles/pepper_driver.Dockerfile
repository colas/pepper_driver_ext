######################
# pepper-driver-base #
######################
# pepper-driver-base: ros + official pepper packages + empty catkin workspace + user
FROM ros:kinetic-ros-base-xenial AS pepper-driver-base

# Install packages
RUN apt-get update && \
    apt-get install -q -y --no-install-recommends \
    python-catkin-tools \
    ros-kinetic-pepper-dcm-bringup \
    ros-kinetic-pepper-robot \
    && rm -rf /var/lib/apt/lists/*

# Create Pepper user
RUN useradd --create-home --shell /bin/bash --home-dir /home/pepper --password "AMrAx/LQvZmzs" pepper
USER pepper
WORKDIR /home/pepper

# Install python naoqi
COPY --chown=pepper:pepper resources/pynaoqi-python2.7-2.5.5.5-linux64.tar.gz .
RUN tar -xzf pynaoqi-python2.7-2.5.5.5-linux64.tar.gz && rm pynaoqi-python2.7-2.5.5.5-linux64.tar.gz
RUN echo "export PYTHONPATH=/home/pepper/pynaoqi-python2.7-2.5.5.5-linux64/lib/python2.7/site-packages:${PYTHONPATH}" >> /home/pepper/.bashrc

# Create catkin workspace
RUN mkdir -p catkin_ws/src && \
    . /opt/ros/kinetic/setup.sh && \
    catkin init -w catkin_ws && \
    catkin build -w catkin_ws

# Source catkin environment in new shells
RUN echo "source /home/pepper/catkin_ws/devel/setup.bash" >> /home/pepper/.bashrc


#######################
# pepper-driver-devel #
#######################
# pepper-driver-devel: pepper-driver-base + sudo for pepper user
FROM pepper-driver-base AS pepper-driver-devel
USER root
RUN adduser pepper sudo
USER pepper

######################
# pepper-driver-prod #
######################
# pepper-driver-prod: pepper-driver-base + copy of current driver version

FROM pepper-driver-base AS pepper-driver-prod

# Copy drivers
COPY --chown=pepper:pepper pepper_driver_ext_msgs catkin_ws/src/pepper_driver_ext_msgs
COPY --chown=pepper:pepper pepper_driver_ext catkin_ws/src/pepper_driver_ext

# Build catkin workspace with drivers
RUN . /opt/ros/kinetic/setup.sh && unset LD_PRELOAD && \
    catkin build -w catkin_ws


#####################
# pepper-driver-doc #
#####################
# pepper-driver-doc: pepper-driver-base + sphinx to compile doc

FROM pepper-driver-base AS pepper-driver-doc

# Install pip
USER root
RUN apt-get update && \
    apt-get install -q -y --no-install-recommends \
    python-pip \
    preview-latex-style \
    texlive-latex-extra \
    lmodern \
    && rm -rf /var/lib/apt/lists/*

# Install recent sphinx with pip as well as python2 dependencies
RUN python -m pip install \
    babel==2.9.0 \
    requests==2.27.1 \
    Jinja2==2.11.3 \
    MarkupSafe==1.1.1 \
    certifi==2020.4.5.2 \
    packaging==20.9 \
    snowballstemmer==2.1.0 \
    Pygments==2.5.2 \
    sphinx==1.8.6 \
    alabaster==0.7.12 \
    sphinxcontrib-websupport==1.2.3

USER pepper
# assume pepper_driver_ext will have been bound on /home/pepper/pepper_driver_ext
WORKDIR /home/pepper/pepper_driver_ext/doc
# default command
CMD ["make"]
