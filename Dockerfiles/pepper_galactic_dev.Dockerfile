FROM ros:galactic-ros-base-focal AS pepper-galactic-dev

# Additional packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    bash-completion \
    ros-galactic-desktop \
    ros-galactic-naoqi-bridge-msgs \
    && rm -rf /var/lib/apt/lists/*

# Create user
RUN useradd --create-home --shell /bin/bash --home-dir /home/pepper --password "AMrAx/LQvZmzs" pepper
RUN adduser pepper sudo
USER pepper

# Create ros2 workspace
RUN mkdir -p /home/pepper/ros2_ws/src

# Entrypoint and default command
COPY --chown=pepper:pepper resources/galactic_dev_entrypoint.sh /home/pepper/entrypoint.sh
RUN chmod a+x /home/pepper/entrypoint.sh
ENTRYPOINT ["/home/pepper/entrypoint.sh"]
CMD ["bash", "-i"]
WORKDIR /home/pepper
