FROM ros:galactic-ros1-bridge-focal AS pepper-rosbridge-galactic

# Additional packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ros-noetic-tf2-msgs \
    ros-noetic-naoqi-bridge-msgs \
    && rm -rf /var/lib/apt/lists/*

# Create user
RUN useradd --create-home --shell /bin/bash --home-dir /home/pepper pepper
USER pepper
WORKDIR /home/pepper

# Create catkin workspace
RUN mkdir -p catkin_ws/src

# Create ros2 workspace
RUN mkdir -p ros2_ws/src

# Create bridge workspace
RUN mkdir -p bridge_ws/src

# Copy message packages
COPY --chown=pepper:pepper pepper_driver_ext_msgs catkin_ws/src/pepper_driver_ext_msgs
COPY --chown=pepper:pepper pepper_driver_ext_msgs2 ros2_ws/src/pepper_driver_ext_msgs2
ADD --chown=pepper:pepper resources/naoqi_bridge_msgs2-2.1.0.tar.gz ros2_ws/src/
# apply mapping rules patch
USER root
RUN chown -R pepper:pepper ros2_ws/src/naoqi_bridge_msgs2-2.1.0
USER pepper
COPY --chown=pepper:pepper resources/naoqi_bridge_msgs_mapping_rules.patch ros2_ws/src/naoqi_bridge_msgs2-2.1.0/
RUN cd ros2_ws/src/naoqi_bridge_msgs2-2.1.0 && \
    patch -p1 < naoqi_bridge_msgs_mapping_rules.patch

# copy bridge code
ADD --chown=pepper:pepper resources/ros1_bridge_3d5328d.tar.gz bridge_ws/src

# Build ROS1 message definitions
RUN unset ROS_DISTRO && \
    . /opt/ros/noetic/setup.sh && \
    cd catkin_ws && \
    catkin_make_isolated --install

# Build ROS2 message definitions
RUN . /opt/ros/galactic/setup.sh && \
    cd ros2_ws && \
    colcon build && \
    rm -rf build/ log/

# Build bridge
RUN unset ROS_DISTRO && \
    . /opt/ros/noetic/setup.sh && \
    unset ROS_DISTRO &&\
    . /opt/ros/galactic/setup.sh && \
    unset ROS_DISTRO &&\
    . catkin_ws/install_isolated/setup.sh && \
    . ros2_ws/install/local_setup.sh && \
    cd bridge_ws && \
    colcon build --cmake-force-configure && \
    rm -rf build/ log/

# entrypoint
COPY --chown=pepper:pepper resources/rosbridge_galactic_entrypoint.sh /home/pepper
RUN chmod u+x /home/pepper/rosbridge_galactic_entrypoint.sh
ENTRYPOINT ["/home/pepper/rosbridge_galactic_entrypoint.sh"]

# parameter bridge configuration
COPY --chown=pepper:pepper resources/param_bridge_topics.yaml /home/pepper
COPY --chown=pepper:pepper resources/param_bridge_services.yaml /home/pepper

# default command
COPY --chown=pepper:pepper resources/start_parameter_bridge.sh /home/pepper
RUN chmod a+x /home/pepper/start_parameter_bridge.sh
CMD ["/home/pepper/start_parameter_bridge.sh"]
