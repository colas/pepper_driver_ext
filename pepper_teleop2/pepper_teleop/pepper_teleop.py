"""Joystick teleoperation node for the Pepper robot.

This node computes velocity commands from joystick messages.

It subscribes to:
    - `/joy` (sensor_msgs/msg/Joy): joystick message.

It publishes to:
    - `/cmd_vel` (geometry_msgs/msg/Twist): command velocity.

Parameters:
    - `max_lin_vel`: maximum linear velocity in m/s
        (default: 0.35).
    - `max_ang_vel`: maximum angular velocity in rad/s
        (default: 1.0).
"""

import rclpy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from rclpy.node import Node


class PepperTeleop(Node):
    """Joystick teleoperation for Pepper robot."""

    def __init__(self):
        """Create publisher and subscriber."""
        super().__init__('pepper_teleop')
        # parameters
        self.declare_parameter('max_lin_vel', 0.35)
        self.declare_parameter('max_ang_vel', 1.0)
        # publisher
        self.cmd_vel_pub = self.create_publisher(Twist, '/cmd_vel', 1)
        # subscription
        self.joy_sub = self.create_subscription(Joy, '/joy', self.joy_cb, 1)

    def joy_cb(self, msg: Joy):
        """Compute and publish twist command."""
        # initialize message
        cmd_vel_msg = Twist()
        # get current parameter values
        max_lin_vel = self.get_parameter('max_lin_vel').value
        max_ang_vel = self.get_parameter('max_ang_vel').value
        # fill message
        cmd_vel_msg.linear.x = max_lin_vel * msg.axes[1]
        cmd_vel_msg.linear.y = max_lin_vel * msg.axes[2]
        cmd_vel_msg.linear.z = 0.
        cmd_vel_msg.angular.x = 0.
        cmd_vel_msg.angular.y = 0.
        cmd_vel_msg.angular.z = max_ang_vel * msg.axes[0]
        # publish message
        self.cmd_vel_pub.publish(cmd_vel_msg)


def main(args=None):
    """Instantiate node and class."""
    rclpy.init(args=args)
    # create node
    pepper_teleop_node = PepperTeleop()
    # run
    try:
        rclpy.spin(pepper_teleop_node)
    except KeyboardInterrupt:
        pass
    # end
    rclpy.shutdown()


if __name__ == '__main__':
    main()
