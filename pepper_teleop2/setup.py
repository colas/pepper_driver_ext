from setuptools import find_packages, setup

package_name = 'pepper_teleop'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/launch',
            ['launch/pepper_teleop_launch.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='colas',
    maintainer_email='francis.colas@inria.fr',
    description='TODO: Package description',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'pepper_teleop = pepper_teleop.pepper_teleop:main',
        ],
    },
)
