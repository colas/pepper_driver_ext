#!/usr/bin/env python2
"""
This node handles the Pepper tablet.

It provides access to the method to display a web page or an image
as well as the touch coordinates.
It actually contains a small static web server to serve the files in
a given directory if needed.

Services:
    - `tablet_display` (pepper_driver_ext_msgs/TabletDisplay):
        specify url of image or html page to display on the tablet.

Publications:
    - `tablet_touch_event` (pepper_driver_ext_msgs/TabletTouchEvent):
        coordinates and kind of touch event on the tablet.

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
    - `web_ip`: IP address of the web server from the Pepper robot.
    - `web_port`: port for web server (default: 8080).
    - `web_directory`: directory of the files to serve.
"""

# standard imports
import os.path
from functools import partial
# ROS imports
import rospy
# third party imports
import qi
from pepper_driver_ext_msgs.msg import TabletTouchEvent
from pepper_driver_ext_msgs.srv import TabletDisplay, TabletDisplayRequest
# local imports
from pepper_driver_ext.utils import get_ros_param
from pepper_driver_ext.http_server import SimpleThreadedServer


class PepperTabletDriver(object):
    """Main class of the dialog driver."""

    def __init__(self):
        """Create web server and ROS service and publisher."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[Tablet] ')
        pepper_port = 9559  # never seen any other port: no point in param
        web_ip = get_ros_param('~web_ip', log_prefix='[Tablet] ')
        web_port = get_ros_param('~web_port', 8080, log_prefix='[Tablet] ')
        web_directory = get_ros_param('~web_directory', log_prefix='[Tablet] ')
        self.web_url_root = 'http://%s:%d/' % (web_ip, web_port)
        self.check_frequency = 5  # TOS topic subscription check frequency (Hz)
        # session and connection
        self._session = qi.Session()
        self._url = 'tcp://%s:%d' % (pepper_ip, pepper_port)
        self._ts = None
        self._conns = {}  # connections to all touch events
        self.subscribed = False  # node ready or not to publish touch events
        # web server
        self.web_server = SimpleThreadedServer(web_directory, web_port)
        # ROS publisher
        self.touch_event_pub = rospy.Publisher(
            'tablet_touch_event', TabletTouchEvent, queue_size=5, latch=True)
        # ROS service
        self.tablet_display_srv = rospy.Service(
            'tablet_display', TabletDisplay, self.tablet_display)

    def get_tablet(self):
        """Connect to the ALTabletService module on Pepper.

        Return:
            proxy to ALTabletService.

        Raise:
            RuntimeError: in case of connection problem.
        """
        connected = self._session.isConnected()
        rospy.logdebug("[Tablet] connected to robot: %r", connected)
        if connected:
            return self._ts
        rospy.logdebug("[Tablet] connecting to robot...")
        self._session.connect(self._url)
        rospy.logdebug("[Tablet] connecting to ALTabletService...")
        self._ts = self._session.service('ALTabletService')
        return self._ts

    def _te_callback(self, event_type, event_name, x, y):
        """Callback generator to publish touch events.

        Actual callbacks must be partial functions with
        provided `event_type` and `event_name` arguments.
        """
        rospy.logdebug("[Tablet] Received %s event in (%.2f, %.2f).",
                       event_name, x, y)
        # create and fill message
        msg = TabletTouchEvent()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = 'torso'
        msg.event = event_type
        msg.x = x
        msg.y = y
        # publish message
        self.touch_event_pub.publish(msg)

    def subscribe(self):
        """Subscribe to the touch signals.

        Signals are:
            - onTouchDown
            - onTouchMove
            - onTouchUp
        """
        assert self.subscribed is False
        # mapping between event names and ROS message id
        event_dict = {
            'onTouchDown': TabletTouchEvent.TOUCH_DOWN,
            'onTouchMove': TabletTouchEvent.TOUCH_MOVE,
            'onTouchUp': TabletTouchEvent.TOUCH_UP
        }
        try:
            ts = self.get_tablet()
            rospy.logdebug("[Tablet] subscribing to touch events %r...",
                           tuple(event_dict.keys()))
            # subscribe to all events and storing of connection
            for event_name, event_type in event_dict.iteritems():
                self._conns[event_name] = getattr(ts, event_name).connect(
                    partial(self._te_callback, event_type, event_name))
        except RuntimeError as e:
            rospy.logwarn("[Tablet] Could not connect to the touch events: %s",
                          str(e))
        else:
            rospy.loginfo("[Tablet] Success in subscribing to touch events.")
            self.subscribed = True

    def unsubscribe(self):
        """Unsubscribe to the touch signals."""
        assert self.subscribed
        try:
            ts = self.get_tablet()
            rospy.logdebug("[Tablet] unsubscribing from touch events %r...",
                           tuple(self._conns.keys()))
            while self._conns:
                event_name, conn = self._conns.popitem()
                getattr(ts, event_name).disconnect(conn)
        except RuntimeError as e:
            rospy.logwarn("[Tablet] Could not fully unsubscribe: %s", str(e))
        else:
            rospy.loginfo("[Tablet] Success in unsubscribing from touch "
                          "events.")
        finally:
            # whether or not there was an issue, connections are over
            self._conns = {}
            self.subscribed = False

    def tablet_display(self, req):
        """Control what the tablet displays."""
        def make_url(path):
            """Create url from a relative path"""
            # collapse internal redundancies
            normalized_path = os.path.normpath(path)
            # remove starting '/' if present
            if normalized_path.startswith('/'):
                normalized_path = normalized_path[1:]
            # error if trying to get out of root
            if normalized_path.startswith('../') or normalized_path == '..':
                raise RuntimeError("invalid path: %s" % path)
            return self.web_url_root + normalized_path

        rospy.logdebug("[Tablet] tablet_display request: %r", req)
        display_type = req.display_type
        # default
        success = True
        error_message = ''
        try:
            # get ALTabletService proxy
            ts = self.get_tablet()
            if display_type == TabletDisplayRequest.SLEEP:
                # adapt web server status
                if self.web_server.started:
                    rospy.logdebug("[Tablet] stopping web server...")
                    self.web_server.stop()
                # actual display change
                ts.goToSleep()
            elif display_type == TabletDisplayRequest.IDLE:
                # adapt web server status
                if self.web_server.started:
                    rospy.logdebug("[Tablet] stopping web server...")
                    self.web_server.stop()
                # actual display change
                ts.wakeUp()
                ts.hide()
            elif display_type == TabletDisplayRequest.WEB:
                # adapt web server status
                if not self.web_server.started:
                    rospy.logdebug("[Tablet] starting web server...")
                    self.web_server.start()
                # compose url
                url = make_url(req.path)
                ts.enableWifi()  # ensure tablet wifi is on
                rospy.logdebug("[Tablet] url to display: %r", url)
                # actual display change
                ts.wakeUp()
                res = ts.showWebview(url)
                if not res:
                    raise RuntimeError("url unreachable: %r" % url)
            elif display_type == TabletDisplayRequest.IMAGE:
                # adapt web server status
                if not self.web_server.started:
                    rospy.logdebug("[Tablet] starting web server...")
                    self.web_server.start()
                # compose url
                url = make_url(req.path)
                # TODO check if wifi is already on
                ts.enableWifi()  # ensure tablet wifi is on
                rospy.logdebug("[Tablet] url to display: %r", url)
                # actual display change
                ts.wakeUp()
                res = ts.showImage(url)
                if not res:
                    raise RuntimeError(
                        "url unreachable or not an image: %r" % url)
            else:
                raise RuntimeError("Unknown display type: %d" % display_type)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Tablet] Failed changing tablet display: %s",
                          error_message)
        response = (success, error_message)
        rospy.logdebug("[Tablet] tablet_display response: %r", response)
        return response

    def run(self):
        """Check connection status to subscribe"""
        r = rospy.Rate(self.check_frequency)
        while not rospy.is_shutdown():
            r.sleep()
            # subscribe or unsubscribe from touch events on the robot depending
            # on the presence of ROS subscribers to the `touch_event` topic.
            if self.touch_event_pub.get_num_connections():
                if not self.subscribed:
                    rospy.logdebug("[Tablet] subscribing to touch events...")
                    self.subscribe()
            else:
                if self.subscribed:
                    rospy.logdebug("[Tablet] unsubscribing from touch "
                                   "events...")
                    self.unsubscribe()

    def cleanup(self):
        """Stops web server and disconnect from touch events."""
        rospy.logdebug("[Tablet] cleanup...")
        try:
            if self.subscribed:
                rospy.logdebug("[Tablet] unsubscribing from touch events...")
                self.unsubscribe()
            if self.web_server.started:
                rospy.logdebug("[Tablet] stopping web server...")
                try:
                    self.web_server.stop()
                except RuntimeError as e:
                    rospy.logwarn("[Tablet] could not stop web server: %s",
                                  str(e))
        except AttributeError:
            pass  # __init__ did not finish due to missing parameters
        rospy.logdebug("[Tablet] cleanup done")

    def __del__(self):
        """Cleanup."""
        self.cleanup()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_tablet_driver')
        td = PepperTabletDriver()
        td.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
