#!/usr/bin/env python2
"""
This node handles the Pepper LEDs through a few services.

It can query a single LED color (ear LEDs only have the blue
component), set the color of a set of LEDs (defined by a mask), and
set a execute a list of color commands.

LEDs have an index and an associated mask (`1 << index`): a set of
LEDs can therefore be encoded with a logical or applied on the
desired LEDs masks.

For this node, LEDs are named:
    - right eye: `EYE_R0`, `EYE_R1`, `EYE_R2`, `EYE_R3`, `EYE_R4`,
        `EYE_R5`, `EYE_R6`, `EYE_R7`
    - left eye: `EYE_L0`, `EYE_L1`, `EYE_L2`, `EYE_L3`, `EYE_L4`,
        `EYE_L5`, `EYE_L6`, `EYE_L7`
    - right ear: `EAR_R0`, `EAR_R1`, `EAR_R2`, `EAR_R3`, `EAR_R4`,
        `EAR_R5`, `EAR_R6`, `EAR_R7`, `EAR_R8`, `EAR_R9`
    - left ear: `EAR_L0`, `EAR_L1`, `EAR_L2`, `EAR_L3`, `EAR_L4`,
        `EAR_L5`, `EAR_L6`, `EAR_L7`, `EAR_L8`, `EAR_L9`
    - shoulders: `SHOULDERS`

See `the Pepper LED placement <http://doc.aldebaran.com/2-5/family/pepper_technical/leds_pep.html#led-pepper>`_
documentation for reference.


Services:
    - `get_led_color` (pepper_driver_ext_msgs/GetLEDColor): query the
        color from a single led.
    - `set_leds_color` (pepper_driver_ext_msgs/SetLEDsColor): sets
        the color of a set of LEDs.
    - `set_leds_color_list` (pepper_driver_ext_msgs/SetLEDsColorList):
        execute a list of color commands.

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import (
    GetLEDColor, GetLEDColorRequest, GetLEDColorResponse,
    SetLEDsColor, SetLEDsColorRequest, SetLEDsColorResponse,
    SetLEDsColorList, SetLEDsColorListRequest, SetLEDsColorListResponse)
# third party import
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class PepperLEDDriver(object):
    """Main class of the LED driver"""

    def __init__(self):
        """Create services"""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[LED] ')
        pepper_port = 9559  # never seen any other port: no point in param
        # session
        self._session = qi.Session()
        self._url = 'tcp://%s:%d' % (pepper_ip, pepper_port)
        self._leds = None
        # LED name mapping and variables
        self._get_led_color_map = self.__get_led_color_map()
        self._set_leds_color_map = self.__set_leds_color_map()
        self._set_leds_color_list_map = self.__set_leds_color_list_map()
        self._nb_leds = 37
        self._led_groups = set()
        # ROS services
        self.get_led_color_srv = rospy.Service(
            'get_led_color', GetLEDColor, self.get_led_color)
        self.set_leds_color_srv = rospy.Service(
            'set_leds_color', SetLEDsColor, self.set_leds_color)
        self.set_leds_color_list_srv = rospy.Service(
            'set_leds_color_list', SetLEDsColorList, self.set_leds_color_list)

    def __get_led_color_map(self):
        """Mapping between request constant and LED names"""
        return {
            # Right eye
            GetLEDColorRequest.EYE_R0:
                ['Face/Led/Red/Right/45Deg/Actuator/Value',
                 'Face/Led/Green/Right/45Deg/Actuator/Value',
                 'Face/Led/Blue/Right/45Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_R1:
                ['Face/Led/Red/Right/0Deg/Actuator/Value',
                 'Face/Led/Green/Right/0Deg/Actuator/Value',
                 'Face/Led/Blue/Right/0Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_R2:
                ['Face/Led/Red/Right/315Deg/Actuator/Value',
                 'Face/Led/Green/Right/315Deg/Actuator/Value',
                 'Face/Led/Blue/Right/315Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_R3:
                ['Face/Led/Red/Right/270Deg/Actuator/Value',
                 'Face/Led/Green/Right/270Deg/Actuator/Value',
                 'Face/Led/Blue/Right/270Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_R4:
                ['Face/Led/Red/Right/225Deg/Actuator/Value',
                 'Face/Led/Green/Right/225Deg/Actuator/Value',
                 'Face/Led/Blue/Right/225Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_R5:
                ['Face/Led/Red/Right/180Deg/Actuator/Value',
                 'Face/Led/Green/Right/180Deg/Actuator/Value',
                 'Face/Led/Blue/Right/180Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_R6:
                ['Face/Led/Red/Right/135Deg/Actuator/Value',
                 'Face/Led/Green/Right/135Deg/Actuator/Value',
                 'Face/Led/Blue/Right/135Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_R7:
                ['Face/Led/Red/Right/90Deg/Actuator/Value',
                 'Face/Led/Green/Right/90Deg/Actuator/Value',
                 'Face/Led/Blue/Right/90Deg/Actuator/Value'],
            # Left eye
            GetLEDColorRequest.EYE_L0:
                ['Face/Led/Red/Left/45Deg/Actuator/Value',
                 'Face/Led/Green/Left/45Deg/Actuator/Value',
                 'Face/Led/Blue/Left/45Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_L1:
                ['Face/Led/Red/Left/0Deg/Actuator/Value',
                 'Face/Led/Green/Left/0Deg/Actuator/Value',
                 'Face/Led/Blue/Left/0Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_L2:
                ['Face/Led/Red/Left/315Deg/Actuator/Value',
                 'Face/Led/Green/Left/315Deg/Actuator/Value',
                 'Face/Led/Blue/Left/315Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_L3:
                ['Face/Led/Red/Left/270Deg/Actuator/Value',
                 'Face/Led/Green/Left/270Deg/Actuator/Value',
                 'Face/Led/Blue/Left/270Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_L4:
                ['Face/Led/Red/Left/225Deg/Actuator/Value',
                 'Face/Led/Green/Left/225Deg/Actuator/Value',
                 'Face/Led/Blue/Left/225Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_L5:
                ['Face/Led/Red/Left/180Deg/Actuator/Value',
                 'Face/Led/Green/Left/180Deg/Actuator/Value',
                 'Face/Led/Blue/Left/180Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_L6:
                ['Face/Led/Red/Left/135Deg/Actuator/Value',
                 'Face/Led/Green/Left/135Deg/Actuator/Value',
                 'Face/Led/Blue/Left/135Deg/Actuator/Value'],
            GetLEDColorRequest.EYE_L7:
                ['Face/Led/Red/Left/90Deg/Actuator/Value',
                 'Face/Led/Green/Left/90Deg/Actuator/Value',
                 'Face/Led/Blue/Left/90Deg/Actuator/Value'],
            # Right ear
            GetLEDColorRequest.EAR_R0:
                ['Ears/Led/Right/0Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R1:
                ['Ears/Led/Right/36Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R2:
                ['Ears/Led/Right/72Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R3:
                ['Ears/Led/Right/108Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R4:
                ['Ears/Led/Right/144Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R5:
                ['Ears/Led/Right/180Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R6:
                ['Ears/Led/Right/216Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R7:
                ['Ears/Led/Right/252Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R8:
                ['Ears/Led/Right/288Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_R9:
                ['Ears/Led/Right/324Deg/Actuator/Value'],
            # Left ear
            GetLEDColorRequest.EAR_L0:
                ['Ears/Led/Left/0Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L1:
                ['Ears/Led/Left/36Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L2:
                ['Ears/Led/Left/72Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L3:
                ['Ears/Led/Left/108Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L4:
                ['Ears/Led/Left/144Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L5:
                ['Ears/Led/Left/180Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L6:
                ['Ears/Led/Left/216Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L7:
                ['Ears/Led/Left/252Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L8:
                ['Ears/Led/Left/288Deg/Actuator/Value'],
            GetLEDColorRequest.EAR_L9:
                ['Ears/Led/Left/324Deg/Actuator/Value'],
            # shoulders
            GetLEDColorRequest.SHOULDERS:
                ['ChestBoard/Led/Red/Actuator/Value',
                 'ChestBoard/Led/Green/Actuator/Value',
                 'ChestBoard/Led/Blue/Actuator/Value']
        }

    def __set_leds_color_map(self):
        """Mapping between request constant and LED names"""
        return {
            # Right eye
            SetLEDsColorRequest.EYE_R0: 'RightFaceLed8',
            SetLEDsColorRequest.EYE_R1: 'RightFaceLed7',
            SetLEDsColorRequest.EYE_R2: 'RightFaceLed6',
            SetLEDsColorRequest.EYE_R3: 'RightFaceLed5',
            SetLEDsColorRequest.EYE_R4: 'RightFaceLed4',
            SetLEDsColorRequest.EYE_R5: 'RightFaceLed3',
            SetLEDsColorRequest.EYE_R6: 'RightFaceLed2',
            SetLEDsColorRequest.EYE_R7: 'RightFaceLed1',
            # Left eye
            SetLEDsColorRequest.EYE_L0: 'LeftFaceLed1',
            SetLEDsColorRequest.EYE_L1: 'LeftFaceLed2',
            SetLEDsColorRequest.EYE_L2: 'LeftFaceLed3',
            SetLEDsColorRequest.EYE_L3: 'LeftFaceLed4',
            SetLEDsColorRequest.EYE_L4: 'LeftFaceLed5',
            SetLEDsColorRequest.EYE_L5: 'LeftFaceLed6',
            SetLEDsColorRequest.EYE_L6: 'LeftFaceLed7',
            SetLEDsColorRequest.EYE_L7: 'LeftFaceLed8',
            # Right ear
            SetLEDsColorRequest.EAR_R0: 'RightEarLed1',
            SetLEDsColorRequest.EAR_R1: 'RightEarLed2',
            SetLEDsColorRequest.EAR_R2: 'RightEarLed3',
            SetLEDsColorRequest.EAR_R3: 'RightEarLed4',
            SetLEDsColorRequest.EAR_R4: 'RightEarLed5',
            SetLEDsColorRequest.EAR_R5: 'RightEarLed6',
            SetLEDsColorRequest.EAR_R6: 'RightEarLed7',
            SetLEDsColorRequest.EAR_R7: 'RightEarLed8',
            SetLEDsColorRequest.EAR_R8: 'RightEarLed9',
            SetLEDsColorRequest.EAR_R9: 'RightEarLed10',
            # Left ear
            SetLEDsColorRequest.EAR_L0: 'LeftEarLed1',
            SetLEDsColorRequest.EAR_L1: 'LeftEarLed2',
            SetLEDsColorRequest.EAR_L2: 'LeftEarLed3',
            SetLEDsColorRequest.EAR_L3: 'LeftEarLed4',
            SetLEDsColorRequest.EAR_L4: 'LeftEarLed5',
            SetLEDsColorRequest.EAR_L5: 'LeftEarLed6',
            SetLEDsColorRequest.EAR_L6: 'LeftEarLed7',
            SetLEDsColorRequest.EAR_L7: 'LeftEarLed8',
            SetLEDsColorRequest.EAR_L8: 'LeftEarLed9',
            SetLEDsColorRequest.EAR_L9: 'LeftEarLed10',
            # shoulders
            SetLEDsColorRequest.SHOULDERS: 'ChestLeds'
        }

    def __set_leds_color_list_map(self):
        """Mapping between request constant and LED names"""
        return {
            # Right eye
            SetLEDsColorListRequest.EYE_R0: 'RightFaceLed8',
            SetLEDsColorListRequest.EYE_R1: 'RightFaceLed7',
            SetLEDsColorListRequest.EYE_R2: 'RightFaceLed6',
            SetLEDsColorListRequest.EYE_R3: 'RightFaceLed5',
            SetLEDsColorListRequest.EYE_R4: 'RightFaceLed4',
            SetLEDsColorListRequest.EYE_R5: 'RightFaceLed3',
            SetLEDsColorListRequest.EYE_R6: 'RightFaceLed2',
            SetLEDsColorListRequest.EYE_R7: 'RightFaceLed1',
            # Left eye
            SetLEDsColorListRequest.EYE_L0: 'LeftFaceLed1',
            SetLEDsColorListRequest.EYE_L1: 'LeftFaceLed2',
            SetLEDsColorListRequest.EYE_L2: 'LeftFaceLed3',
            SetLEDsColorListRequest.EYE_L3: 'LeftFaceLed4',
            SetLEDsColorListRequest.EYE_L4: 'LeftFaceLed5',
            SetLEDsColorListRequest.EYE_L5: 'LeftFaceLed6',
            SetLEDsColorListRequest.EYE_L6: 'LeftFaceLed7',
            SetLEDsColorListRequest.EYE_L7: 'LeftFaceLed8',
            # Right ear
            SetLEDsColorRequest.EAR_R0: 'RightEarLed1',
            SetLEDsColorRequest.EAR_R1: 'RightEarLed2',
            SetLEDsColorRequest.EAR_R2: 'RightEarLed3',
            SetLEDsColorRequest.EAR_R3: 'RightEarLed4',
            SetLEDsColorRequest.EAR_R4: 'RightEarLed5',
            SetLEDsColorRequest.EAR_R5: 'RightEarLed6',
            SetLEDsColorRequest.EAR_R6: 'RightEarLed7',
            SetLEDsColorRequest.EAR_R7: 'RightEarLed8',
            SetLEDsColorRequest.EAR_R8: 'RightEarLed9',
            SetLEDsColorRequest.EAR_R9: 'RightEarLed10',
            # Left ear
            SetLEDsColorRequest.EAR_L0: 'LeftEarLed1',
            SetLEDsColorRequest.EAR_L1: 'LeftEarLed2',
            SetLEDsColorRequest.EAR_L2: 'LeftEarLed3',
            SetLEDsColorRequest.EAR_L3: 'LeftEarLed4',
            SetLEDsColorRequest.EAR_L4: 'LeftEarLed5',
            SetLEDsColorRequest.EAR_L5: 'LeftEarLed6',
            SetLEDsColorRequest.EAR_L6: 'LeftEarLed7',
            SetLEDsColorRequest.EAR_L7: 'LeftEarLed8',
            SetLEDsColorRequest.EAR_L8: 'LeftEarLed9',
            SetLEDsColorRequest.EAR_L9: 'LeftEarLed10',
            # shoulders
            SetLEDsColorListRequest.SHOULDERS: 'ChestLeds'
        }

    def get_leds(self):
        """Connect to the ALLeds module from the Pepper.

        Return:
            proxy to the ALLeds module

        Raise:
            RuntimeError: in case of connection problem.
        """
        connected = self._session.isConnected()
        rospy.logdebug("[LED] connected to robot: %r", connected)
        if connected:
            return self._leds
        rospy.logdebug("[LED] connecting to robot...")
        self._session.connect(self._url)
        rospy.logdebug("[LED] connecting to ALLeds...")
        self._leds = self._session.service('ALLeds')
        return self._leds

    def get_led_color(self, req):
        """Get color of given LED"""
        rospy.logdebug("[LED] get_led_color request: %r", req)
        # default values
        success = True
        error_message = ''
        color = 0
        # get LED name
        try:
            led_names = self._get_led_color_map[req.led_index]
        except KeyError:
            success = False
            error_message = "Unknown LED with index %d" % (req.led_index,)
            rospy.logwarn("[LED] %s", error_message)
            return GetLEDColorResponse(success, error_message, color)
        # connect to the robot
        try:
            leds = self.get_leds()
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[LED] Failed connecting to the robot: %s",
                          error_message)
            return GetLEDColorResponse(success, error_message, color)
        # get each component
        color_components = []
        for led_name in led_names:
            rospy.logdebug("[LED] Getting intensity for: %s...", led_name)
            color_comp = leds.getIntensity(led_name)
            rospy.logdebug("[LED] Intensity for %s: %f", led_name, color_comp)
            color_components.append(int(round(255*color_comp)))
        # build color
        if len(led_names) == 1:
            color = color_components[0]
        elif len(led_names) == 3:
            color = sum(comp << n
                        for comp, n in zip(color_components, [16, 8, 0]))
        else:
            assert False
        # response
        response = GetLEDColorResponse(success, error_message, color)
        rospy.logdebug("[LED] get_led_color response: %r", response)
        return response

    def _build_led_list(self, led_mask, led_map):
        """List of LEDS from given mask"""
        current_led = 1
        led_list = []
        for _ in range(self._nb_leds):
            if not (current_led & led_mask):
                current_led <<= 1
                continue
            assert current_led in led_map
            led_list.append(led_map[current_led])
            current_led <<= 1
        return led_list

    def _ensure_group(self, group_name, led_list):
        """Get group for given LED mask.

        Raise:
            RuntimeError: in case of connection problem.
        """
        if group_name in self._led_groups:
            rospy.logdebug("[LED] group %r already known")
            return
        leds = self.get_leds()
        rospy.logdebug("[LED] setting group %r: %r...", group_name, led_list)
        leds.createGroup(group_name, led_list)
        self._led_groups.add(group_name)

    def set_leds_color(self, req):
        """Set LEDs to desired color"""
        rospy.logdebug("[LED] set_leds_color request: %r", req)
        # default values
        success = True
        error_message = ''
        # ensure valid mask
        all_leds_mask = sum(self._set_leds_color_map.keys())
        if req.leds_mask != (all_leds_mask & req.leds_mask):
            success = False
            error_message = "Unsupported mask: %s" % (hex(req.leds_mask),)
            rospy.logwarn("[LED] %s", error_message)
            return SetLEDsColorResponse(success, error_message)
        # null mask
        if not req.leds_mask:
            rospy.loginfo("[LED] set_leds_color called with null mask.")
            return SetLEDsColorResponse(success, error_message)
        # get LEDs
        led_list = self._build_led_list(req.leds_mask,
                                        self._set_leds_color_map)
        # connect to the robot
        try:
            leds = self.get_leds()
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[LED] Failed connecting to the robot: %s",
                          error_message)
            return SetLEDsColorResponse(success, error_message)
        # get group
        group_name = 'SLC_' + hex(req.leds_mask)
        try:
            self._ensure_group(group_name, led_list)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[LED] Failed to create group %r: %s", group_name,
                          error_message)
            return SetLEDsColorResponse(success, error_message)
        # set color
        rospy.logdebug("[LED] calling fadeRGB(%r, %s, %r)...",
                       group_name, hex(req.color), req.fade_duration)
        leds.fadeRGB(group_name, req.color, req.fade_duration)
        # response
        return SetLEDsColorResponse(success, error_message)

    def set_leds_color_list(self, req):
        """Set LEDs to desired color sequence"""
        rospy.logdebug("[LED] set_leds_color_list request: %r", req)
        # default values
        success = True
        error_message = ''
        # ensure valid mask
        all_leds_mask = sum(self._set_leds_color_list_map.keys())
        if req.leds_mask != (all_leds_mask & req.leds_mask):
            success = False
            error_message = "Unsupported mask: %s" % (hex(req.leds_mask),)
            rospy.logwarn("[LED] %s", error_message)
            return SetLEDsColorListResponse(success, error_message)
        # null mask
        if not req.leds_mask:
            rospy.loginfo("[LED] set_leds_color_list called with null mask.")
            return SetLEDsColorListResponse(success, error_message)
        # get LEDs
        led_list = self._build_led_list(req.leds_mask,
                                        self._set_leds_color_list_map)
        # connect to the robot
        try:
            leds = self.get_leds()
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[LED] Failed connecting to the robot: %s",
                          error_message)
            return SetLEDsColorListResponse(success, error_message)
        # get group
        group_name = 'SLC_' + hex(req.leds_mask)
        try:
            self._ensure_group(group_name, led_list)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[LED] Failed to create group %r: %s", group_name,
                          error_message)
            return SetLEDsColorListResponse(success, error_message)
        # set color
        rospy.logdebug("[LED] calling fadeListRGB(%r, %r, %r)...",
                       group_name, [hex(c) for c in req.color_list],
                       req.time_list)
        leds.fadeListRGB(group_name, req.color_list, req.time_list)
        # response
        return SetLEDsColorListResponse(success, error_message)

    def run(self):
        """Spin"""
        rospy.spin()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_led_driver')
        ld = PepperLEDDriver()
        ld.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
