#!/usr/bin/env python2
"""
This node relays velocity commands to the Pepper robot.

It overrides the original ``/naoqi_moveto`` node since it uses a wrong
call and the velocity performed is slower than requested (see `issue
<https://gitlab.inria.fr/colas/pepper_driver_ext/-/issues/29>`_).

Subscribed topics:
    - `cmd_vel` (geometry_msgs/Twist): velocity command.

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
"""

# ROS imports
import rospy
from geometry_msgs.msg import Twist
# third party imports
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class PepperCmdVelDriver(object):
    """Main class of the cmd_vel driver."""

    def __init__(self):
        """Connect to Pepper and create services."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[CmdVel] ')
        pepper_port = 9559  # never seen any other port: no point in param
        # session
        self._session = qi.Session()
        self._url = 'tcp://%s:%d' % (pepper_ip, pepper_port)
        self._motion = None
        # ROS subscription
        self.cmd_vel_sub = rospy.Subscriber('cmd_vel', Twist, self.cmd_vel_cb,
                                            queue_size=1)

    def get_motion(self):
        """Connect to Pepper robot ALMotion module.

        Return:
            proxy to ALMotion module

        Raise:
            RuntimeError: in case of connection problem.
        """
        connected = self._session.isConnected()
        rospy.logdebug("[CmdVel] connected to robot: %r", connected)
        if connected:
            assert self._motion is not None
            return self._motion
        self._motion = None
        rospy.logdebug("[CmdVel] connecting to robot...")
        self._session.connect(self._url)
        rospy.logdebug("[CmdVel] connecting to ALMotion...")
        self._motion = self._session.service("ALMotion")
        assert self._motion is not None
        return self._motion

    def move(self, vx, vy, wz):
        """Actual sending of command.

        Raise RuntimeError: in case of connection problem.
        """
        # first try to get function as if connected
        # instead of always calling get_motion() which would add a test of the
        # connection
        try:
            rospy.logdebug("[CmdVel] getting move function...")
            move_fn = self._motion.move
        except (AttributeError, RuntimeError) as e:
            rospy.logdebug("[CmdVel] error: %r", str(e))
            move_fn = self.get_motion().move
        rospy.logdebug("[CmdVel] calling move(%f, %f, %f)...", vx, vy, wz)
        move_fn(vx, vy, wz)

    def cmd_vel_cb(self, msg):
        """Callback to cmd_vel topic."""
        rospy.logdebug("[CmdVel] cmd_vel message: %r", msg)
        try:
            self.move(msg.linear.x, msg.linear.y, msg.angular.z)
        except RuntimeError as e:
            rospy.logwarn("[CmdVel] Failed: %s", str(e))

    def run(self):
        """Spin"""
        rospy.spin()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_cmd_vel_driver')
        cvd = PepperCmdVelDriver()
        cvd.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
