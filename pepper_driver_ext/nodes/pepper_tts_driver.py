#!/usr/bin/env python2
"""
This node provides text to speech conversion on the Pepper robot.

Subscribed topics:
    - `text_to_speech` (std_msgs/String): say given text.

Services:
    - `text_to_speech` (pepper_driver_ext_msgs/TextToSpeech): say
        given text

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import TextToSpeech, TextToSpeechResponse
from std_msgs.msg import String
# third party imports
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class PepperTextToSpeechDriver(object):
    """Main class of the text-to-speech driver."""

    def __init__(self):
        """Connect to Pepper and create services."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[TTS] ')
        pepper_port = 9559  # never seen any other port: no point in param
        # session
        self._session = qi.Session()
        self._url = "tcp://%s:%d" % (pepper_ip, pepper_port)
        self._tts = None
        try:
            self.get_tts()
        except RuntimeError as e:
            rospy.logwarn("[TTS] could not connect to robot: %r", str(e))
        # ROS services
        self.tts_srv = rospy.Service('text_to_speech', TextToSpeech,
                                     self.tts_srv)
        # ROS subscription
        self.tts_sub = rospy.Subscriber('text_to_speech', String,
                                        self.tts_sub, queue_size=1)

    def get_tts(self):
        """Connect to Pepper robot ALTextToSpeech module.

        Return:
            proxy to ALTextToSpeech module

        Raise:
            RuntimeError: in case of connection problem.
        """
        connected = self._session.isConnected()
        rospy.logdebug("[TTS] connected to robot: %r", connected)
        if connected:
            return self._tts
        rospy.logdebug("[TTS] connecting to robot...")
        self._session.connect(self._url)
        rospy.logdebug("[TTS] connecting to ALTextToSpeech...")
        self._tts = self._session.service("ALTextToSpeech")
        return self._tts

    def _say(self, text):
        """Low-level text-to-speech function.

        Raise:
            RuntimeError: in case of connection or speech
                problem.
        """
        tts = self.get_tts()
        rospy.logdebug("[TTS] saying: %r", text)
        tts.say(text)
        rospy.loginfo("[TTS] Said: %r", text)

    def tts_sub(self, msg):
        """Callback for text_to_speech topic."""
        rospy.logdebug("[TTS] text_to_speech message: %r", msg)
        try:
            self._say(msg.data)
        except RuntimeError as e:
            rospy.logwarn("[TTS] Failed: %s", str(e))

    def tts_srv(self, req):
        """Text-to-speech service handler."""
        rospy.logdebug("[TTS] text_to_speech message: %r", req)
        # default
        success = True
        error_message = ''
        try:
            self._say(req.text)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[TTS] Failed: %s", str(e))
        response = TextToSpeechResponse(success, error_message)
        rospy.logdebug("[TTS] text_to_speech response: %r", response)
        return response

    def run(self):
        """Spin"""
        rospy.spin()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_tts_driver')
        ttsd = PepperTextToSpeechDriver()
        ttsd.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
