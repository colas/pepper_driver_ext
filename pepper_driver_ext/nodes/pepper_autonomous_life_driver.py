#!/usr/bin/env python2
"""
This node handles the level of autonomous life of the Pepper robot.

Four levels are defined:
    - rest: no autonomous life nor motion (joint stiffness is 0)
    - wake_up: no autonomous life but stiff
    - no_autonomous_life: some autonomous motion but don't try to
        interact
    - full_autonomous_life: full autonomous life as when started up

Services:
    - `set_autonomous_life_level`
        (pepper_driver_ext_msgs/SetAutonomousLifeLevel): set
        autonomous life level.
    - `get_autonomous_life_level`
        (pepper_driver_ext_msgs/GetAutonomousLifeLevel): get
        autonomous life level.

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
"""

# standard imports
import time
# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import (GetAutonomousLifeLevel,
                                        GetAutonomousLifeLevelResponse,
                                        SetAutonomousLifeLevel,
                                        SetAutonomousLifeLevelRequest,
                                        SetAutonomousLifeLevelResponse)
# third party imports
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class PepperAutonomousLifeDriver(object):
    """Main class of the autonomous life driver."""

    def __init__(self):
        """Connect to Pepper and create services."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[AutonomousLife] ')
        pepper_port = 9559  # never seen any other port: no point in param
        # session
        self.session = qi.Session()
        self.url = "tcp://%s:%d" % (pepper_ip, pepper_port)
        try:
            self._connect()
        except RuntimeError as e:
            rospy.logwarn("[AutonomousLife] Could not connect to robot: %r",
                          str(e))
        # ROS services
        self.set_autonomous_life_level_srv = rospy.Service(
            'set_autonomous_life_level', SetAutonomousLifeLevel, self.set_all)
        self.get_autonomous_life_level_srv = rospy.Service(
            'get_autonomous_life_level', GetAutonomousLifeLevel, self.get_all)

    def _connect(self):
        """Connect to Pepper robot.

        Raise:
            RuntimeError: in case of connection problem.
        """
        connected = self.session.isConnected()
        rospy.logdebug("[AutonomousLife] connected to robot: %r", connected)
        if connected:
            return
        self.session.connect(self.url)

    def set_all(self, req):
        """Set autonomous life level."""
        rospy.logdebug(
            "[AutonomousLife] set_autonomous_life_level request: %r",
            req)
        level = req.level
        # default
        success = True
        error_message = ''
        try:
            # connect if needed
            self._connect()
            al = self.session.service("ALAutonomousLife")
            if level == SetAutonomousLifeLevelRequest.REST:
                al.setState('disabled')
            elif level == SetAutonomousLifeLevelRequest.WAKE_UP:
                motion = self.session.service("ALMotion")
                if al.getState() != 'disabled':
                    al.setState('disabled')
                motion.wakeUp()
            elif level == SetAutonomousLifeLevelRequest.NO_AUTONOMOUS_LIFE:
                al.setState('solitary')
                al.setAutonomousAbilityEnabled("BasicAwareness", False)
                al.setSafeguardEnabled('RobotMoved', False)
                dialog = self.session.service("ALDialog")
                time.sleep(0.3)
                for topic in dialog.getLoadedTopics(dialog.getLanguage()):
                    dialog.unloadTopic(topic)
            elif level == SetAutonomousLifeLevelRequest.FULL_AUTONOMOUS_LIFE:
                if al.getState() not in ('solitary', 'interactive'):
                    al.setState('solitary')
                if not al.getAutonomousAbilityEnabled("BasicAwareness"):
                    al.setAutonomousAbilityEnabled("BasicAwareness", True)
                if not al.isSafeguardEnabled('RobotMoved'):
                    al.setSafeguardEnabled('RobotMoved', True)
            else:
                raise RuntimeError("Unknown level: %d." % level)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[AutonomousLife] Failed setting autonomous life "
                          "level to %d: %r", level, error_message)
        else:
            rospy.loginfo("[AutonomousLife] Success in setting autonomy level "
                          "to %d.", level)
        response = SetAutonomousLifeLevelResponse(success, error_message)
        rospy.logdebug(
            "[AutonomousLife] set_autonomous_life_level response: %r",
            response)
        return response

    def get_all(self, req):
        """Get autonomous life level."""
        rospy.logdebug(
            "[AutonomousLife] get_autonomous_life_level request: %r",
            req)
        # default
        success = True
        error_message = ''
        level = GetAutonomousLifeLevelResponse.UNKNOWN
        try:
            # connect if needed
            self._connect()
            al = self.session.service("ALAutonomousLife")
            al_state = al.getState()
            if al_state == 'disabled':
                # REST or WAKE_UP
                motion = self.session.service("ALMotion")
                wake_up = motion.robotIsWakeUp()
                if wake_up:
                    level = GetAutonomousLifeLevelResponse.WAKE_UP
                else:
                    level = GetAutonomousLifeLevelResponse.REST
            else:
                # FULL or NO?
                basic_on = al.getAutonomousAbilityEnabled("BasicAwareness")
                if basic_on:
                    level = GetAutonomousLifeLevelResponse.FULL_AUTONOMOUS_LIFE
                else:
                    level = GetAutonomousLifeLevelResponse.NO_AUTONOMOUS_LIFE
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[AutonomousLife] Failed getting autonomous life "
                          "level: %r", error_message)
        else:
            rospy.loginfo("[AutonomousLife] Success in getting autonomous "
                          "life level: %d.", level)
        response = GetAutonomousLifeLevelResponse(success, error_message,
                                                  level)
        rospy.logdebug(
            "[AutonomousLife] get_autonomous_life_level response: %r",
            response)
        return response

    def run(self):
        """Spin"""
        rospy.spin()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_autonomous_life_driver')
        ald = PepperAutonomousLifeDriver()
        ald.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
