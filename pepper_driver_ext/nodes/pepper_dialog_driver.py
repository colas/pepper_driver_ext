#!/usr/bin/env python2
"""
This node handles the Pepper dialog through a collection of services.

In particular, it can load, unload, active, or deactivate topics.
It can also list topics.

Services:
    - `load_dialog_topic` (pepper_driver_ext_msgs/LoadDialogTopic):
        load topic from its content and return its name.
    - `manage_dialog_topic` (pepper_driver_ext_msgs/ManageDialogTopic):
        manage topic (unload, activate, deactivate).
    - `list_dialog_topics` (pepper_driver_ext_msgs/ListDialogTopics):
        list active, loaded, or all topics.
    - `get_language` (pepper_driver_ext_msgs/GetLanguage): get
        language.
    - `set_language` (pepper_driver_ext_msgs/SetLanguage): set
        language.

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
    - `default_language`: default language of the dialog (default:
        "French").
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import (
    LoadDialogTopic, LoadDialogTopicResponse, ManageDialogTopic,
    ManageDialogTopicResponse, ManageDialogTopicRequest,
    ListDialogTopicsRequest, ListDialogTopics, ListDialogTopicsResponse,
    GetLanguage, GetLanguageResponse, SetLanguage, SetLanguageResponse)
# third party imports
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class PepperDialogDriver(object):
    """Main class of the dialog driver."""

    def __init__(self):
        """Connect to Pepper and create services."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[Dialog] ')
        pepper_port = 9559  # never seen any other port: no point in param
        default_language = get_ros_param('~default_language', 'French',
                                         '[Dialog] ')
        self.subscriber_name = 'ros_dialog_driver'
        # internal states
        self._subscribed = False
        self._activated_topics = set()
        self._current_language = None
        # session
        self._session = qi.Session()
        self._url = 'tcp://%s:%d' % (pepper_ip, pepper_port)
        self._dialog = None
        try:
            dialog = self.get_dialog()
        except RuntimeError as e:
            rospy.logwarn("[Dialog] Could not connect to robot: %s.", str(e))
        else:
            rospy.logdebug("[Dialog] Setting default language to %r...",
                           default_language)
            try:
                dialog.setLanguage(default_language)
                self._current_language = default_language
            except RuntimeError as e:
                rospy.logwarn("[Dialog] Failed setting default language to "
                              "%r: %s", default_language, str(e))
            else:
                rospy.loginfo("[Dialog] Success setting default language to "
                              "%s.", default_language)
        # ROS services
        self.load_topic_srv = rospy.Service(
            'load_dialog_topic', LoadDialogTopic, self.load_topic)
        self.manage_topic_srv = rospy.Service(
            'manage_dialog_topic', ManageDialogTopic, self.manage_topic)
        self.list_topics_srv = rospy.Service(
            'list_dialog_topics', ListDialogTopics, self.list_topics)
        self.get_language_srv = rospy.Service(
            'get_language', GetLanguage, self.get_language)
        self.set_language_srv = rospy.Service(
            'set_language', SetLanguage, self.set_language)

    def get_dialog(self):
        """Connect to the ALDialog module from the Pepper.

        Return:
            proxy to ALDialog module

        Raise:
            RuntimeError: in case of connection problem.
        """
        connected = self._session.isConnected()
        rospy.logdebug("[Dialog] connected to robot: %r", connected)
        if connected:
            return self._dialog
        rospy.logdebug("[Dialog] connecting to robot...")
        self._session.connect(self._url)
        rospy.logdebug("[Dialog] connecting to ALDialog...")
        self._dialog = self._session.service('ALDialog')
        return self._dialog

    def _activate_topic(self, topic_name):
        """Activate topic and handle subscription.

        Automatically subscribe to the dialog engine if needed.

        Raise:
            RuntimeError: in case of connection problem.
        """
        # activate
        rospy.logdebug("[Dialog] activating topic '%s'...", topic_name)
        self.get_dialog().activateTopic(topic_name)
        self._activated_topics.add(topic_name)
        # update subscription
        if not self._subscribed:
            rospy.logdebug("[Dialog] subscribing to dialog engine...")
            self.get_dialog().subscribe(self.subscriber_name)
            self._subscribed = True

    def _deactivate_topic(self, topic_name):
        """Deactivate topic and handle subscription.

        Automatically unsubscribe from the dialog engine if needed.

        Raise:
            RuntimeError: in case of connection problem.
        """
        # deactivate
        rospy.logdebug("[Dialog] deactivating topic '%s'...",  topic_name)
        self.get_dialog().deactivateTopic(topic_name)
        self._activated_topics.discard(topic_name)
        # update subscription
        if not self._activated_topics and self._subscribed:
            rospy.logdebug("[Dialog] unsubscribing to dialog engine...")
            self.get_dialog().unsubscribe(self.subscriber_name)
            self._subscribed = False

    def _unload_topic(self, topic_name):
        """Unload topic and handle subscription.

        Automatically unsubscribe from the dialog engine if needed.

        Raise:
            RuntimeError: in case of connection problem
        """
        # unload
        rospy.logdebug("[Dialog] unloading topic '%s'...",  topic_name)
        self.get_dialog().unloadTopic(topic_name)
        self._activated_topics.discard(topic_name)
        # update subscription
        if not self._activated_topics and self._subscribed:
            rospy.logdebug("[Dialog] unsubscribing to dialog engine...")
            self.get_dialog().unsubscribe(self.subscriber_name)
            self._subscribed = False

    def load_topic(self, req):
        """Load topic from its content."""
        rospy.logdebug("[Dialog] load_topic request: %r", req)
        # default values
        success = True
        name = ''
        error_message = ''
        # load
        try:
            name = self.get_dialog().loadTopicContent(req.topic_content)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Dialog] Failed loading topic: %s", error_message)
        else:
            rospy.loginfo("[Dialog] Success loading topic %r.", name)
        # activate
        if success and req.activate:
            try:
                self._activate_topic(name)
            except RuntimeError as e:
                success = False
                error_message = str(e)
            else:
                rospy.loginfo("[Dialog] Success activating topic %r.", name)
        # response
        response = LoadDialogTopicResponse(success, name, error_message)
        rospy.logdebug("[Dialog] load_topic response: %r", response)
        return response

    def manage_topic(self, req):
        """Manage topic life cycle."""
        rospy.logdebug("[Dialog] manage_topic request: %r", req)
        # default
        success = True
        error_message = ''
        try:
            if req.command == ManageDialogTopicRequest.ACTIVATE_TOPIC:
                action = 'activating'
                self._activate_topic(req.topic_name)
            elif req.command == ManageDialogTopicRequest.DEACTIVATE_TOPIC:
                action = 'deactivating'
                self._deactivate_topic(req.topic_name)
            elif req.command == ManageDialogTopicRequest.UNLOAD_TOPIC:
                action = 'unloading'
                self._unload_topic(req.topic_name)
            else:
                action = 'managing'
                raise RuntimeError("Unknown command %d" % req.command)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Dialog] Failed %s topic: '%s': %s", action,
                          req.topic_name, error_message)
        else:
            rospy.loginfo("[Dialog] Success %s topic %r.", action,
                          req.topic_name)
        response = ManageDialogTopicResponse(success, error_message)
        rospy.logdebug("[Dialog] manage_topic response: %r", response)
        return response

    def list_topics(self, req):
        """List topics."""
        rospy.logdebug("[Dialog] list_topics request: %r", req)
        # default
        success = True
        error_message = ''
        topic_names = []
        try:
            if req.topics == ListDialogTopicsRequest.ACTIVATED_TOPICS:
                kind = 'activated'
                topic_names = self.get_dialog().getActivatedTopics()
            elif req.topics == ListDialogTopicsRequest.LOADED_TOPICS:
                kind = 'loaded'
                if self._current_language is not None:
                    lang = self._current_language
                else:
                    rospy.logwarn("[Dialog] Current language not known: using "
                                  "default: %s.", self.default_language)
                    lang = self.default_language
                topic_names = self.get_dialog().getLoadedTopics(lang)
            elif req.topics == ListDialogTopicsRequest.ALL_LOADED_TOPICS:
                kind = 'all loaded'
                topic_names = self.get_dialog().getAllLoadedTopics()
            else:
                raise RuntimeError("unknown list %d" % req.topics)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Dialog] Failed listing topics: %s", str(e))
        else:
            rospy.loginfo("[Dialog] Listed %s topics: %r.", kind, topic_names)
        response = ListDialogTopicsResponse(success, error_message,
                                            topic_names)
        rospy.logdebug("[Dialog] list_topics response: %r", response)
        return response

    def get_language(self, req):
        """Language query."""
        rospy.logdebug("[Dialog] get_language request.")
        # default
        success = True
        error_message = ''
        language = ''
        try:
            language = self.get_dialog().getLanguage()
            self._current_language = language
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Dialog] Failed getting language: %s", str(e))
        else:
            rospy.loginfo("[Dialog] Got language: %s.", language)
        response = GetLanguageResponse(success, error_message, language)
        rospy.logdebug("[Dialog] get_language response: %r", response)
        return response

    def set_language(self, req):
        """Set language."""
        rospy.logdebug("[Dialog] set_language request: %r", req)
        # default
        success = True
        error_message = ''
        try:
            self.get_dialog().setLanguage(req.language)
            self._current_language = req.language
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Dialog] Failed setting language to %s: %s",
                          req.language, error_message)
        else:
            rospy.loginfo("[Dialog] Success setting language %s.",
                          req.language)
        response = SetLanguageResponse(success, error_message)
        rospy.logdebug("[Dialog] set_language response: %r", response)
        return response

    def run(self):
        """Spin"""
        rospy.spin()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_dialog_driver')
        dd = PepperDialogDriver()
        dd.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
