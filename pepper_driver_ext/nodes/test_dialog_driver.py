#!/usr/bin/env python2
"""
Node to quickly test the dialog drivers.
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.srv import (
    LoadDialogTopic, ManageDialogTopic, ListDialogTopics, GetLanguage,
    SetLanguage, ListDialogTopicsRequest, ManageDialogTopicRequest)


class TestDialog(object):
    def __init__(self):
        # get service proxies
        rospy.loginfo("[TestDialog] wait for 'load_dialog_topic' service...")
        rospy.wait_for_service('load_dialog_topic')
        self.load_topic = rospy.ServiceProxy('load_dialog_topic', LoadDialogTopic)
        rospy.loginfo("[TestDialog] wait for 'manage_dialog_topic' service...")
        rospy.wait_for_service('manage_dialog_topic')
        self.manage_topic = rospy.ServiceProxy('manage_dialog_topic', ManageDialogTopic)
        rospy.loginfo("[TestDialog] wait for 'list_dialog_topics' service...")
        rospy.wait_for_service('list_dialog_topics')
        self.list_topics = rospy.ServiceProxy('list_dialog_topics', ListDialogTopics)
        rospy.loginfo("[TestDialog] wait for 'get_language' service...")
        rospy.wait_for_service('get_language')
        self.get_language = rospy.ServiceProxy('get_language', GetLanguage)
        rospy.loginfo("[TestDialog] wait for 'set_language' service...")
        rospy.wait_for_service('set_language')
        self.set_language = rospy.ServiceProxy('set_language', SetLanguage)

    def test_load_topic(self, test_content, activate):
        rospy.loginfo("[TestDialog] try to load topic...")
        resp = self.load_topic(test_content, activate)
        rospy.loginfo("[TestDialog] response: success=%i, name='%s', "
                      "error_message='%s'", resp.success, resp.topic_name,
                      resp.error_message)

    def test_manage_topic(self, command, topic_name):
        rospy.loginfo("[TestDialog] try to manage [%d] topic '%s'...",
                      command, topic_name)
        resp = self.manage_topic(command, topic_name)
        rospy.loginfo("[TestDialog] response: success=%i, error_message='%s'",
                      resp.success, resp.error_message)

    def test_list_topics(self):
        rospy.loginfo("[TestDialog] try to list topics...")
        all_loaded_topics = self.list_topics(ListDialogTopicsRequest.ALL_LOADED_TOPICS)
        rospy.loginfo("[TestDialog] all loaded topics: %r", all_loaded_topics)
        loaded_topics = self.list_topics(ListDialogTopicsRequest.LOADED_TOPICS)
        rospy.loginfo("[TestDialog] loaded topics: %r", loaded_topics)
        activated_topics = self.list_topics(ListDialogTopicsRequest.ACTIVATED_TOPICS)
        rospy.loginfo("[TestDialog] activated topics: %r", activated_topics)

    def test_get_language(self):
        rospy.loginfo("[TestDialog] try to get language...")
        resp = self.get_language()
        rospy.loginfo("[TestDialog] response: language='%s'", resp.language)

    def test_set_language(self, lang):
        rospy.loginfo("[TestDialog] try to set language to '%s'...", lang)
        resp = self.set_language(lang)
        rospy.loginfo("[TestDialog] response: success=%i, error_message='%s'",
                      resp.success, resp.error_message)

    def run(self):
        test_content_ok = '''
            topic: ~bonjour()
            language: frf
            u: (Salut Pepper) Salut Francis.
            u: (Bonjour Pepper) Bonjour Vincent.
            '''
        test_content_not_ok = '''
            toddpic: ~bonjoru()
            '''
        # load topics
        rospy.logwarn("[TestDialog] Next should work")
        self.test_load_topic(test_content_ok, True)  # should work
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should fail (already loaded)")
        self.test_load_topic(test_content_ok, True)  # fails? already loaded
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should fail (bad syntax)")
        self.test_load_topic(test_content_not_ok, True)  # should fail
        rospy.sleep(1)
        # manage topics
        rospy.logwarn("[TestDialog] Next should work")
        self.test_manage_topic(ManageDialogTopicRequest.DEACTIVATE_TOPIC, 'bonjour')  # OK
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should work")
        self.test_manage_topic(ManageDialogTopicRequest.ACTIVATE_TOPIC, 'bonjour')  # OK
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should fail (already activated)")
        self.test_manage_topic(ManageDialogTopicRequest.ACTIVATE_TOPIC, 'bonjour')  # KO
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should fail (unknown topic)")
        self.test_manage_topic(ManageDialogTopicRequest.ACTIVATE_TOPIC, 'bonjoru')  # KO
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should work")
        self.test_manage_topic(ManageDialogTopicRequest.UNLOAD_TOPIC, 'bonjour')  # OK
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should fail (unknown topic)")
        self.test_manage_topic(ManageDialogTopicRequest.UNLOAD_TOPIC, 'bonjour')  # KO
        rospy.sleep(1)
        # get/set language
        rospy.logwarn("[TestDialog] Next should show 'French'")
        self.test_get_language()
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should work")
        self.test_set_language('English')  # OK
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should show 'English'")
        self.test_get_language()
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should fail (unknown language)")
        self.test_set_language('NotExistingToTestFailure')  # KO
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should show 'English'")
        self.test_get_language()
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should work")
        self.test_set_language('French')  # OK
        rospy.sleep(1)
        rospy.logwarn("[TestDialog] Next should show 'French'")
        self.test_get_language()
        rospy.sleep(1)
        # list topics
        rospy.logwarn("[TestDialog] Next three should be empty")
        self.test_list_topics()
        rospy.sleep(1)
        self.test_load_topic(test_content_ok, True)
        rospy.logwarn("[TestDialog] Next three should show: ['bonjour']")
        self.test_list_topics()
        rospy.sleep(1)
        self.test_manage_topic(ManageDialogTopicRequest.DEACTIVATE_TOPIC, 'bonjour')
        rospy.logwarn("[TestDialog] 'bonjour' should not be activated")
        self.test_list_topics()
        rospy.sleep(1)
        self.test_manage_topic(ManageDialogTopicRequest.ACTIVATE_TOPIC, 'bonjour')
        rospy.logwarn("[TestDialog] Next three should show: ['bonjour']")
        self.test_list_topics()
        rospy.sleep(1)
        self.test_set_language('English')
        rospy.logwarn("[TestDialog] 'bonjour' should only be in all_loaded")
        self.test_list_topics()
        rospy.sleep(1)
        self.test_set_language('French')
        rospy.logwarn("[TestDialog] Next three should show: ['bonjour']")
        self.test_list_topics()
        rospy.sleep(1)
        self.test_manage_topic(ManageDialogTopicRequest.UNLOAD_TOPIC, 'bonjour')  # OK
        rospy.logwarn("[TestDialog] Next three should be empty")
        self.test_list_topics()
        rospy.sleep(1)


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('test_dialog')
        td = TestDialog()
        td.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
