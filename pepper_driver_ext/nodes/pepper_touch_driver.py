#!/usr/bin/env python2
"""
This node provides access to the touch events on the robot.

It only handles touch events on the body parts, as opposed to events
on the tablet (which are handled by `pepper_tablet_driver.py`).

It only subscribes to touch events when there are subscribers to the
node.

Publications:
    - `/robot_touch_events` (pepper_driver_ext_msgs/RobotTouchEvents):
        detected events.

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
"""

# ROS imports
import rospy
from pepper_driver_ext_msgs.msg import RobotTouchEvent, RobotTouchEvents
# third party imports
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class PepperTouchDriver(object):
    """Main class of the touch driver."""

    def __init__(self):
        """Connect to Pepper and create publisher."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[Touch] ')
        pepper_port = 9559  # never seen any other port: no point in param
        self.check_frequency = 5  # ROS topic subscription check frequency (Hz)
        # session and connection
        self._session = qi.Session()
        self._url = "tcp://%s:%d" % (pepper_ip, pepper_port)
        self._mem = None  # proxy to ALMemory
        self._touch_sub = None  # ALMemory subscription to TouchChanged
        self._conn = None  # TouchChanged signal connection
        self.subscribed = False  # node ready or not to publish events
        # ROS publisher
        self.touch_pub = rospy.Publisher(
            'robot_touch_events', RobotTouchEvents, queue_size=5, latch=True)

    def subscribe(self):
        """Subscribe to the TouchChanged event."""
        assert self.subscribed is False
        connected = self._session.isConnected()
        try:
            if not connected:
                rospy.logdebug("[Touch] connecting to robot...")
                self._session.connect(self._url)
                rospy.logdebug("[Touch] connecting to ALMemory...")
                self._mem = self._session.service("ALMemory")
            # subscribe to touch event
            rospy.logdebug("[Touch] subscribing to TouchChanged event...")
            self._touch_sub = self._mem.subscriber("TouchChanged")
            self._conn = self._touch_sub.signal.connect(self._touch_callback)
        except RuntimeError as e:
            rospy.logwarn("[Touch] Could not subscribe to TouchChanged "
                          "event on the robot: %s", str(e))
        else:
            rospy.loginfo("[Touch] Success in subscribing to TouchChanged "
                          "event.")
            self.subscribed = True

    def unsubscribe(self):
        """Unsubscribe from the TouchChanged event."""
        assert self.subscribed
        # connected = self._session.isConnected()
        try:
            # if not connected:
            #     rospy.logdebug("[Touch] reconnecting to the robot...")
            #     self._session.connect(self._url)
            # disconnect from signal
            rospy.logdebug("[Touch] disconnecting from TouchChanged event...")
            self._touch_sub.signal.disconnect(self._conn)
        except RuntimeError as e:
            rospy.logwanr("[Touch] Couldn't unsubscribe: %s", str(e))
        else:
            rospy.loginfo("[Touch] Success in unsubscribing from TouchChanged "
                          "event.")
        finally:
            self._conn = self._touch_sub = None
            self.subscribed = False

    def _touch_callback(self, value):
        """Callback for the TouchChanged event"""
        rospy.logdebug("[Touch] received TouchChanged event %r", value)
        if not value:
            rospy.logwarn("[Touch] Received empty list: ignoring.")
            return
        # create message
        rte_msg = RobotTouchEvents()
        # fill message
        rte_msg.header.stamp = rospy.Time.now()
        rte_msg.robot_touch_events = [
            RobotTouchEvent(data[0], data[1])
            for data in value
        ]
        # send message
        rospy.logdebug("[Touch] touch events messages: %r", rte_msg)
        self.touch_pub.publish(rte_msg)

    def run(self):
        """Check ROS connection status to subscribe"""
        r = rospy.Rate(self.check_frequency)
        while not rospy.is_shutdown():
            r.sleep()
            # subscribe or unsubscribe from touch events on the robot depending
            # on the presence of ROS subscribers to the topic
            if self.touch_pub.get_num_connections():
                if not self.subscribed:
                    rospy.logdebug("[Touch] connecting to touch events...")
                    self.subscribe()
            else:
                if self.subscribed:
                    rospy.logdebug("[Touch] disconnecting from touch "
                                   "events...")
                    self.unsubscribe()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_touch_driver')
        td = PepperTouchDriver()
        td.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
