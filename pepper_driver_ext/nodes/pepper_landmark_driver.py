#!/usr/bin/env python2
"""
This node provides access to Pepper landmark detection.

It only triggers the landmark detection module of the Pepper when
there are subscribers to the node.

Publications:
    - `/landmark_detection` (pepper_driver_ext_msgs/NaomarkDetection):
        detected landmarks.
    - `/landmarks` (visualization_msgs/Marker): landmarks as markers.

Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
    - `marker_diameter`: diameter of the printed markers in m
        (default: 0.12 m).
"""

# standard imports
from math import hypot, sin, tan
# ROS imports
import rospy
from geometry_msgs.msg import Point, Vector3
from pepper_driver_ext_msgs.msg import Naomark, NaomarkDetection
from std_msgs.msg import ColorRGBA
from visualization_msgs.msg import Marker
# third party imports
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class PepperLandmarkDriver(object):
    """Main class of the landmark driver."""

    def __init__(self):
        """Connect to Pepper and create publisher."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[Landmark] ')
        pepper_port = 9559  # never seen any other port: no point in param
        self.marker_diameter = get_ros_param('~marker_diameter', .12,
                                             '[Landmark] ')
        self.marker_thickness = 0.001  # only for Marker visualization
        self.marker_color = ColorRGBA(.114, .475, .74, 1)
        self.marker_namespace = 'naomark'
        self.check_frequency = 5  # ROS topic subscription check frequency (Hz)
        self.detect_period = 500  # detect period in ms
        self.subscriber_name = 'ros_landmark_driver'
        # session and connection
        self._session = qi.Session()
        self._url = "tcp://%s:%d" % (pepper_ip, pepper_port)
        self._lm_detect = None  # proxy to ALLandMarkDetection
        self._mem = None  # proxy to ALMemory
        self._lm_sub = None  # ALMemory subscription
        self._conn = None  # LandmarkDetected signal connection
        self.subscribed = False  # node ready or not to publish detections
        # ROS publishers
        self.landmark_pub = rospy.Publisher(
            'landmark_detection', NaomarkDetection, queue_size=5, latch=True)
        self.landmark_vis_pub = rospy.Publisher(
            'landmarks', Marker, queue_size=1, latch=True)

    def subscribe(self):
        """Subscribe to the LandmarkDetector event."""
        assert self.subscribed is False
        connected = self._session.isConnected()
        try:
            if not connected:
                rospy.logdebug("[Landmark] connecting to robot...")
                self._session.connect(self._url)
                rospy.logdebug("[Landmark] connecting to ALMemory...")
                self._mem = self._session.service("ALMemory")
                rospy.logdebug("[Landmark] connecting to "
                               "ALLandMarkDetection...")
                self._lm_detect = self._session.service("ALLandMarkDetection")
            # subscribe to detection event
            rospy.logdebug("[Landmark] subscribing to LandmarkDetected "
                           "event...")
            self._lm_sub = self._mem.subscriber("LandmarkDetected")
            self._conn = self._lm_sub.signal.connect(self._lm_callback)
            # activate detection on the robot
            rospy.logdebug("[Landmark] subscribing to detection engine...")
            self._lm_detect.subscribe(self.subscriber_name, self.detect_period,
                                      0.0)
        except RuntimeError as e:
            rospy.logwarn("[Landmark] Could not connect to the landmark "
                          "detector: %s", str(e))
        else:
            rospy.loginfo("[Landmark] Success in subscribing to landmark "
                          "detection engine.")
            self.subscribed = True

    def unsubscribe(self):
        """Unsubscribe from the LandmarkDetector."""
        assert self.subscribed
        connected = self._session.isConnected()
        try:
            if not connected:
                rospy.logdebug("[Landmark] reconnecting to the robot...")
                self._session.connect(self._url)  # protect
                rospy.logdebug("[Landmark] reconnecting to "
                               "ALLandMarkDetection...")
                self._lm_detect = self._session.service("ALLandMarkDetection")
            # disconnect from signal
            rospy.logdebug("[Landmark] disconnecting from LandmarkDetected "
                           "event...")
            self._lm_sub.signal.disconnect(self._conn)
            # deactivate detection on the robot
            rospy.logdebug("[Landmark] unsubscribing from detection engine...")
            self._lm_detect.unsubscribe(self.subscriber_name)
        except RuntimeError as e:
            rospy.logwarn("[Landmark] Couldn't fully unsubscribe: %s", str(e))
        else:
            rospy.loginfo("[Landmark] Success in unsubscribing from landmark "
                          "detection engine.")
        finally:
            self._conn = self._lm_sub = None
            self.subscribed = False

    def _lm_callback(self, value):
        """Callback for the landmark detector."""
        rospy.logdebug("[Landmark] received detection value %r", value)
        # create messages
        det_msg = NaomarkDetection()
        vis_msg = Marker()
        if not value:
            rospy.logwarn("[Landmark] Received empty tuple: ignoring.")
            return
        # unpack value
        _, mark_info, _, _, camera_name = value
        # fill header
        det_msg.header.stamp = rospy.Time.now()
        det_msg.header.frame_id = "%s_optical_frame" % camera_name
        vis_msg.header = det_msg.header
        # global info
        vis_msg.ns = self.marker_namespace
        vis_msg.type = Marker.SPHERE_LIST
        vis_msg.action = Marker.ADD
        vis_msg.pose.orientation.w = 1.  # proper quaternion
        vis_msg.scale = Vector3(self.marker_diameter, self.marker_diameter,
                                self.marker_thickness)
        vis_msg.color = self.marker_color
        # fill markers
        for marker_detection in mark_info:
            # empty marker message
            naomark = Naomark()
            # unpack detection
            (_, alpha, beta, size_x, size_y, _), (mark_id,) = marker_detection
            # compute position
            z = self.marker_diameter / (2 * tan(hypot(size_x, size_y)/2))
            x = z * sin(alpha)
            y = z * sin(beta)
            p = Point(x, y, z)
            # fill messages
            naomark.marker_id = mark_id
            naomark.position = p
            det_msg.markers.append(naomark)
            vis_msg.points.append(p)
        # publication
        rospy.logdebug("[Landmark] detection message: %r", det_msg)
        self.landmark_pub.publish(det_msg)
        self.landmark_vis_pub.publish(vis_msg)

    def run(self):
        """Check ROS connection status to subscribe"""
        r = rospy.Rate(self.check_frequency)
        while not rospy.is_shutdown():
            r.sleep()
            # subscribe or unsubscribe from landmark detector on the robot
            # depending on the presence of ROS subscriber to one of our topics
            if (self.landmark_pub.get_num_connections()
                    or self.landmark_vis_pub.get_num_connections()):
                if not self.subscribed:
                    rospy.logdebug("[Landmark] connecting to landmark "
                                   "detector...")
                    self.subscribe()
            else:
                if self.subscribed:
                    rospy.logdebug("[Landmark] disconnecting from landmark "
                                   "detector...")
                    self.unsubscribe()


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_landmark_driver')
        lmd = PepperLandmarkDriver()
        lmd.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
