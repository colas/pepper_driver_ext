#!/usr/bin/env python2
"""
This node provides access to Pepper memory.

It can read and write in Pepper memory using the get/set_memory_*
services (according to type).

It can also create a topic to publish values set into Pepper memory
using the manage_data_stream service (to start or stop publication).
In that case, it also publishes the (key, value) pair on the event
topic of the corresponding type.

Note that it works only for events and not for all memory values.
Conveniently, it works for the values stored by the dialog engine as
well as the values set using the present node.


Services:
    - `manage_data_stream` (pepper_driver_ext_msgs/ManageDataStream):
        start or stop a ROS topic to publish data events.
    - `get_memory_int` (pepper_driver_ext_msgs/GetMemoryInt): get an
        integer value from Pepper memory.
    - `set_memory_int` (pepper_driver_ext_msgs/SetMemoryInt): set an
        integer value from Pepper memory.
    - `get_memory_float` (pepper_driver_ext_msgs/GetMemoryFloat): get
        a float value from Pepper memory.
    - `set_memory_float` (pepper_driver_ext_msgs/SetMemoryFloat): set
        a float value from Pepper memory.
    - `get_memory_string` (pepper_driver_ext_msgs/GetMemoryString):
        get a string value from Pepper memory.
    - `set_memory_string` (pepper_driver_ext_msgs/SetMemoryString):
        set a string value from Pepper memory.
    - `get_memory_bool` (pepper_driver_ext_msgs/GetMemoryBool): get a
        Boolean value from Pepper memory.
    - `set_memory_bool` (pepper_driver_ext_msgs/SetMemoryBool): set a
        Boolean value from Pepper memory.
    - `del_memory` (pepper_driver_ext_msgs/DelMemory): removes key
        from Pepper memory.

Publications:
    - `memory_event_int` (pepper_driver_ext_msgs/MemoryEventInt):
        key value pair for new value.
    - `memory_event_float` (pepper_driver_ext_msgs/MemoryEventFloat):
        key value pair for new value.
    - `memory_event_string` (pepper_driver_ext_msgs/MemoryEventString):
        key value pair for new value.
    - `memory_event_bool` (pepper_driver_ext_msgs/MemoryEventBool):
        key value pair for new value.


Parameters:
    - `/pepper_ip`: IP of the Pepper robot to connect to.
"""

# standard imports
from functools import partial
# ROS imports
import rospy
from pepper_driver_ext_msgs.msg import (MemoryEventBool, MemoryEventFloat,
                                        MemoryEventInt, MemoryEventString)
from pepper_driver_ext_msgs.srv import (GetMemoryInt, SetMemoryInt,
                                        GetMemoryFloat, SetMemoryFloat,
                                        GetMemoryString, SetMemoryString,
                                        GetMemoryBool, SetMemoryBool,
                                        DelMemory, ManageDataStream,
                                        ManageDataStreamRequest)
from std_msgs.msg import Int32, Float32, String, Bool
# third party imports
import qi
# local imports
from pepper_driver_ext.utils import get_ros_param


class MemorySubscription(object):
    """A memory subscription.

    It includes the subscription signal on the Pepper side,
    as well as the callback and ROS publisher.
    When deleted, it will automatically unsubscribe and
    close the topic.
    """

    def __init__(self, parent, key, dtype, topic_name):
        """Subscribe and publish ROS topic.

        Raise:
            RuntimeError: in case of connection error.
        """
        rospy.logdebug("[Memory] create memory subscription to %s on topic %s",
                       key, topic_name)
        self._parent = parent
        self.key = key
        self.topic_name = topic_name
        self.message_class = {
            Bool: MemoryEventBool,
            Float32: MemoryEventFloat,
            Int32: MemoryEventInt,
            String: MemoryEventString}[dtype]
        # get connection
        mem = self._parent.get_mem()  # may raise
        # ROS publisher
        # It needs to be created before we actually subscribe so that our
        # callback can do the publishing in case it is called right away.
        # It implies we delete it in case of issue.
        self.publisher = rospy.Publisher(self.topic_name, dtype, queue_size=10)
        # Pepper subscription
        try:
            rospy.logdebug("[Memory] subscribing to %s...", key)
            self._signal_sub = mem.subscriber(key)
            self._conn = self._signal_sub.signal.connect(self._callback)
        except RuntimeError as e:
            self.publisher.unregister()  # closes connection
            self.publisher = None
            raise e
        # event publisher
        self.event_publisher = parent.event_publishers[dtype]

    def _callback(self, value):
        """Callback when value is changed."""
        rospy.logdebug("[Memory] received new value %r for %s", value,
                       self.key)
        # value publication
        self.publisher.publish(value)
        # event publication
        event_message = self.message_class()
        event_message.key = self.key
        event_message.value = value
        self.event_publisher.publish(event_message)

    def cleanup(self):
        """Cleanup subscription and ROS topic."""
        rospy.logdebug("[Memory] deleting subscription to %s on %s", self.key,
                       self.topic_name)
        # disconnect subscription
        # XXX should we check the connection?
        self._signal_sub.signal.disconnect(self._conn)
        # close publication
        self.publisher.unregister()
        self.publisher = None

    def __def__(self):
        """Properly close subscription on deletion"""
        self.cleanup()


class PepperMemoryDriver(object):
    """Main class of the memory driver."""

    def __init__(self):
        """Connect to Pepper and create services."""
        # parameters
        pepper_ip = get_ros_param('pepper_ip', log_prefix='[Memory] ')
        pepper_port = 9559  # never seen any other port: no point in param
        # memory subscriptions
        self.memory_subscriptions = {}
        # session
        self._session = qi.Session()
        self._url = "tcp://%s:%d" % (pepper_ip, pepper_port)
        self._mem = None
        try:
            self.get_mem()
        except RuntimeError as e:
            rospy.logwarn("[Memory] Could not connect to robot: %r", str(e))
        # Publishers
        self.event_publishers = {
            Bool: rospy.Publisher('memory_event_bool', MemoryEventBool,
                                  queue_size=10),
            Float32: rospy.Publisher('memory_event_float', MemoryEventFloat,
                                     queue_size=10),
            Int32: rospy.Publisher('memory_event_int', MemoryEventInt,
                                   queue_size=10),
            String: rospy.Publisher('memory_event_string', MemoryEventString,
                                    queue_size=10)
        }
        # ROS services
        self.get_mem_int_srv = rospy.Service(
            'get_memory_int', GetMemoryInt,
            partial(self.get_memory_value, dtype_str='int'))
        self.get_mem_float_srv = rospy.Service(
            'get_memory_float', GetMemoryFloat,
            partial(self.get_memory_value, dtype_str='float'))
        self.get_mem_string_srv = rospy.Service(
            'get_memory_string', GetMemoryString,
            partial(self.get_memory_value, dtype_str='string'))
        self.get_mem_bool_srv = rospy.Service(
            'get_memory_bool', GetMemoryBool,
            partial(self.get_memory_value, dtype_str='bool'))
        self.set_mem_int_srv = rospy.Service(
            'set_memory_int', SetMemoryInt,
            partial(self.set_memory_value, dtype_str='int'))
        self.set_mem_float_srv = rospy.Service(
            'set_memory_float', SetMemoryFloat,
            partial(self.set_memory_value, dtype_str='float'))
        self.set_mem_string_srv = rospy.Service(
            'set_memory_string', SetMemoryString,
            partial(self.set_memory_value, dtype_str='string'))
        self.set_mem_bool_srv = rospy.Service(
            'set_memory_bool', SetMemoryBool,
            partial(self.set_memory_value, dtype_str='bool'))
        self.del_mem_srv = rospy.Service('del_memory', DelMemory,
                                         self.del_memory)
        self.manage_data_stream_srv = rospy.Service(
            'manage_data_stream', ManageDataStream, self.manage_topic)

    def get_mem(self):
        """Connect to the Pepper robot ALMemory module.

        Return:
            proxy to ALMemory module

        Raise:
            RuntimeError: in case of connection problem.
        """
        connected = self._session.isConnected()
        rospy.logdebug("[Memory] connected to robot: %r", connected)
        if connected:
            return self._mem
        rospy.logdebug("[Memory] connecting to robot...")
        self._session.connect(self._url)
        rospy.logdebug("[Memory] connecting to ALMemory...")
        self._mem = self._session.service("ALMemory")
        return self._mem

    def get_memory_value(self, req, dtype_str):
        """Get value from Pepper memory."""
        rospy.logdebug("[Memory] get_memory_%s request: %r", dtype_str, req)
        # default
        success = True
        error_message = ''
        # default value
        value = {'int': 0, 'float': 0., 'string': '', 'bool': False}[dtype_str]
        try:
            value = self.get_mem().getData(req.key)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Memory] Failed getting value for key %r: %s",
                          req.key, error_message)
        else:
            rospy.loginfo("[Memory] Got value for key %r: %r.", req.key, value)
        response = (success, error_message, value)
        rospy.logdebug("[Memory] get_memory_%s response: %r", dtype_str,
                       response)
        return response

    def set_memory_value(self, req, dtype_str):
        """Set value in Pepper memory."""
        rospy.logdebug("[Memory] set_memory_%s request: %r", dtype_str, req)
        # default
        success = True
        error_message = ''
        try:
            mem = self.get_mem()
            mem.raiseEvent(req.key, req.value)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Memory] Failed setting key-value pair %r: %r: %s",
                          req.key, req.value, error_message)
        else:
            rospy.loginfo("[Memory] Success setting value for key %r: %r.",
                          req.key, req.value)
        response = (success, error_message)
        rospy.logdebug("[Memory] set_memory_%s response: %r", dtype_str,
                       response)
        return response

    def del_memory(self, req):
        """Remove key from Pepper memory."""
        rospy.logdebug("[Memory] del_memory request: %r", req)
        # default
        success = True
        error_message = ''
        try:
            self.get_mem().removeData(req.key)
        except RuntimeError as e:
            success = False
            error_message = str(e)
            rospy.logwarn("[Memory] Failed removing key %r: %s", req.key,
                          error_message)
        else:
            rospy.loginfo("[Memory] Success removing key: %s.", req.key)
        response = (success, error_message)
        rospy.logdebug("[Memory] del_memory response: %r", response)
        return response

    def manage_topic(self, req):
        """Manage data subscription."""
        rospy.logdebug("[Memory] manage_data_stream request: %r", req)
        # default outcome
        success = True
        error_message = ''
        if req.command == ManageDataStreamRequest.START:
            entry_key = req.key, req.topic_name
            if entry_key in self.memory_subscriptions:
                success = False
                error_message = ("key %s already published on topic %s."
                                 % entry_key)
                rospy.logwarn("[Memory] Ignoring request: %s", error_message)
            else:
                dtype_table = {
                    ManageDataStreamRequest.INT: Int32,
                    ManageDataStreamRequest.FLOAT: Float32,
                    ManageDataStreamRequest.STRING: String,
                    ManageDataStreamRequest.BOOL: Bool
                }
                try:
                    dtype = dtype_table[req.type]
                    new_mem_sub = MemorySubscription(self, req.key, dtype,
                                                     req.topic_name)
                except KeyError:
                    success = False
                    error_message = "unknown data type: %d." % req.type
                    rospy.logwarn("[Memory] Ignoring request: %s",
                                  error_message)
                except RuntimeError as e:
                    success = False
                    error_message = str(e)
                    rospy.logwarn("[Memory] Failed creating subscription: %s",
                                  error_message)
                else:
                    self.memory_subscriptions[entry_key] = new_mem_sub
        elif req.command == ManageDataStreamRequest.STOP:
            entry_key = req.key, req.topic_name
            if entry_key not in self.memory_subscriptions:
                success = False
                error_message = "key %s not published on topic %s." % entry_key
                rospy.logwarn("[Memory] Ignoring request: %s", error_message)
            else:
                mem_sub = self.memory_subscriptions.pop(entry_key)
                mem_sub.cleanup()  # explicit cleanup
        else:
            success = False
            error_message = "unknown command %d." % req.command
            rospy.logwarn("[Memory] Ignoring request: %s", error_message)
        response = (success, error_message)
        rospy.logdebug("[Memory] manage_data_stream response: %r", response)
        return response

    def run(self):
        """Spin"""
        rospy.spin()

    def cleanup(self):
        """Cleanup living subscriptions."""
        rospy.logdebug("[Memory] cleanup...")
        while self.memory_subscriptions:
            _, mem_sub = self.memory_subscriptions.popitem()
            mem_sub.cleanup()  # explicit cleanup


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('pepper_memory_driver')
        memd = PepperMemoryDriver()
        memd.run()
    except rospy.ROSInterruptException:
        pass
    memd.cleanup()


if __name__ == '__main__':
    main()
