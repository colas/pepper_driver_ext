"""Simple web server."""

# standard import
import os
import socket
import threading
from SocketServer import ThreadingTCPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler


class SimpleThreadedServer(object):
    """Simple HTTP server running in a thread."""

    def __init__(self, directory, port):
        """Initialize status (stopped)"""
        # internal state
        self._directory = directory
        self._port = port
        self._server = None
        self._server_thread = None
        # status
        self.started = False

    def start(self):
        """Start web server.

        Raise:
            RuntimeError
        """
        if self.started:
            raise RuntimeError('Web server already started')
        # check and change directory
        if not os.path.isdir(self._directory):
            raise RuntimeError('Invalid directory: %s' % self._directory)
        os.chdir(self._directory)
        # make it so we don't have an error when restarting
        ThreadingTCPServer.allow_reuse_address = True
        # create server
        try:
            self._server = ThreadingTCPServer(('', self._port),
                                              SimpleHTTPRequestHandler)
        except socket.error as e:
            raise RuntimeError('Cannot create server: %s' % str(e))
        # create server thread
        self._server_thread = threading.Thread(
            target=self._server.serve_forever)
        # exit server thread when main thread terminates
        self._server_thread.daemon = True
        # start server thread
        self._server_thread.start()
        self.started = True

    def stop(self):
        """Stop web server.

        Raise:
            RuntimeError
        """
        if not self.started:
            raise RuntimeError('Web server not started')
        self._server.shutdown()  # stop TCPServer
        self._server.server_close()  # close socket
        self._server = None
        self.started = False  # assume it's ok
        # check thread is properly closed
        if self._server_thread.is_alive():
            self._server_thread.join(.1)
        if self._server_thread.is_alive():
            raise RuntimeError('server thread does not join')
