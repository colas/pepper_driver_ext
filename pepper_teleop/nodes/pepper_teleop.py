#!/usr/bin/env python

"""Joystick teleoperation node for the Pepper robot.

This node computes velocity commands from joystick messages.

It subscribes to:
    - `/joy` (sensor_msgs/Joy): joystick message.

It publishes to:
    - `/cmd_vel` (geometry_msgs/Twist): command velocity.

Parameters:
    - `max_lin_vel`: maximum linear velocity in m/s
        (default 0.35).
    - `max_ang_vel`: maximum angular velocity in rad/s
        (default 1.0).
"""

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy


class PepperTeleop:
    """Joystick teleoperation for Pepper robot."""

    def __init__(self):
        """Create publisher and subscriber."""
        # parameters
        self.max_lin_vel = rospy.get_param('~max_lin_vel', 0.35)
        self.max_ang_vel = rospy.get_param('~max_ang_vel', 1.0)
        # publisher
        self.cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        # subscriber
        self.joy_sub = rospy.Subscriber('/joy', Joy, self.joy_cb)

    def joy_cb(self, msg):
        """Compute and publish twist command."""
        # initialize message
        cmd_vel_msg = Twist()
        # fill message
        cmd_vel_msg.linear.x = self.max_lin_vel * msg.axes[1]
        cmd_vel_msg.linear.y = self.max_lin_vel * msg.axes[3]
        cmd_vel_msg.linear.z = 0.
        cmd_vel_msg.angular.x = 0.
        cmd_vel_msg.angular.y = 0.
        cmd_vel_msg.angular.z = self.max_ang_vel * msg.axes[0]
        # publish message
        self.cmd_vel_pub.publish(cmd_vel_msg)


def main():
    """Instantiate node and class."""
    # declare node
    rospy.init_node('pepper_teleop')
    # instantiate class
    pepper_teleop = PepperTeleop()
    # run
    rospy.spin()


if __name__ == '__main__':
    main()
