#!/bin/bash

# wait until rosparam responds
rosparam list 2> /dev/null > /dev/null
while [ $? -ne 0 ]
do
    echo "rosmaster not found, retrying in 2s..."
    sleep 2
    rosparam list 2> /dev/null > /dev/null
done

# load parameters
rosparam load param_bridge_topics.yaml
rosparam load param_bridge_services.yaml
# launch bridge
ros2 run ros1_bridge parameter_bridge
