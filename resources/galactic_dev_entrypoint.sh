#!/bin/bash
set -e

# source ROS2 distribution
source /opt/ros/$ROS_DISTRO/setup.bash
# source ROS2 environment
if [ -f /home/pepper/ros2_ws/install/local_setup.bash ]; then
    source /home/pepper/ros2_ws/install/local_setup.bash
fi

exec "$@"
