#!/bin/bash
set -e

# source ROS1 environment
unset ROS_DISTRO  # prevent warning
source /opt/ros/noetic/setup.bash
# source ROS2 environment
unset ROS_DISTRO  # prevent warning
source /opt/ros/galactic/setup.bash
# source ROS1 pepper messages
unset ROS_DISTRO  # prevent warning
source /home/pepper/catkin_ws/install_isolated/setup.bash
# source ROS2 pepper messages
source /home/pepper/ros2_ws/install/local_setup.bash
# source bridge workspace
source /home/pepper/bridge_ws/install/local_setup.bash

exec "$@"
