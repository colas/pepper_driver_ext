#!/bin/bash
set -e

# source ROS1 environment
unset ROS_DISTRO  # prevent warning
source /opt/ros/noetic/setup.bash
# source ROS1 pepper messages
source /home/pepper/catkin_ws/install_isolated/setup.bash
# source ROS2 environment
unset ROS_DISTRO  # prevent warning
source /home/pepper/ros2_humble/install/local_setup.bash
# source bridge workspace
source /home/pepper/bridge_ws/install/local_setup.bash

# disable shared memory transport
export FASTRTPS_DEFAULT_PROFILES_FILE=/home/pepper/fastrtps_udp_transport_profile.xml

exec "$@"
